﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace UFS.UPSWrapper
{
    public class HttpDataExchangeProcessor
    {
        public object Post(string url, string xmlData)
        {
            var req = (HttpWebRequest)WebRequest.Create(url);
            var postData = Encoding.ASCII.GetBytes(xmlData);
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = postData.Length;
            using (var stream = req.GetRequestStream())
            {
                stream.Write(postData, 0, postData.Length);
            }
            var response = (HttpWebResponse)req.GetResponse();
            var resposeStr = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return resposeStr;
        }
    }
}
