﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shopify.Web.Interaction
{
    [Serializable]
    public class PutRestRequest
    {
        public string SiteOrderID { get; set; }
        public string Site { get; set; }
        public string OrderStatus { get; set; }
        public string DateOrdered { get; set; }
        public string DateShipped { get; set; }
        public string OrderTracking { get; set; }
        public string OrderCarrier { get; set; }
        public string OrderService { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string StateOrRegion { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public Orderdetail[] OrderDetails { get; set; }
    }

    [Serializable]
    public class Orderdetail
    {
        public string SiteItemID { get; set; }
        public string OrderStatus { get; set; }
        public string DateShipped { get; set; }
        public string SKU { get; set; }
        public string Title { get; set; }
        public int QuantityOrdered { get; set; }
        public int QuantityShipped { get; set; }
        public int QuantityUnfillable { get; set; }
        public string CurrencyISO { get; set; }
        public int UnitPrice { get; set; }
        public int UnitTax { get; set; }
        public int ShippingPrice { get; set; }
        public int ShippingTax { get; set; }
        public int ShippingDiscount { get; set; }
        public string GiftMessage { get; set; }
        public int GiftWrapPrice { get; set; }
        public int GiftWrapTax { get; set; }
        public string ShippingServiceOrdered { get; set; }
        public string ShippingServiceActual { get; set; }
        public string ShippingTracking { get; set; }
        public string ShippingActualWeight { get; set; }
        public int ShippingActualCharge { get; set; }
        public string ShippingCarrier { get; set; }
    }

}

