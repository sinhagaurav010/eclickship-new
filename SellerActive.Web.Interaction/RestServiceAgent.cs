﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace Shopify.Web.Interaction
{
    public class RestServiceAgent
    {
        private RestResponse _response;
        public RestResponse SrvResponse
        {
            get
            {
                return _response != null ? _response : new RestResponse();
            }
            set
            {
                _response = value;
            }
        }
        public RestRequest Request { get; set; }
        public string AccessKey { get; set; }
        public string RestPutRequest { get; set; }
        public RestServiceAgent()
        {

        }
        public void GetAccessToken(string code)
        {
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(Request.RequestURL);
            rq.Method = "POST";
            string requestBody = "{ \"client_id\": \"811c80f5338bc72e940b50b9b1ec2765\",\"client_secret\" :\"0f645c3d0d4467e2b3b3a1c699fa369d\",\"code\": \"" + code + "\"}";
           // rq.ContentType = "application/json; charset=utf-8";
          //  rq.Headers.Add("client_id", "4e3ee71792fa3a5b31853b664baf29ba");
         //   rq.Headers.Add("client_secret", "603ac34dad1ff63250e62bfd0778810d");
         //   rq.Headers.Add("code", code);
            try
            {
              string val =  MakePutRequestAsync(requestBody, "POST");
                JavaScriptSerializer oJS = new JavaScriptSerializer();
                AccessTokenData oRootObject = new AccessTokenData();
                oRootObject = oJS.Deserialize<AccessTokenData>(val);
                AccessKey = oRootObject.access_token;
            }
            catch (Exception ex)
            {
                SrvResponse.Message = ex.Message;
                SrvResponse.IsSuccessfull = false;
            }
        }
        public void MakeGetRequest()
        {
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(Request.RequestURL);
            rq.Method = "GET";
            rq.ContentType = "application/json; charset=utf-8";
            rq.Headers.Add(Request.RequestAuthorizationHeader.Key, Request.RequestAuthorizationHeader.Value);
            try
            {
                HttpWebResponse rs = (HttpWebResponse)rq.GetResponse();
               // Encoding enc = System.Text.Encoding.GetEncoding("utf-8");              
                using (StreamReader sr = new StreamReader(rs.GetResponseStream()))
                {
                    string result = sr.ReadToEnd();
                    SrvResponse.Response = result;
                    SrvResponse.IsSuccessfull = true;
                }
                rs.Close();
            }
            catch (Exception ex)
            {
                SrvResponse.Message = ex.Message;
                SrvResponse.IsSuccessfull = false;
            }
            
        }

        public string MakeGetRequestAsync()
        {
            try
            {
                string Method = "GET";
                string uri = Request.RequestURL;
                HttpWebRequest req = WebRequest.Create(uri) as HttpWebRequest;
                req.KeepAlive = false;
                req.Method = Method.ToUpper();

                req.ContentType = "application/json; charset=utf-8";
                req.Headers.Add(Request.RequestAuthorizationHeader.Key, Request.RequestAuthorizationHeader.Value);
                //req.Headers.Add("X-Shopify-Access-Token", this.State.AccessToken);
                HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader loResponseStream =
                new StreamReader(resp.GetResponseStream(), enc);

                string output= loResponseStream.ReadToEnd();
                SrvResponse.Response = output;
                loResponseStream.Close();
                resp.Close();
                SrvResponse.IsSuccessfull = true;
                return output;
            }
            catch (Exception ex)
            {
                SrvResponse.Message = ex.Message;
                return "";
            }
        }

        public string MakePutRequestAsync(string postData = "",string requestMethod = "PUT")
        {
            try
            {
                string Method = requestMethod;
                string uri = Request.RequestURL;
                HttpWebRequest req = WebRequest.Create(uri) as HttpWebRequest;
                req.KeepAlive = false;
                req.Method = Method.ToUpper();               
                req.ContentType = "application/json; charset=utf-8";
                if (Request.RequestAuthorizationHeader!=null && !string.IsNullOrWhiteSpace(Request.RequestAuthorizationHeader.Value))
                {
                    req.Headers.Add(Request.RequestAuthorizationHeader.Key, Request.RequestAuthorizationHeader.Value);
                }
                if (!string.IsNullOrWhiteSpace(postData))
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    req.ContentLength = byteArray.Length;
                    using (Stream strm = req.GetRequestStream())
                    {
                        strm.Write(byteArray, 0, byteArray.Length);
                    }
                }               
                HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader loResponseStream =
                new StreamReader(resp.GetResponseStream(), enc);

                string output = loResponseStream.ReadToEnd();
                SrvResponse.Response = output;
                loResponseStream.Close();
                resp.Close();
                SrvResponse.IsSuccessfull = true;
                return output;
            }
            catch (Exception ex)
            {
                SrvResponse.Message = ex.Message;
                return "";
            }
        }
    }
    
    public class RestRequest
    {

        public KeyValuePair RequestAuthorizationHeader { get; set; }
        public List<KeyValuePair> RequestParamteres { get; set; }

        public string RequestURL { get; set; }

    }
    public class RestResponse
    {
        public RestResponse()
        {
            RawResponse = "";
            Response = "";
            IsSuccessfull = false;
            Message = "";
        }
        public string RawResponse { get; set; }
        public string Response { get; set; }
        public bool IsSuccessfull { get; set; }

        public string Message { get; set; }
    }

    public class KeyValuePair
    {
        public int KeyInt { get; set; }
        public int ValueInt { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }

    }
    public class AccessTokenData
    {
        public string access_token { get; set; }
        public string scope { get; set; }
    }
}
