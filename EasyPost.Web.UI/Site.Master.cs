﻿using UFS.Web.BusinesEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EasyPostDemo
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] != null)
                {
                    UserDetail usr = (UserDetail)Session["user"];
                    liLogoff.Visible = true;
                    aUser.InnerHtml = "<div>" + usr.FirstName + " " + usr.LastName + "</div>";
                    username.Visible = true;
                    liReport.Visible = true;
                    liRegister.Visible = false;
                    liLogin.Visible = false;
                    batchHistory.Visible = true;
                    summaryHistory.Visible = true;
                    liManageUser.Visible = usr.RoleId == "1";
                    liReport.Visible = true;
                    liEdit.Visible = true;
                    liCLear.Visible = usr.RoleId == "1";
                }
                else
                {
                    batchHistory.Visible = false;
                    summaryHistory.Visible = false;
                    liLogoff.Visible = false;
                    username.Visible = false;
                    liRegister.Visible = true;
                    liLogin.Visible = true;
                    manageUsers.Visible = false;
                    liEdit.Visible = false;
                    liCLear.Visible = false;
                    liManageUser.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
}