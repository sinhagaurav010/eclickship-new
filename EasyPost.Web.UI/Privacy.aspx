﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Privacy.aspx.cs" Inherits="EasyPostDemo.Privacy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="margin-bottom: 0px; color: #0089da">
        <h3>Privacy Policy</h3>
    </div>
    <div style="width: 100%; text-align: justify">
        <div style="width: 100%; text-align: center">

            <h3>UNITED FREIGHT SAVERS PRIVACY POLICY</h3>

        </div>
        <b>Last updated on January 26, 2015
        <br />
        <br />
        </b>
        <strong>Our Policy:
        <br />
        <br />
        </strong>Welcome to the web site (the “Site”) of United Freight Savers. (“UFS”, “we”, “us” and/or “our”). This Site is operated by Mobilecom International Inc and UFS has been created to provide information about our company and our packaging and shipping services and related services (together with the Site, the “Services”) to our Service visitors (“you”, “your”). This Privacy Policy sets forth UFS’s policy with respect to information including personally identifiable data (“Personal Data”) and other information that is collected from visitors to the Site and Services.
        <br />
        <br />
        <br />
        <strong>Information We Collect:</strong>
        <br />
        <br />
When you interact with us through the Services, we may collect Personal Data and other information from you, as further described below:
        <br />
        <br />
        Personal Data That You Provide Through the Services: We collect Personal Data from you when you voluntarily provide such information, such as when you contact us with inquiries, respond to one of our surveys, register for access to the Services or use certain Services. Wherever UFS collects Personal Data we make an effort to provide a link to this Privacy Policy.
        <br />
        <br />
        By voluntarily providing us with Personal Data, you are consenting to our use of it in accordance with this Privacy Policy. If you provide Personal Data to the Services, you acknowledge and agree that such Personal Data may be transferred from your current location to the offices and servers of UFS and the authorized third parties referred to herein located in the United States.
        <br />
        <br />
        <br />
        <strong>Other Information:</strong>
        <br />
        <br />
Non-Identifiable Data: When you interact with UFS through the Services, we receive and store certain personally non-identifiable information. Such information, which is collected passively using various technologies, cannot presently be used to specifically identify you. UFS may share such information with third parties for any lawful purpose, or such information may be stored or included in databases owned and maintained by UFS affiliates, agents or service providers. The Services may use such information and pool it with other information to track, for example, the total number of visitors to our Site, the number of visitors to each page of our Site, and the domain names of our visitors’ Internet service providers. It is important to note that no Personal Data is available or used in this process.
        <br />
        <br />
        In operating the Services, we may use a technology called “cookies.” A cookie is a piece of information that the computer that hosts our Services gives to your browser when you access the Services. Our cookies help provide additional functionality to the Services and help us analyze Services usage more accurately. For instance, our Site may set a cookie on your browser that allows you to access the Services without needing to remember and then enter a password more than once during a visit to the Site. In all cases in which we use cookies, we will not collect Personal Data except with your permission. On most web browsers, you will find a “help” section on the toolbar. Please refer to this section for information on how to receive notification when you are receiving a new cookie and how to turn cookies off. We recommend that you leave cookies turned on because they allow you to take advantage of some of the Service features.<br />
        <br />
Aggregated Personal Data: In an ongoing effort to better understand and serve the users of the Services, UFS often conducts research on its customer demographics, interests and behavior based on the Personal Data and other information provided to us. This research may be compiled and analyzed on an aggregate basis, and UFS may share this aggregate data with its affiliates, agents and business partners. This aggregate information does not identify you personally. UFS may also disclose aggregated user statistics in order to describe our services to current and prospective business partners, and to other third parties for other lawful purposes.
        <br />
        <br />
        Location Information: Our Service may collect and use your location information (for example, by using the GPS on your mobile device) to provide certain functionality of our Service. If you choose to enable our location features, your location information may be publicly displayed within the Service. We may also use your location information in a non-identifiable or aggregate way, as described above in the “Non-Identifiable Data” and “Aggregated Personal Data” sections.
        <br />
        <br />
        <br />
        <strong>Our Use of Your Personal Data and Other Information:
        <br />
        <br />
        </strong>UFS uses the Personal Data you provide in a manner that is consistent with this Privacy Policy. If you provide Personal Data for a certain reason, we may use the Personal Data in connection with the reason for which it was provided. For instance, if you contact us by email, we will use the Personal Data you provide to answer your question or resolve your problem. Also, if you provide Personal Data in order to obtain access to the Services, we will use your Personal Data to provide you with access to such services and to monitor your use of such services. UFS and its subsidiaries and affiliates (the “Related Companies”) may also use your Personal Data and other personally non-identifiable information collected through the Services to help us improve the content and functionality of the Services, to better understand our users and to improve the Services. UFS and its affiliates may use this information to contact you in the future to tell you about services we believe will be of interest to you. If we do so, each marketing communication we send you will contain instructions permitting you to “opt-out” of receiving future marketing communications. In addition, if at any time you wish not to receive any future marketing communications or you wish to have your name deleted from our mailing lists, please contact us as indicated below.
        <br />
        <br />
        If UFS intends on using any Personal Data in any manner that is not consistent with this Privacy Policy, you will be informed of such anticipated use prior to or at the time at which the Personal Data is collected.
        <br />
        <br />
        <br />
        <strong>Our Disclosure of Your Personal Data and Other Information:
        <br />
        <br />
        </strong>UFS is not in the business of selling your information. We consider this information to be a vital part of our relationship with you. There are, however, certain circumstances in which we may share your Personal Data with certain third parties without further notice to you, as set forth below:<br />
        <br />
Business Transfers: As we develop our business, we might sell or buy businesses or assets. In the event of a corporate sale, merger, reorganization, dissolution or similar event, Personal Data may be part of the transferred assets.
        <br />
        <br />
        Related Companies: We may also share your Personal Data with our Related Companies for purposes consistent with this Privacy Policy.
        <br />
        <br />
        Agents, Consultants and Related Third Parties: UFS, like many businesses, sometimes hires other companies to perform certain business-related functions. Examples of such functions include mailing information, maintaining databases and processing payments. When we employ another entity to perform a function of this nature, we only provide them with the information that they need to perform their specific function.
        <br />
        <br />
        Legal Requirements: UFS may disclose your Personal Data if required to do so by law or in the good faith belief that such action is necessary to (i) comply with a legal obligation, (ii) protect and defend the rights or property of UFS, (iii) act in urgent circumstances to protect the personal safety of users of the Services or the public, or (iv) protect against legal liability.
        <br />
        <br />
        <br />
        <strong>Your Choices:
        <br />
        <br />
        </strong>You can visit the Site without providing any Personal Data. If you choose not to provide any Personal Data, you may not be able to use certain Services.
        <br />
        <br />
        <br />
        <strong>Exclusions:
        <br />
        <br />
        </strong>This Privacy Policy does not apply to any Personal Data collected by UFS other than Personal Data collected through the Services. This Privacy Policy shall not apply to any unsolicited information you provide to UFS through the Services or through any other means. This includes, but is not limited to, information posted to any public areas of the Services, such as forums, any ideas for new products or modifications to existing products, and other unsolicited submissions (collectively, “Unsolicited Information”). All Unsolicited Information shall be deemed to be non-confidential and UFS shall be free to reproduce, use, disclose, and distribute such Unsolicited Information to others without limitation or attribution.
        <br />
        <br />
        Children: UFS does not knowingly collect Personal Data from children under the age of 13. If you are under the age of 13, please do not submit any Personal Data through the Services. We encourage parents and legal guardians to monitor their children’s Internet usage and to help enforce our Privacy Policy by instructing their children never to provide Personal Data on the Services without their permission. If you have reason to believe that a child under the age of 13 has provided Personal Data to UFS through the Services, please contact us, and we will endeavor to delete that information from our databases.
       <br />
        <br />
        <br />
        <br />
        <strong>Links to Other Web Sites:
        <br />
        <br />
        </strong>This Privacy Policy applies only to the Services. The Services may contain links to other web sites not operated or controlled by UFS (the “Third Party Sites”). The policies and procedures we described here do not apply to the Third Party Sites. The links from the Services do not imply that UFS endorses or has reviewed the Third Party Sites. We suggest contacting those sites directly for information on their privacy policies.
        <br />
        <br />
        <br />
        <strong>Security:
        <br />
        <br />
        </strong>UFS takes reasonable steps to protect the Personal Data provided via the Services from loss, misuse, and unauthorized access, disclosure, alteration, or destruction. However, no Internet or email transmission is ever fully secure or error free. In particular, email sent to or from the Services may not be secure. Therefore, you should take special care in deciding what information you send to us via email. Please keep this in mind when disclosing any Personal Data to UFS via the Internet.
        <br />
        <br />
        <br />
        <strong>Other Terms and Conditions:
        <br />
        <br />
        </strong>Your access to and use of the Services is subject to the Terms of Service at terms.unitedfreightsavers.com.
        <br />
        <br />
        <br />
        <strong>Changes to UFS’s Privacy Policy:
        <br />
        <br />
        </strong>The Services and our business may change from time to time. As a result, at times it may be necessary for UFS to make changes to this Privacy Policy. UFS reserves the right to update or modify this Privacy Policy at any time and from time to time without prior notice. Please review this policy periodically, and especially before you provide any Personal Data. This Privacy Policy was last updated on the date indicated above. Your continued use of the Services after any changes or revisions to this Privacy Policy shall indicate your agreement with the terms of such revised Privacy Policy.
        <br />
        <br />
        <br />
        <strong>Access to Information; Contacting United Freight Savers:
        <br />
        <br />
        <br />
&nbsp;&nbsp;&nbsp; </strong>To keep your Personal Data accurate, current, and complete, please contact us as specified below. We will take reasonable steps to update or correct Personal Data in our possession that you have previously submitted via the Services.
Please also feel free to contact us if you have any questions about UFS’s Privacy Policy or the information practices of the Services.
        <br />
        <br />
        <br />
        You may contact us as follows: <a href="mailto:support@unitedfreightsavers.com">support@unitedfreightsavers.com</a>.



    </div>
</asp:Content>
