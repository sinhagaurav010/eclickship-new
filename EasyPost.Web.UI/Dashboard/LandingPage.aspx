﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" Async="true" CodeBehind="LandingPage.aspx.cs" Inherits="EasyPostDemo.Dashboard.LandingPage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <script src="http://cdn.kendostatic.com/2013.2.716/js/kendo.all.min.js"></script>
    <link href="../css/kendo.common.min.css" rel="stylesheet" />
    <link href="../css/kendo.metro.min.css" rel="stylesheet" />
    <script src="../Scripts/jquery.min.js"></script>
    <br />

    <div id="mainDiv" style="padding-top: 10px; text-align: center" runat="server">
        <%-- <img src="../css/Metro/loading_2x.gif" />--%>
        <img src="../Images/waiting.gif" />
        <h3>Please wait while Shopify store data is being retrieved.<br />
            You will automatically be redirected to home page after the successfull operation</h3>
    </div>

    <script type="text/javascript">
      
        $.ajax({
            type: 'POST',
            url: "LandingPage.aspx/FetchDatafromStore",
            dataType: 'json',
            timeout: 0,
            data: "{}",
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                debugger;
                if (response.d == "0" || response.d == "1") {
                    $("#mainDiv").html("<h3><img src='../Images/accept.png' />Successfully updated data from shopify store. Redirecting to home page. please wait.</h3>");
                    window.location.href = "../Dashboard/";
                    } 
                //else if (response.d == "1") {
                //    $("#mainDiv").html("<img src='../Images/info.png' /> Process completed successfully. Some orders are not sysnced due to insufficient data.<br/>Try Syncing manually from dashborad after fixing the data issue in the Shopify Store.");
                //}
            }
        });
       
    </script>
</asp:Content>
