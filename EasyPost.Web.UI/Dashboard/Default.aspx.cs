﻿using UFS.Web.BusinesEntities;
using UFS.Web.DataOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using UFS.Service.BusinessEntities;
using UFS.Web.Dashboard;
using System.IO;
using System.Net;
using System.Web.Configuration;
using UFS.Messaging.EmailService;

namespace EasyPostDemo.Dashboard
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    Response.Redirect("~/Account/Login.aspx?");
                }
                else
                {
                    UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                    if (hdnUserAddress != null)
                    {
                        hdnUserAddress.Value = "<b>" + (!string.IsNullOrWhiteSpace(usr.Company) ? usr.Company : usr.FirstName + " " + usr.LastName) + "</b><br/>" + usr.Address.Street1 + " " + usr.Address.Street2 + "<br/>" + usr.Address.City + ", " + usr.Address.State + " " + usr.Address.Zip;
                    }
                    //FetchDatafromStore()
                    // Task.Factory.StartNew(this.FetchDatafromStore);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        [WebMethod]
        public static string CancelOrder(string orderId)
        {
            try
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                //string res = UpdateShopify(usr, orderId, string.Empty, string.Empty, false, "POST", 1);
                if (usr != null)
                {
                    new MSSQLCommonDataOperations().CancelOrder(orderId, usr.UserId);
                    return "1";
                }
                else
                {
                    return "-1";
                }
            }
            catch (Exception)
            {

                return "-1";
            }
        }
        [WebMethod]
        public static string DeleteOrder(string orderId)
        {
            try
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                if (usr != null)
                {
                    string res = "{}";// UpdateShopify(usr, orderId, string.Empty, string.Empty, false, "DELETE", 0);
                    if (res == "{}")
                    {
                        new MSSQLCommonDataOperations().DeleteOrder(orderId, usr.UserId);
                    }
                    return res;
                }
                else
                {
                    return "-1";
                }
            }
            catch (Exception)
            {

                return "-1";
            }
        }


        [WebMethod]
        public static string DeleteMultipleOrder(string orderIds)
        {
            try
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                if (usr != null)
                {
                    string[] ordersArr = orderIds.Split(new char[] { ',' });
                    string res = "{}";// UpdateShopify(usr, orderId, string.Empty, string.Empty, false, "DELETE", 0);
                    MSSQLCommonDataOperations dao = new MSSQLCommonDataOperations();
                    foreach (var item in ordersArr)
                    {
                        if (res == "{}")
                        {
                            dao.DeleteOrder(item, usr.UserId);
                        }
                    }
                    return res;
                }
                else
                {
                    return "-1";
                }
            }
            catch (Exception)
            {

                return "-2";
            }
        }



        private static string GetServiceData(UserDetail usr, string datatoFetch, Int64 orderId)
        {

            return LandingPage.GetServiceData(usr, datatoFetch, orderId);
            /*
            Shopify.Web.Interaction.RestServiceAgent agent = new Shopify.Web.Interaction.RestServiceAgent();
            string authKey = usr.AuthCode;
            string apiKey = usr.ShopifyApiKey;
            string pass = usr.ShopifyApiPassword;
            string url = usr.ShpifyStoreUrl.Replace("http://", "").Replace("https://", "");

            string fullUrl = "https://{0}:{1}@{2}/admin/orders.json";
            switch (datatoFetch)
            {
                case "All":
                    fullUrl = !string.IsNullOrWhiteSpace(authKey)? "https://"+url+"/admin/orders.json" : "https://{0}:{1}@{2}/admin/orders.json";
                    break;
                case "Order":
                    fullUrl = !string.IsNullOrWhiteSpace(authKey) ? "https://"+url+"/admin/orders/" + orderId.ToString() + ".json" : "https://{0}:{1}@{2}/admin/orders/" + orderId.ToString() + ".json";
                    break;
                default:
                    break;
            }
            agent.GetAccessToken(authKey);
            agent.Request = new Shopify.Web.Interaction.RestRequest()
            {
                RequestAuthorizationHeader = new Shopify.Web.Interaction.KeyValuePair()
                {
                    Key = !string.IsNullOrWhiteSpace(authKey) ? "X-Shopify-Access-Token" : "Authorization",
                    Value = !string.IsNullOrWhiteSpace(authKey) ? agent.AccessKey :  usr.AuthCode,
                },
                RequestURL = string.Format(fullUrl, apiKey, pass, url)
            };

            string data = agent.MakeGetRequestAsync();
            return data;*/
        }
        private static string UpdateShopify(UserDetail usr, string orderid, string fulfilmentid, string postData, bool isUpdate = true, string reqMethod = "PUT", int isCancelDelete = -1)
        {
            Shopify.Web.Interaction.RestServiceAgent agent = new Shopify.Web.Interaction.RestServiceAgent();
            string apiKey = usr.ShopifyApiKey;
            string pass = usr.ShopifyApiPassword;
            string url = usr.ShpifyStoreUrl.Replace("http://", "").Replace("https://", "");
            string fullUrl = "https://{0}:{1}@{2}/admin/orders/" + orderid.ToString() + "/fulfillments/" + fulfilmentid + ".json";
            if (!isUpdate)
            {
                fullUrl = "https://{0}:{1}@{2}/admin/orders/" + orderid.ToString() + "/fulfillments.json";
            }
            if (isCancelDelete >= 0)
            {
                if (isCancelDelete == 1)
                    fullUrl = "https://{0}:{1}@{2}/admin/orders/" + orderid.ToString() + "/cancel.json";
                if (isCancelDelete == 0)
                    fullUrl = "https://{0}:{1}@{2}/admin/orders/" + orderid.ToString() + ".json";
            }
            agent.Request = new Shopify.Web.Interaction.RestRequest()
            {
                RequestAuthorizationHeader = new Shopify.Web.Interaction.KeyValuePair()
                {
                    Key = "Authorization",
                    Value = usr.AuthCode,
                },
                RequestURL = string.Format(fullUrl, apiKey, pass, url)
            };

            string data = agent.MakePutRequestAsync(postData, reqMethod);
            return data;
        }
        [WebMethod]
        public static object GetOrders()
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                var data = new MSSQLUserOperations().GetOrders(usr.UserId);
                return data;
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                return null;
            }
        }


        [WebMethod]
        public static object GetOrderDetail(string orderid)
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                var data = GetServiceData(usr, "Order", Convert.ToInt64(orderid));
                return data;
            }
            else
            {
                // HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                //return null;
                return "-1";
            }
        }
        [WebMethod]
        public static string GetOrderSummary(string orderids)
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                if (!string.IsNullOrWhiteSpace(orderids))
                {
                    string orderNums = "";
                    UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                    string[] arr = orderids.Split(new char[] { ',' });
                    UFS.Web.ShopifyOrder.MainOrder ordr = new UFS.Web.ShopifyOrder.MainOrder();
                    List<UFS.Web.ShopifyOrder.LineItem> lstLineItms = new List<UFS.Web.ShopifyOrder.LineItem>();
                    foreach (var item in arr)
                    {
                        //  res.Add();
                        ordr = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue }.Deserialize<UFS.Web.ShopifyOrder.MainOrder>(GetServiceData(usr, "Order", Convert.ToInt64(item)));
                        if (ordr != null && ordr.Order != null && ordr.Order.line_items != null && ordr.Order.line_items.Count > 0)
                        {
                            orderNums = orderNums + ", #" + ordr.Order.order_number.ToString();
                            foreach (var lItem in ordr.Order.line_items)
                            {
                                if (lItem.sku == "DOZ101")
                                {
                                    lItem.sku = "RBW101";
                                    lItem.quantity = lItem.quantity * 12;
                                    lItem.title = "The Original Rainbow Bagelâ„¢";
                                }
                                if (lItem.sku == "DOZ102")
                                {
                                    lItem.sku = "CRG101";
                                    lItem.quantity = lItem.quantity * 12;
                                    lItem.title = "The Original Cragel";
                                }
                                lstLineItms.Add(lItem);
                            }
                        }
                    }
                    List<SummaryData> lstData = new List<SummaryData>();
                    if (lstLineItms != null && lstLineItms.Count > 0)
                    {
                        var item = lstLineItms.GroupBy(m => m.sku);
                        foreach (var val in item)
                        {
                            lstData.Add(new SummaryData()
                            {
                                SKU = val.Key,
                                Title = val.FirstOrDefault(m => m.sku == val.Key).title,
                                Quantity = (val.Key.ToUpper().StartsWith("DOZ") ? (val.Sum(m => m.quantity) * 12).ToString() + " pc" : val.Sum(m => m.quantity).ToString() + " " + (val.Key.ToUpper().StartsWith("SPR") ? "lbs" : "pc")),
                            });
                        }
                        if (lstData != null && lstData.Count > 0)
                        {
                            lstData[0].OrderNumbers = orderNums;
                        }
                    }
                    new MSSQLCommonDataOperations().UpdateOrdersForSummary(usr.UserId, orderids);
                    return new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue }.Serialize(lstData);

                }
                else
                {
                    return "Please select Orders";
                }
            }
            else
            {
                //HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                return "-1";
            }

        }
        [WebMethod]
        public static string GetOrderDetails(string orderids)
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                string[] arr = orderids.Split(new char[] { ',' });
                List<object> res = new List<object>();
                foreach (var item in arr)
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        res.Add(GetServiceData(usr, "Order", Convert.ToInt64(item)));
                    }
                }

                return new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue }.Serialize(res);
            }
            else
            {
                // HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                //return null;
                return "-1";
            }
        }

        [WebMethod]
        public static string GetLabels(string shipmentId)
        {
            try
            {
                string randString = UFS.Web.Common.CommonUtilities.GetRandomString(10);
                string images = new MSSQLReportDataOperations().GetLabels(shipmentId);
                if (!string.IsNullOrWhiteSpace(images))
                {
                    string[] arrLabels = images.Split(new char[] { ',' });
                    if (arrLabels.Length > 0)
                    {
                        HttpContext.Current.Session[randString] = arrLabels.ToList<string>();
                        return randString;
                    }
                    else
                    {
                        return "Unable to get labels at this time. Please try again in few moments or contatc system admin.";
                    }
                }
                else
                {
                    return "Unable to get labels at this time. Please try again in few moments or contatc system admin.";
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [WebMethod]
        public static string GetShipmentId(string siteOrderId)
        {
            try
            {
                string randString = UFS.Web.Common.CommonUtilities.GetRandomString(10);
                string shipmentId = new MSSQLReportDataOperations().GetShipmentIds(siteOrderId);
                if (!string.IsNullOrWhiteSpace(shipmentId))
                {
                    return shipmentId;
                }
                return "";
            }
            catch (Exception)
            {
                throw;
            }
        }

        [WebMethod]
        public static Dictionary<string, string> GenerateSummaryLabel(string orderids)
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                Dictionary<string, string> res = GenerateLables(orderids);
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                string finalIds = "";
                if (!string.IsNullOrWhiteSpace(orderids))
                {
                    foreach (var item in res)
                    {
                        if (item.Value.ToUpper().Contains("SUCCESSFULLY"))
                        {
                            finalIds = finalIds + "," + item.Key.Split(new char[] { '_' })[1];
                        }
                    }
                }
                if (!string.IsNullOrWhiteSpace(finalIds))
                {
                    finalIds = finalIds.Remove(0, 1);
                    new MSSQLCommonDataOperations().UpdateSummaryLabels(finalIds, usr.UserId);
                }
                return res;
            }

            {
                return new Dictionary<string, string>() { { "-1", "<span style = 'color:red' >Your session has been expired. Please Log out and Login again.</span>" } };

            }
        }

        [WebMethod]
        public static object GetSummaryNumbers()
        {
            try
            {
                if (HttpContext.Current.Session["user"] != null)
                {
                    UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                    var res = new MSSQLCommonDataOperations().GetSummaryIds(usr.UserId);
                    if (res != null && res.Count > 0)
                        return res;
                    else
                        return "-1";
                }
                else
                {
                    return "-3";
                }
            }
            catch (Exception ex)
            {
                return "-2";
            }
        }

        [WebMethod]
        public static string AddOrderToSummary(string orderId, string summaryId, bool IsMainButton)
        {
            try
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                if (usr != null)
                {
                    new MSSQLCommonDataOperations().AddOrderSummary(orderId, summaryId, usr.UserId, IsMainButton);
                    return "1";
                }
                else
                {
                    return "-1";
                }
            }
            catch (Exception)
            {

                return "-1";
            }
        }
        [WebMethod]
        public static Dictionary<string, string> GenerateLables(string orderids)
        {
            try
            {
                if (HttpContext.Current.Session["user"] != null)
                {
                    Dictionary<string, string> response = new Dictionary<string, string>();
                    if (!string.IsNullOrWhiteSpace(orderids))
                    {
                        string[] ids = orderids.Split(new char[] { ',' });
                        string BatchNo = DateTime.Now.ToString("yyyyMMddHHmmss");
                        List<string> urls = new List<string>();
                        List<string> trackingNums = new List<string>();
                        decimal amount = 0M;
                        string ordernum = "";
                        foreach (string item in ids)
                        {
                            object result = GetOrderDetail(item);

                            UFS.Web.Dashboard.MainOrder ordr = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue }.Deserialize<UFS.Web.Dashboard.MainOrder>(result.ToString());
                            if (ordr != null)
                            {
                                ordernum = ordernum + " " + ordr.Order.order_number.ToString() + ",";
                                response.Add(ordr.Order.order_number.ToString() + "_" + BatchNo, ProcessOrder(ordr.Order, BatchNo, ref urls, ref trackingNums, ref amount));
                            }
                        }
                        if (amount > 0)
                        {
                            ordernum = ordernum.TrimEnd(new char[] { ',' });
                            SendEmail(urls, BatchNo, trackingNums, amount, ordernum);
                        }
                    }
                    else
                    {
                        response.Add("0", "No order selected");
                    }

                    return response;
                }
                else
                {
                    return new Dictionary<string, string>() { { "-1", "<span style = 'color:red' >Your session has been expired. Please Log out and Login again.</span>" } };
                }
            }
            catch (Exception ex)
            {
                return new Dictionary<string, string>() { { "-2", "<span style = 'color:red' >" + ex.Message + "</span>" } };
            }
        }

        private static string ProcessOrder(UFS.Web.Dashboard.Order rawOrdr, string batchNo, ref List<string> urls, ref List<string> trackNums, ref decimal totalAmount)
        {
            try
            {
                //  urls = new List<string>();
                // trackNums = new List<string>();
                ShipmentDetail shp = ConvertObject(rawOrdr);
                EasyPost.Order ordr = CreateEshipperOrder(shp);
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                UFS.eShipper.Wrapper.eShipperWrapper wrapper = new UFS.eShipper.Wrapper.eShipperWrapper();
                string service = new MSSQLUserOperations().GetService(rawOrdr.id, usr.UserId);
                service = !string.IsNullOrWhiteSpace(service) ? service : rawOrdr.shipping_lines[0].title;
                wrapper.ServiceId = UFS.Web.Common.StaticValues.GetServiceId(service);
                wrapper.OrderNumber = rawOrdr.order_number;
                wrapper.Buy(ordr);
                if (wrapper.Messages != null && wrapper.Messages.Count > 0)
                {
                    //Task.Factory.StartNew(() => UpdateBatch(rawOrdr.id.ToString(), 0, batchNo, "", "","0.00",string.Empty));
                    //UpdateBatch(rawOrdr.id.ToString(), 0, batchNo, "", "");
                    return "<span style = 'color:red' > " + wrapper.Messages["100"] + "</span>";
                }
                else
                {
                    string labelUrls = "";
                    string trackingNums = "";
                    ordr.id = wrapper.UniqueObjectCode;
                    ordr.shipments = new List<EasyPost.Shipment>();
                    foreach (var item in wrapper.ShippingLabel)
                    {
                        ordr.shipments.Add(new EasyPost.Shipment()
                        {
                            postage_label = new EasyPost.PostageLabel()
                            {
                                label_url = item.Value
                            },
                            tracking_code = item.Key
                        });
                        labelUrls = labelUrls + " " + item.Value + ",";
                        trackingNums = trackingNums + "" + item.Key + ",";
                        totalAmount = totalAmount + decimal.Parse(wrapper.AmountCharged);
                        urls.Add(item.Value);
                        trackNums.Add(item.Key);
                    }
                    if (!string.IsNullOrWhiteSpace(labelUrls) && !string.IsNullOrWhiteSpace(trackingNums))
                    {
                        labelUrls = labelUrls.TrimEnd(new char[] { ',' });
                        trackingNums = trackingNums.TrimEnd(new char[] { ',' });
                        UpdateBatch(rawOrdr.id.ToString(), 1, batchNo, trackingNums, labelUrls, wrapper.AmountCharged, wrapper.CarrierName);
                        if (rawOrdr.fulfillments.Count > 0)
                        {
                            //UpdateShopifyLog(rawOrdr.id.ToString(), rawOrdr.fulfillments[0].id.ToString(), trackingNums, wrapper.ServiceName, wrapper.CarrierName);
                        }
                        else
                        {
                            // AddShopifyLog(rawOrdr.id.ToString(), trackingNums, wrapper.ServiceName, wrapper.CarrierName);
                        }
                        return "<span style='color:green'>Label Generated Successfully. <a href='PrintOrderLabel.aspx?data=" + rawOrdr.order_number + "' target='_blank'>Print</a></span>";
                    }
                    else
                    {
                        return "<span style='color:red'> Unable to generate labels this time. Please try again later or contact system administrator</span>";
                    }
                }
            }
            catch (Exception ex)
            {
                //Task.Factory.StartNew(() => UpdateBatch(rawOrdr.id.ToString(), 0, batchNo, "", "","0.00",string.Empty));
                return "<span style='color:green'> Unable to generate labels this time. Please try again later or contact system administrator</span>";

            }
        }

        public static void AddShopifyLog(string orderId, string trackingNums, string service, string carrier)
        {
            try
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];

                string postData = "";
                if (trackingNums.IndexOf(',') > 0)
                {
                    string trackings = "[";
                    string[] codes = trackingNums.Split(new char[] { ',' });
                    foreach (var item in codes)
                    {
                        trackings = trackings + "\"" + item + "\",";
                    }
                    trackings = trackings.TrimEnd(new char[] { ',' });
                    trackings = trackings + "]";
                    postData = "{ \"fulfillment\": { \"tracking_numbers\": \"" + trackings + "\",\"tracking_company\" :\"" + carrier + "\",\"service\": \"" + service + "\"}}";
                }
                else
                {
                    postData = "{ \"fulfillment\": { \"tracking_number\": \"" + trackingNums + "\",\"tracking_company\" :\"" + carrier + "\",\"service\": \"" + service + "\"}}";
                }

                //UpdateShopify(usr, orderId, string.Empty, postData, false, "POST");
            }
            catch (Exception ex)
            {

            }
        }

        public static void UpdateShopifyLog(string orderId, string fulfilmentId, string trackingCode, string service, string carrier)
        {
            UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];

            string postData = "";
            if (trackingCode.IndexOf(',') > 0)
            {
                string trackings = "[";
                string[] codes = trackingCode.Split(new char[] { ',' });
                foreach (var item in codes)
                {
                    trackings = trackings + "\"" + item + "\",";
                }
                trackings = trackings.TrimEnd(new char[] { ',' });
                trackings = trackings + "]";
                postData = "{ \"fulfillment\": { \"tracking_numbers\": \"" + trackings + "\",\"tracking_company\" :\"" + carrier + "\",\"service\": \"" + service + "\",\"id\":" + fulfilmentId + "}}";
            }
            else
            {
                postData = "{ \"fulfillment\": { \"tracking_number\": \"" + trackingCode + "\",\"tracking_company\" :\"" + carrier + "\",\"service\": \"" + service + "\",\"id\":" + fulfilmentId + "}}";
            }

            //UpdateShopify(usr, orderId, fulfilmentId, postData);
        }
        private static void UpdateBatch(string id, int status, string batch, string trackingCode, string labelUrl, string amount, string carr)
        {
            try
            {
                new MSSQLReportDataOperations().UpdateBatch(id, status, batch, trackingCode, labelUrl, amount, carr);
            }
            catch (Exception exs)
            {
            }
        }
        private static string GetServiceId(string title)
        {
            // return title.ToUpper().Contains("EXPEDITED") ? (title.ToUpper().Contains("ONE") ? "28" : "29") : (title.ToUpper().Contains("ECONOMY") ? "3" : "");
            return title.ToUpper().Contains("ECONOMY") ? (title.ToUpper().Contains("2") ? "29" : "3700")
                    : (title.ToUpper().Contains("EXPEDITED") ? "28" : "");
        }

        private static ShipmentDetail ConvertObject(UFS.Web.Dashboard.Order rawOrdr)
        {
            try
            {

                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                ShipmentDetail shp = new ShipmentDetail();
                shp.FromAddress = new Address()
                {
                    Name = usr.FirstName + " " + usr.LastName,
                    Company = !string.IsNullOrWhiteSpace(usr.Company) ? usr.Company : usr.FirstName + " " + usr.LastName,
                    Street1 = usr.Address.Street1,
                    Street2 = usr.Address.Street2,
                    City = usr.Address.City,
                    Country = usr.Address.Country,
                    Phone = usr.Address.FormattedPhone,
                    State = usr.Address.State,
                    Zip = usr.Address.Zip
                };
                shp.ToAddress = new Address()
                {
                    Name = rawOrdr.shipping_address.name,
                    Company = rawOrdr.shipping_address.company != null ? rawOrdr.shipping_address.company.ToString() : rawOrdr.shipping_address.name,
                    Street1 = rawOrdr.shipping_address.address1,
                    Street2 = rawOrdr.shipping_address.address2,
                    City = rawOrdr.shipping_address.city,
                    Country = rawOrdr.shipping_address.country_code,
                    Phone = !string.IsNullOrWhiteSpace(rawOrdr.shipping_address.phone) ? rawOrdr.shipping_address.phone.Replace("(", "").Replace(")", "").Replace("-", "") : shp.FromAddress.FormattedPhone,
                    State = rawOrdr.shipping_address.province_code,
                    Zip = rawOrdr.shipping_address.zip
                };
                shp.Carrier = "EShipper";
                UFS.Web.ShopifyOrder.MainOrder odr = new MSSQLUserOperations().GetShopifyOrderDetail(rawOrdr.order_number.ToString(), usr.UserId);
                if (odr != null && odr.Order != null && odr.Order.line_items != null && odr.Order.line_items.Count > 0)
                {
                    if (rawOrdr.line_items.Count == odr.Order.line_items.Count)
                    {
                        for (int i = 0; i < odr.Order.line_items.Count; i++)
                        {
                            if (rawOrdr.line_items[i].sku == odr.Order.line_items[i].sku)
                            {
                                rawOrdr.line_items[i].grams1 = odr.Order.line_items[i].grams1;
                                rawOrdr.line_items[i].quantity = odr.Order.line_items[i].quantity;
                            }
                        }
                    }
                }
                shp.Parcel = new ParcelDetails()
                {
                    CarrierCode = "EShipper",
                    CarrierName = "EShipper",
                    CODAmount = "0.00",
                    Dimension = GetDimensions(rawOrdr.line_items),
                };

                return shp;
            }
            catch (Exception)
            {

                throw;
            }
        }

        [WebMethod]
        public static Dictionary<string, UFS.Web.ShopifyOrder.MainOrder> GetOrderDetailEdit(string orderid, string ordernum)
        {
            Dictionary<string, UFS.Web.ShopifyOrder.MainOrder> res = new Dictionary<string, UFS.Web.ShopifyOrder.MainOrder>();
            if (HttpContext.Current.Session["user"] != null)
            {
                string orderDetail = GetOrderDetail(orderid).ToString();
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                UFS.Web.ShopifyOrder.MainOrder odr = new MSSQLUserOperations().GetShopifyOrderDetail(ordernum, usr.UserId);
                res.Add(orderDetail, odr);
            }
            else
            {
                res.Add("-1", null);
            }
            return res;
        }
        private static List<PackageDimensions> GetDimensions(List<LineItem> line_items)
        {
            List<PackageDimensions> dim = new List<PackageDimensions>();
            long countART, countCRG, countRBW, countDOZ103, countDOZ104, countDOZ102, countDOZ101, count, countPck101, countPck102, countPck103;

            double weightART = 0, weightCRG = 0, weightRBW = 0, weightDOZ101 = 0, weightDOZ102 = 0, weightDOZ103 = 0, weightDOZ104 = 0, weight = 0, weightPck101 = 0, weightPck102 = 0, weightPck103 = 0;
            countART = line_items.Where(m => m.sku.ToUpper().StartsWith("ART")).Sum(l => l.quantity);
            countCRG = line_items.Where(m => m.sku.ToUpper().StartsWith("CRG")).Sum(l => l.quantity);
            countRBW = line_items.Where(m => m.sku.ToUpper().StartsWith("RBW")).Sum(l => l.quantity);
            countDOZ102 = line_items.Where(m => m.sku.ToUpper().StartsWith("DOZ102")).Sum(l => (l.quantity * 12));
            countDOZ101 = line_items.Where(m => m.sku.ToUpper().StartsWith("DOZ101")).Sum(l => (l.quantity * 12));
            countDOZ103 = line_items.Where(m => m.sku.ToUpper().StartsWith("DOZ103")).Sum(l => (l.quantity * 12));
            countDOZ104 = line_items.Where(m => m.sku.ToUpper().StartsWith("DOZ104")).Sum(l => (l.quantity * 12));
            countPck101 = line_items.Where(m => m.sku.ToUpper().StartsWith("PCK101")).Sum(l => (l.quantity));
            countPck102 = line_items.Where(m => m.sku.ToUpper().StartsWith("PCK102")).Sum(l => (l.quantity));
            countPck103 = line_items.Where(m => m.sku.ToUpper().StartsWith("PCK103")).Sum(l => (l.quantity));
            for (int i = 0; i < line_items.Count; i++)
            {
                if (line_items[i].sku.ToUpper().StartsWith("ART"))
                {
                    weightART = weightART + (Convert.ToDouble(line_items[i].grams1) * line_items[i].quantity);
                }
                if (line_items[i].sku.ToUpper().StartsWith("CRG"))
                {
                    weightCRG = weightCRG + (Convert.ToDouble(line_items[i].grams1) * line_items[i].quantity);
                }
                if (line_items[i].sku.ToUpper().StartsWith("RBW"))
                {
                    weightRBW = weightRBW + (Convert.ToDouble(line_items[i].grams1) * line_items[i].quantity);
                }
                if (line_items[i].sku.ToUpper().StartsWith("DOZ101"))
                {
                    weightDOZ101 = weightDOZ101 + (Convert.ToDouble(line_items[i].grams1) * line_items[i].quantity);
                }
                if (line_items[i].sku.ToUpper().StartsWith("DOZ102"))
                {
                    weightDOZ102 = weightDOZ102 + (Convert.ToDouble(line_items[i].grams1) * line_items[i].quantity);
                }
                if (line_items[i].sku.ToUpper().StartsWith("DOZ103"))
                {
                    weightDOZ103 = weightDOZ103 + (Convert.ToDouble(line_items[i].grams1) * line_items[i].quantity);
                }
                if (line_items[i].sku.ToUpper().StartsWith("DOZ104"))
                {
                    weightDOZ104 = weightDOZ104 + (Convert.ToDouble(line_items[i].grams1) * line_items[i].quantity);

                }
                if (line_items[i].sku.ToUpper().StartsWith("PCK101"))
                {
                    weightPck101 = weightPck101 + (Convert.ToDouble(line_items[i].grams1) * line_items[i].quantity);

                }
                if (line_items[i].sku.ToUpper().StartsWith("PCK102"))
                {
                    weightPck102 = weightPck101 + (Convert.ToDouble(line_items[i].grams1) * line_items[i].quantity);

                }
                if (line_items[i].sku.ToUpper().StartsWith("PCK103"))
                {
                    weightPck103 = weightPck101 + (Convert.ToDouble(line_items[i].grams1) * line_items[i].quantity);

                }
            }

            if (countDOZ103 > 0)
            {
               
                dim.Add(new PackageDimensions
                {
                    Length = "12",
                    Width = "12",
                    Height = "5",
                    Qunatity = countDOZ103.ToString(),
                    Weight = Math.Ceiling((weightDOZ103 / countDOZ103) + 1).ToString()
                });
            }
            if (countPck101 > 0)
            {
                dim.Add(new PackageDimensions
                {
                    Length = "12",
                    Width = "12",
                    Height = "12",
                    Qunatity = countPck101.ToString(),
                    Weight = (Math.Ceiling(weightPck101) + 1).ToString()
                });
            }
            if (countPck102 > 0)
            {
                dim.Add(new PackageDimensions
                {
                    Length = "12",
                    Width = "12",
                    Height = "12",
                    Qunatity = countPck102.ToString(),
                    Weight = (Math.Ceiling(weightPck102) + 1).ToString()
                });
            }
            if (countPck103 > 0)
            {
                dim.Add(new PackageDimensions
                {
                    Length = "12",
                    Width = "12",
                    Height = "12",
                    Qunatity = countPck103.ToString(),
                    Weight = (Math.Ceiling(weightPck103) + 1).ToString()
                });
            }
            if (countDOZ104 > 0)
            {
                dim.Add(new PackageDimensions
                {
                    Length = "12",
                    Width = "12",
                    Height = "8",
                    Qunatity = countDOZ104.ToString(),
                    Weight = Math.Ceiling((weightDOZ104 / countDOZ104) + 1).ToString()
                });
            }
            //total count
            count = countART + countCRG + countRBW + countDOZ101 + countDOZ102;
            //total weight
            weight = Math.Ceiling(weightART + weightCRG + weightRBW + weightDOZ101 + weightDOZ102);
            double avgWeight = (weight) / count;
            if (count > 0)
            {
                string weightLBS = (weight + 1).ToString();
                if (count <= 10)
                {
                    dim.Add(new PackageDimensions()
                    {
                        Length = "12",
                        Width = "12",
                        Height = "6",
                        Weight = weightLBS,
                        Qunatity = "1"
                    });
                }
                if (count > 10 && count <= 16)
                {
                    dim.Add(new PackageDimensions()
                    {
                        Length = "12",
                        Width = "12",
                        Height = "8",
                        Weight = weightLBS,
                        Qunatity = "1"
                    });
                }
                if (count > 16 && count <= 32)
                {
                    dim.Add(new PackageDimensions()
                    {
                        Length = "12",
                        Width = "12",
                        Height = "12",
                        Weight = weightLBS,
                        Qunatity = "1"
                    });
                }
                long res = 0;
                long remCount = 0;
                if (count > 32)
                {
                    double wt = 0;
                    res = count % 32;
                    remCount = count / 32;
                    if (remCount > 0)
                    {
                        if (res > 16)
                        {
                            res = 0;
                            wt = Math.Ceiling(avgWeight * res) + 1;
                            dim.Add(new PackageDimensions()
                            {
                                Length = "12",
                                Width = "12",
                                Height = "12",
                                Weight = wt.ToString(),
                                Qunatity = "1"
                            });

                        }
                        else
                        {
                            weightLBS = Math.Ceiling((weight / remCount) + 1).ToString();
                            dim.Add(new PackageDimensions()
                            {
                                Length = "12",
                                Width = "12",
                                Height = "8",
                                Weight = weightLBS,
                                Qunatity = remCount.ToString()
                            });
                        }
                    }
                    if (res <= 16 && res > 10)
                    {
                        dim.Add(new PackageDimensions()
                        {
                            Length = "12",
                            Width = "12",
                            Height = "8",
                            Weight = Math.Ceiling((avgWeight * res) + 1).ToString(),
                            Qunatity = "1"
                        });
                    }
                    if (res <= 10)
                    {
                        dim.Add(new PackageDimensions()
                        {
                            Length = "12",
                            Width = "12",
                            Height = "6",
                            Weight = Math.Ceiling((avgWeight * res) + 1).ToString(),
                            Qunatity = "1"
                        });

                    }


                }
            }
            count = line_items.Where(m => m.sku.ToUpper().StartsWith("SPR")).Sum(l => l.quantity);
            if (count > 0)
            {
                decimal result = count / 3;
                for (int i = 0; i < Math.Ceiling(result); i++)
                {
                    dim.Add(new PackageDimensions()
                    {
                        Length = "10",
                        Width = "6",
                        Height = "5",
                        Weight = "5",
                        Qunatity = "1"
                    });
                }


            }


            return dim;

        }

        private static EasyPost.Order CreateEshipperOrder(ShipmentDetail shp)
        {
            EasyPost.Order ordr = new EasyPost.Order();
            ordr.to_address = new EasyPost.Address()
            {
                name = shp.ToAddress.Name,
                company = shp.ToAddress.Company,
                street1 = shp.ToAddress.Street1,
                street2 = shp.ToAddress.Street2,
                city = shp.ToAddress.City,
                state = shp.ToAddress.State,
                country = shp.ToAddress.Country,
                phone = shp.ToAddress.Phone,
                zip = shp.ToAddress.Zip,
                residential = true
            };
            ordr.from_address = new EasyPost.Address()
            {
                name = shp.FromAddress.Name,
                company = shp.FromAddress.Company,
                street1 = shp.FromAddress.Street1,
                street2 = shp.FromAddress.Street2,
                city = shp.FromAddress.City,
                state = shp.FromAddress.State,
                country = shp.FromAddress.Country,
                phone = shp.FromAddress.Phone,
                zip = shp.FromAddress.Zip
            };
            ordr.shipments = new List<EasyPost.Shipment>();
            shp.Parcels = new List<ParcelDetails>();
            foreach (var item in shp.Parcel.Dimension)
            {
                if (!string.IsNullOrWhiteSpace(item.Qunatity) && item.Qunatity != "0")
                {
                    FIllParcelsObjectForUPS(shp, item);
                    Dictionary<string, object> dcOptions = null;
                    for (int i = 0; i < Convert.ToInt32(item.Qunatity); i++)
                    {
                        dcOptions = new Dictionary<string, object>();
                        dcOptions.Add("declared_value", item.DecVal);
                        dcOptions.Add("delivery_confirmation", shp.Parcel.Signature);
                        dcOptions.Add("hold_for_pickup", shp.Parcel.HoldForPickup);
                        if (item.IsCOD)
                        {
                            dcOptions.Add("cod_amount", item.CODAmount);
                            dcOptions.Add("cod_method", item.CODMethod);
                            dcOptions.Add("currency", "USD");
                        }
                        if (item.PredefinedParcel != null && item.PredefinedParcel != "Custom")
                        {

                            ordr.shipments.Add(new EasyPost.Shipment()
                            {
                                parcel = new EasyPost.Parcel()
                                {
                                    //weight = !string.IsNullOrWhiteSpace(item.Weight) ? double.Parse(item.Weight) * 16 : 0,
                                    weight = !string.IsNullOrWhiteSpace(item.Weight) ? double.Parse(item.Weight) : 0,
                                    predefined_package = item.PredefinedParcel
                                },
                                options = dcOptions
                            });

                        }
                        else
                        {
                            ordr.shipments.Add(new EasyPost.Shipment()
                            {
                                parcel = new EasyPost.Parcel()
                                {
                                    length = !string.IsNullOrWhiteSpace(item.Length) ? double.Parse(item.Length) : 0,
                                    width = !string.IsNullOrWhiteSpace(item.Width) ? double.Parse(item.Width) : 0,
                                    height = !string.IsNullOrWhiteSpace(item.Height) ? double.Parse(item.Height) : 0,
                                    //weight = !string.IsNullOrWhiteSpace(item.Weight) ? double.Parse(item.Weight) * 16 : 0,
                                    weight = !string.IsNullOrWhiteSpace(item.Weight) ? double.Parse(item.Weight) : 0,
                                },
                                options = dcOptions
                            });
                        }
                    }
                }
            }
            ordr.reference = "OrderRef";
            UFS.eShipper.Wrapper.eShipperWrapper serv = new UFS.eShipper.Wrapper.eShipperWrapper();
            UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
            List<string> emailAddresses = new List<string>();
            if (usr != null && !string.IsNullOrWhiteSpace(usr.EmailAddress))
            {
                emailAddresses.Add(usr.EmailAddress);
                if (!string.IsNullOrWhiteSpace(usr.EmailAddress1))
                    emailAddresses.Add(usr.EmailAddress1);
                if (!string.IsNullOrWhiteSpace(usr.EmailAddress2))
                    emailAddresses.Add(usr.EmailAddress2);

                shp.EmailAddress = emailAddresses.ToArray<string>();
            }
            serv.Shipment = shp;

            return ordr;
        }
        private static void FIllParcelsObjectForUPS(ShipmentDetail shp, PackageDimensions item)
        {
            for (int i = 0; i < Convert.ToInt32(item.Qunatity); i++)
            {
                shp.Parcels.Add(new ParcelDetails()
                {
                    CarrierCode = shp.Parcel.CarrierCode,
                    CarrierName = shp.Parcel.CarrierName,
                    CODAmount = item.CODAmount,
                    CODMethod = item.CODMethod,
                    IsCOD = item.IsCOD,
                    DeclaredValue = item.DecVal,
                    Height = item.Height,
                    HoldForPickup = shp.Parcel.HoldForPickup,
                    Length = item.Length,
                    PredefinedParcel = item.PredefinedParcel,
                    Signature = shp.Parcel.Signature,
                    Weight = item.Weight,
                    Width = item.Width

                });
            }
        }
        [WebMethod]
        public static string UpdateOrders(List<SummaryData> dimension, string order, string service)
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                string res = new MSSQLUserOperations().UpdateShopifyOrderDetail(dimension, order, service, usr.UserId);
                if (res == "0")
                    return "Data updated successfully";
                if (res == "-1")
                    return "Unable to update data this time. Please try again later";
                if (res == "1")
                    return "Unable to perform operation due to insufficient data. Try with different orders.";

                return "Unknown error occured. Contact system admin";
            }
            else
            {
                //HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                //return "";
                return "You session has been expired. Please login again to proceed";
            }
        }

        private static void SendEmail(List<string> imageUrl, string batchNum, List<string> trackingNums, decimal amount, string ids)
        {
            try
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                EmailService email = new EmailService("", usr.EmailAddress);
                Dictionary<Stream, string> lstStream = new Dictionary<Stream, string>();
                email.Attachments = new Dictionary<Stream, string>();
                email.CCMailAddressList = new List<System.Net.Mail.MailAddress>();
                if (!string.IsNullOrWhiteSpace(usr.EmailAddress1))
                    email.CCMailAddressList.Add(new System.Net.Mail.MailAddress(usr.EmailAddress1));

                if (!string.IsNullOrWhiteSpace(usr.EmailAddress2))
                    email.CCMailAddressList.Add(new System.Net.Mail.MailAddress(usr.EmailAddress2));

                foreach (var item in imageUrl)
                {

                    Stream strm = null;
                    if (item.Contains("../Labels/"))
                    {
                        strm = new FileStream(HttpContext.Current.Server.MapPath(item.Replace("..", "~")), FileMode.Open, FileAccess.Read);
                    }
                    if (strm != null)
                    {
                        string[] fil = item.Split(new char[] { '.' });
                        string extn = fil[fil.Length - 1].ToString();
                        lstStream.Add(strm, extn);
                    }
                }
                email.Attachments = lstStream;
                email.MailBody = "<img src='http://www.eclickship.com/images/logo-name.png' alt='logo'><p>Hello " + usr.FirstName + " " + usr.LastName + " !</p> <br/><p>Find the attached label and below detail of your created on eClickShip <br/></p><p>" +
                    "<b>Tracking Numbers : </b><br/>" + (trackingNums != null && trackingNums.Count > 0 ? String.Join(",", trackingNums.Select(x => x.ToString()).ToArray()) : "Not generated.") + "</p><br/><p>" +
                    "<b>Order Numbers : </b><br/>" + (ids) + "<br/></p><p>" +
                    "<b>Batch Number :</b><br/>" + batchNum + "<br/></p><p>";
                //  "Total Amount chargs : " + amount.ToString("0.00") + "</p>";

                if (WebConfigurationManager.AppSettings["LabelSubject"] != null)
                {
                    email.Subject = WebConfigurationManager.AppSettings["LabelSubject"].ToString();// "Package Details Created on United Freight Savers";
                }
                else
                {
                    email.Subject = "Package Details Created on eClickShip : Batch# " + batchNum;
                }
                string resp = email.SendMail();

            }
            catch (Exception ex)
            {
                // dvResult1.InnerHtml = "Unale to Send Mail. Error is : " + ex.Message;
            }
        }

        private Stream DownloadData(string url)
        {
            try
            {
                WebRequest req = WebRequest.Create(url);
                WebResponse response = req.GetResponse();
                return response.GetResponseStream();
            }
            catch (Exception ex)
            {
                //dvResult1.InnerHtml = "Error while downloading Label. Error is : " + ex.Message;
                return null;
            }
        }
    }


}