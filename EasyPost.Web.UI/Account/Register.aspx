﻿<%@ Page Title="Register" Language="C#" EnableEventValidation="false" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="EasyPostDemo.Account.Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <style>
        input.text {
            width: 75%;
        }

        .ui-dialog .ui-state-error {
            padding: .3em;
        }

        .validateTips {
            border: 1px solid transparent;
            padding: 0.3em;
        }

        table, tr, td, th {
            vertical-align: middle;
            border-width: 0px;
            border-style: none;
        }
    </style>
    <link href="../css/table.css" rel="stylesheet" />
    <p class="validation-summary-errors">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <table class="customers">
        <tr>
            <th colspan="4">
                <div runat="server" id="dvTitle">
                    Fill the below form to register
                </div>
            </th>
        </tr>
        <tr>
            <td style="width: 15%">Email Address
            </td>
            <td style="width: 35%">
                <asp:TextBox runat="server" ID="Email" CssClass="fo-input" /><asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="field-validation-error" Text="*" ErrorMessage="The email address field is required." /><asp:RegularExpressionValidator runat="server" ControlToValidate="Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        CssClass="field-validation-error" Text="*" ErrorMessage="Invalid Email Address"></asp:RegularExpressionValidator>
            </td>
            <td style="width: 15%">Street 1
            </td>
            <td style="width: 35%">
                <asp:TextBox runat="server" ID="Street1" CssClass="fo-input" /><asp:RequiredFieldValidator runat="server" ControlToValidate="Street1"
                    CssClass="field-validation-error" Text="*" ErrorMessage="The Street1 field is required." />
            </td>
        </tr>
        <tr>
            <td>First Name</td>
            <td>
                <asp:TextBox runat="server" ID="FirstName" CssClass="fo-input" /><asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                    CssClass="field-validation-error" Text="*" ErrorMessage="The first name field is required." /></td>
            <td>Street 2</td>
            <td>
                <asp:TextBox runat="server" ID="Street2" CssClass="fo-input" /></td>
        </tr>
        <tr>
            <td>Last Name</td>
            <td>
                <asp:TextBox runat="server" ID="LastName" CssClass="fo-input" /><asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                    CssClass="field-validation-error" Text="*" ErrorMessage="Last is required." /></td>
            <td>City</td>
            <td>
                <asp:TextBox runat="server" ID="City" CssClass="fo-input" /><asp:RequiredFieldValidator runat="server" ControlToValidate="City"
                    CssClass="field-validation-error" Text="*" Display="Dynamic" ErrorMessage="The City field is required." /></td>
        </tr>
        <tr>
            <td>Company Name</td>
            <td>
                <asp:TextBox runat="server" ID="Company" CssClass="fo-input" /></td>
            <td>State</td>
            <td>
                <asp:TextBox runat="server" ID="State" CssClass="fo-input" /><asp:RequiredFieldValidator runat="server" ControlToValidate="State"
                    CssClass="field-validation-error" Text="*" ErrorMessage="The State field is required." /></td>
        </tr>
        <tr>
            <td>Alternate Email 1</td>
            <td>
                <asp:TextBox runat="server" ID="EmailAddress1" CssClass="fo-input" /><asp:RegularExpressionValidator runat="server" ControlToValidate="EmailAddress1" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    CssClass="field-validation-error" Text="*" ErrorMessage="Invalid Alternate Email Address 1"></asp:RegularExpressionValidator></td>
            <td>Zip</td>
            <td>
                <asp:TextBox runat="server" ID="Zip" CssClass="fo-input" /><asp:RequiredFieldValidator runat="server" ControlToValidate="Zip"
                    CssClass="field-validation-error" Text="*" ErrorMessage="The Zip field is required." /><asp:RegularExpressionValidator ControlToValidate="Zip" ID="rgexZip" runat="server" Text="*" ErrorMessage="Invalid Zip Code" SetFocusOnError="true" ValidationExpression="\d{5}(-\d{4})?"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>Alternate Email 2</td>
            <td>
                <asp:TextBox runat="server" ID="EmailAddress2" CssClass="fo-input" /><asp:RegularExpressionValidator runat="server" ControlToValidate="EmailAddress2" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    CssClass="field-validation-error" Text="*" ErrorMessage="Invalid Alternate Email Address 2"></asp:RegularExpressionValidator></td>
            <td>Country</td>
            <td>
                <asp:TextBox runat="server" Text="US" Enabled="false" MaxLength="2" ID="Country" CssClass="fo-input" /><asp:RequiredFieldValidator runat="server" ControlToValidate="Country"
                    CssClass="field-validation-error" Text="*" ErrorMessage="The country field is required." /></td>
        </tr>
        <tr>
            <td>Authorization Details</td>
            <td runat="server" id="tdAuth"><a href="#" id="lnkAuthorization" class="dkLink" runat="server"> Set Authorization Details</a></td>
            <td>Phone</td>
            <td>
                <asp:TextBox runat="server" ID="Phone" CssClass="fo-input" /><asp:RegularExpressionValidator ControlToValidate="Phone" ID="regexPhone" runat="server" ErrorMessage="Invalid Phone  Number" Text="*" ValidationExpression="^\(\d{3}\) ?\d{3}( |-)?\d{4}|^\d{3}( |-)?\d{3}( |-)?\d{4}"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <div id="dvPass1" runat="server">
                <td>Password
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Password" CssClass="fo-input" TextMode="Password" /><asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                        CssClass="field-validation-error" Text="*" ErrorMessage="The password field is required." />
                </td>
            </div>
            <div id="dvPass2" runat="server">
                <td>Confirm Password
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="fo-input" ID="ConfirmPassword" TextMode="Password" /><asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                        CssClass="field-validation-error" Text="*" Display="Dynamic" ErrorMessage="The confirm password field is required." /><asp:CompareValidator runat="server" Text="*" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                            CssClass="field-validation-error" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
                </td>
            </div>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td style="text-align: right">
                <input type="reset" value="Reset" id="btnReset" class="re-button" style="width: 100%;" />
            </td>
            <td style="text-align: left">
                <asp:Button ID="btnRegister" runat="server" OnClick="btnRegister_Click" Text="Register" CssClass="re-button" Style="width: 45%;" />
            </td>
        </tr>
    </table>
    <asp:ValidationSummary runat="server" ShowMessageBox="true" ShowSummary="false" DisplayMode="BulletList" />
    <script>
        $(document).ready(function () {
            $("#reg").addClass("pagebutton");
            $("#aUser").addClass("pagebutton");
            GetDatas();
            $(".dkLink").click(function () {
                dialog.dialog("open");
            });
            var dialog;
            var form;
            var name;
            var email;
            var password;
            allFields = $([]).add(name).add(email).add(password),
           tips = $(".validateTips");
            function updateTips(t) {
                tips
                  .text(t)
                  .addClass("ui-state-highlight");
                setTimeout(function () {
                    tips.removeClass("ui-state-highlight", 1500);
                }, 500);
            }

            function checkLength(o, n, min, max) {
                if (o.val().length > max || o.val().length < min) {
                    o.addClass("ui-state-error");
                    updateTips("Length of " + n + " must be between " +
                      min + " and " + max + ".");
                    return false;
                } else {
                    return true;
                }
            }
            function addUser() {
                var data = GetAuthData();
                $.ajax({
                    type: "POST",
                    url: "Register.aspx/SaveAuthDetails",
                    data: JSON.stringify({ 'AuthData': data }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (msg) {
                        if (msg.d != "0") {
                            alert("There is some issue with saving authentication detail. Please contact System Admin");
                        }
                    },
                    error: function (x, e) { alert(x.responseText); }
                });
                dialog.dialog("close");
                return true;
            }
            function GetDatas() {
                $.ajax({
                    type: "POST",
                    url: "Register.aspx/GetAuthData",
                    // data: JSON.stringify({ 'AuthData': data }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (msg) {
                        debugger;
                        $("#txtKey").val(msg.d.AuthKey);
                        $("#txtAPIKey").val(msg.d.APIKey);
                        $("#txtApiPassword").val(msg.d.APIPassword);
                        $("#txtStoreUrl").val(msg.d.StoreURL);
                    },
                    error: function (x, e) { console.log(x.responseText); }
                });
            }
            function GetAuthData() {
                var authData = {
                    AuthKey: $("#txtKey").val(),
                    APIKey: $("#txtAPIKey").val(),
                    APIPassword: $("#txtApiPassword").val(),
                    StoreURL: $("#txtStoreUrl").val()
                }
                return authData;
            }


            dialog = $("#dialog-form").dialog({
                autoOpen: false,
                height: 350,
                width: 750,
                modal: true,
                buttons: {
                    "Set Authorization Details": addUser,
                    Cancel: function () {
                        dialog.dialog("close");
                    }
                },
                close: function () {
                    //  form[0].reset();
                    allFields.removeClass("ui-state-error");
                }
            });
            form = dialog.find("form").on("submit", function (event) {
                event.preventDefault();
                addUser();
            });

        });

    </script>

    <div id="dialog-form" title="Authorization Detail">
        <p class="validateTips"></p>
        <table class="customers">
            <tr>
                <td style="width:30%">
                    <label for="name">API Authorization Key</label>

                </td>
                <td style="width:70%">
                    <input type="text" id="txtKey" class="text ui-widget-content ui-corner-all" required="required" /></td>
            </tr>
            <tr>
                <td>
                    <label for="email">Shopify API Key</label>
                </td>
                <td>
                    <input type="text" id="txtAPIKey" class="text ui-widget-content ui-corner-all" />
            </tr>
            <tr>
                <td>
                    <label for="AccessLicenseNumber">Shopify API Password</label>

                </td>
                <td>
                    <input type="text" id="txtApiPassword" class="text ui-widget-content ui-corner-all" />
            </tr>
            <tr>
                <td>
                    <label for="ShipperNumber">Shopify Store URL</label>

                </td>
                <td>
                    <input type="text" id="txtStoreUrl" class="text ui-widget-content ui-corner-all" />
            </tr>
        </table>



        <input type="submit" tabindex="-1" style="position: absolute; top: -1000px">
    </div>
</asp:Content>
