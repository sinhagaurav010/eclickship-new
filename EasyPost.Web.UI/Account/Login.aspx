﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="EasyPostDemo.Account.Login1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="login-left" style="margin:0 auto;width:400px;background-color:#f5fafe;margin-top:60px;border-radius:10px;">
        <asp:Literal runat="server" ID="FailureText"></asp:Literal>
        <div style="height:40px;background-color:#19548C;color:#fff;text-align:center;padding-top:8px;font-size:16px;">LOGIN</div>
         <div style="padding:40px;">
             <div >
            <label style="font-size:12px;font-weight:bold;color:#666;">Email Address</label><br>
            <asp:TextBox ID="UserName" runat="server" CssClass="fo-input" style="width:100%;background-color:#fff !important;font-size:12px;border-radius:2px;"></asp:TextBox>
        </div>
        <div style="padding-top: 10px; padding-bottom: 10px;">
            <label style="font-size:12px;font-weight:bold;color:#666;">Password</label><br>
            <asp:TextBox ID="Password" CssClass="fo-input" runat="server" TextMode="Password" style="width:100%;color:#9ba0a4; background-color:#fff;font-size:12px;border-radius:2px;"></asp:TextBox>
        </div>
        <div style="padding-bottom:10px;font-size:12px;">
       <input type="checkbox" name="checkbox" style="margin:0;"/> Remember me? <asp:CheckBox ID="chkSync" runat="server" Checked="true" Text="Sync Shopify Data on Login" />
      </div>
        <div style="float:right;">
            <asp:Button ID="btnLogin" runat="server" CssClass="fo-button" Text="Login" OnClick="btnLogin_Click" style="width:100px;background-color:#1f1f1f;border-radius:2px;"/>
               <asp:Button ID="btnRegister" runat="server" CssClass="fo-button" Text="Register" OnClick="btnRegister_Click" style="width:100px;background-color:#1f1f1f;border-radius:2px;"/>
        </div>
         </div>
        <div style="margin-top:20px; clear:both; height:40px;background-color:#dfe6ee;color:#1f1f1f;text-align:left;padding-top:8px;font-size:12px;padding-left:40px;">
            
            <a href="~/Account/ForgotPwd" runat="server" style="color:#666;font-weight:bold;">Forgot Password ?</a>

        </div>
    </div>
   <%-- <div class="col-lg-6 login-right">
        <img src="<%=Page.ResolveClientUrl("~/Images/login.JPG")%>" height="300" alt="" />
    </div>--%>
    <script>
        $(document).ready(function () {
            $("#log").addClass("pagebutton");
        });
    </script>
</asp:Content>
