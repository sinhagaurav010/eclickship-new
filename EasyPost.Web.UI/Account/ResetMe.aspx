﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResetMe.aspx.cs" Inherits="EasyPostDemo.Account.ResetMe" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="dvMain" runat="server">
     <p class="validation-summary-errors">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:ValidationSummary ID="valSumm" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <div class="col-md-12 re-heading" runat="server" id="dvTitle">
        Reset Password
    </div>
    <div class="col-md-12">
        <div class="col-lg-6 login-left">
            <div class="row re-div" id="dvPass1" runat="server">
                <div class="col-md-3">
                    <label>Enter New Password</label>
                </div>
                <div class="col-md-9">
                    <asp:TextBox runat="server" ID="Password" CssClass="fo-input" TextMode="Password" /><asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                        CssClass="field-validation-error" Text="*" ErrorMessage="The password field is required." />
                </div>
            </div>
            <div class="row re-div" id="dvPass2" runat="server">
                <div class="col-md-3">
                    <label>Confirm Password</label>
                </div>
                <div class="col-md-9">
                    <asp:TextBox runat="server" CssClass="fo-input" ID="ConfirmPassword" TextMode="Password" /><asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                        CssClass="field-validation-error" Text="*" Display="Dynamic" ErrorMessage="The confirm password field is required." /><asp:CompareValidator runat="server" Text="*" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                            CssClass="field-validation-error" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-6 login-left">
            <div class="row re-div">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <div class="col-md-5">
                        <input type="reset" name="btnReset" class="re-button" value="Clear" style="width:90%" />
                    </div>
                    
                    <div class="col-md-5">
                        
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" CssClass="re-button" Style="width: 90%;" />

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>
