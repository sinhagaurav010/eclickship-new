﻿using UFS.Web.DataOperations;
using UFS.Web.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EasyPostDemo.Account
{
    public partial class ResetMe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString!=null)
                {
                    string rand = Request.QueryString["iam"];
                    string userIdEnc = Request.QueryString["uare"];
                    int correctId =Convert.ToInt32(DecryptionService.DecryptByBase64String(userIdEnc));
                    if (!new MSSQLUserOperations().IsResetCodeExists(correctId, rand))
                    {
                        dvMain.InnerHtml = "Expired link or data does not exists";
                    }
                }
                else
                {
                    dvMain.InnerHtml = "You are not authorize to access this page.";
                }
            }
            catch (Exception ex)
            {
                dvMain.InnerHtml = "Sorry. We are not able to process you resquest. Error is " + ex.Message;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                 string userIdEnc = Request.QueryString["uare"];
                    int correctId =Convert.ToInt32(DecryptionService.DecryptByBase64String(userIdEnc));
                    if(new MSSQLUserOperations().UpdatePassword(Password.Text, correctId)){
                        dvMain.InnerHtml = "<span style='color:green'> Your Password has been changed successfully. Please login to the system with your new password. ";
                    }
            }
            catch (Exception x)
            {
                dvMain.InnerHtml = x.Message;
            }
        }
    }
}