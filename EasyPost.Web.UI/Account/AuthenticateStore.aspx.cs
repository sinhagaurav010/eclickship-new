﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UFS.Web.UI.Account
{
    public partial class AuthenticateStore : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString != null && Request.QueryString.Count > 0)
                {
                    var redirectURL = "https://{0}.myshopify.com/admin/oauth/authorize?client_id={1}&scope={2}&redirect_uri={3}&state={4}";
                    var redirecturi = "http://localhost:62864/Account/Register.aspx";
                    string.Format(redirectURL, new object[] { Request.QueryString["shop"], "", "scope=write_orders,read_customers",redirecturi,Request.QueryString["timestamp"] });
                    Response.Redirect("https://"+ Request.QueryString["shop"] .ToString()+ ".myshopify.com/admin/oauth/authorize?client_id=df82cc28c1804065c5ada08b4bd67e93&scope=write_orders,read_customers&redirect_uri=" + redirecturi);
                }
            }
            catch (Exception)
            {
                
            }
        }
    }
}