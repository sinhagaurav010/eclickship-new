﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Microsoft.AspNet.Membership.OpenAuth;
using UFS.Web.BusinesEntities;
using UFS.Web.DataOperations;
using System.Web.Services;
using System.Web.Script.Serialization;

namespace EasyPostDemo.Account
{
    public partial class Register : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    string isEdit = Request.QueryString != null && Request.QueryString["edit"] != null ? Request.QueryString["edit"] : "";
                    if (!string.IsNullOrWhiteSpace(isEdit))
                    {
                        EditAccount();
                    }
                    if (Request.QueryString["hmac"] != null && Request.QueryString["shop"] != null && Request.QueryString["code"] != null)
                    {
                        ShopifyAuthentication Auth = new ShopifyAuthentication
                        {
                            StoreURL = Request.QueryString["shop"].ToString(),
                            AuthKey = Request.QueryString["code"].ToString()
                        };
                        GetSecretKey(Auth);
                        AutoFillForm(Auth);
                        HttpContext.Current.Session["AuthData"] = Auth;
                        lnkAuthorization.Visible = false;
                        tdAuth.InnerHtml = "Already Set";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.Text = ex.Message;
            }
        }

        private void GetSecretKey(ShopifyAuthentication auth)
        {
            Shopify.Web.Interaction.RestServiceAgent agent = new Shopify.Web.Interaction.RestServiceAgent();
            string tokenUrl = "https://" + auth.StoreURL + "/admin/oauth/access_token";
            agent.Request = new Shopify.Web.Interaction.RestRequest { RequestURL = tokenUrl };
            agent.GetAccessToken(auth.AuthKey);
            auth.AuthKey = agent.AccessKey;
        }

        private void AutoFillForm(ShopifyAuthentication auth)
        {
            try
            {
                string data = GetServiceData(auth);
                if (!string.IsNullOrWhiteSpace(data))
                {
                    ShopDetail response = new JavaScriptSerializer().Deserialize<ShopDetail>(data);
                    if (response != null && response.shop != null)
                    {
                        Email.Text = response.shop.email;

                        // Email.Enabled = false;
                        FirstName.Text = response.shop.shop_owner;
                        Company.Text = response.shop.name;
                        Street1.Text = response.shop.address1;
                        Street2.Text = response.shop.address2;
                        City.Text = response.shop.city;
                        State.Text = response.shop.province;
                        Zip.Text = response.shop.zip;
                        Country.Text = response.shop.country_name;
                        Phone.Text = response.shop.phone;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        private static string GetServiceData(ShopifyAuthentication auth)
        {
            Shopify.Web.Interaction.RestServiceAgent agent = new Shopify.Web.Interaction.RestServiceAgent();
            string fullUrl = "https://" + auth.StoreURL + "/admin/shop.json";
            string tokenUrl = "https://" + auth.StoreURL + "/admin/oauth/access_token";
            //agent.Request = new Shopify.Web.Interaction.RestRequest { RequestURL = tokenUrl };
            // agent.GetAccessToken(auth.AuthKey);
            agent.Request = new Shopify.Web.Interaction.RestRequest()
            {
                RequestAuthorizationHeader = new Shopify.Web.Interaction.KeyValuePair()
                {
                    Key = "X-Shopify-Access-Token",
                    Value = auth.AuthKey,
                },
                RequestURL = fullUrl
            };

            string data = agent.MakeGetRequestAsync();
            return data;
        }
        private void EditAccount()
        {
            if (Session["user"] != null)
            {
                UserDetail uDetail = (UserDetail)Session["user"];
                Email.Text = uDetail.EmailAddress;
                Email.Enabled = false;
                FirstName.Text = uDetail.FirstName;
                LastName.Text = uDetail.LastName;
                Company.Text = uDetail.Company;
                if (uDetail.Address != null)
                {
                    Street1.Text = uDetail.Address.Street1;
                    Street2.Text = uDetail.Address.Street2;
                    City.Text = uDetail.Address.City;
                    State.Text = uDetail.Address.State;
                    Zip.Text = uDetail.Address.Zip;
                    Country.Text = uDetail.Address.Country;
                    Phone.Text = uDetail.Address.Phone;
                    EmailAddress1.Text = uDetail.EmailAddress1;
                    EmailAddress2.Text = uDetail.EmailAddress2;
                    //txtKey.Text = uDetail.AuthCode;
                    //txtAPIKey.Text = uDetail.ShopifyApiKey;
                    //txtApiPassword.Text = uDetail.ShopifyApiPassword;
                    //txtStoreUrl.Text = uDetail.ShpifyStoreUrl;
                }
                btnRegister.Text = "Update Profile";
                dvTitle.InnerHtml = "Update your profile and Address";
                dvPass1.Visible = false;
                dvPass2.Visible = false;
            }
            else
            {
                dvPass1.Visible = true;
                dvPass2.Visible = true;
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                UserDetail usr = new UserDetail()
                {
                    EmailAddress = Email.Text.Trim(),
                    Password = Password.Text,
                    FirstName = FirstName.Text,
                    LastName = LastName.Text,
                    Company = Company.Text,
                    EmailAddress1 = EmailAddress1.Text,
                    EmailAddress2 = EmailAddress2.Text,
                    //AuthCode = txtKey.Text,
                    //ShopifyApiKey = txtAPIKey.Text,
                    //ShopifyApiPassword = txtApiPassword.Text,
                    //ShpifyStoreUrl = txtStoreUrl.Text,                
                    //  EasyPostFedExKey = txtEasyPostFedexKey.Text,
                    Address = new UserAddress()
                    {
                        Street1 = Street1.Text,
                        Street2 = Street2.Text,
                        City = City.Text,
                        State = State.Text,
                        Zip = Zip.Text,
                        Country = Country.Text,
                        Phone = Phone.Text
                    }
                };

                if (Session["AuthData"] != null)
                {
                    usr.AuthDetails = Session["AuthData"] as ShopifyAuthentication;
                }

                if (Session["user"] != null)
                {
                    usr.IsUpdate = true;
                }
                else
                {
                    usr.IsUpdate = false;
                }
                MSSQLUserOperations usrOp = new MSSQLUserOperations();
                string msg = "";
                if (usrOp.AddUser(usr, ref msg))
                {
                    // UserDetail ssUser = usrOp.ValidateLogin(Email.Text, Password.Text);
                    if (!usr.IsUpdate)
                    {
                        // Session["user"] = ssUser;
                        Response.Redirect("~/Account/AccountCreated.aspx");
                    }
                    else
                    {
                        Response.Write("<script type='text/javascript'>alert('Account has been updated. Please logout and login again to see the changes.')</script>");
                    }
                }
                else
                {
                    ErrorMessage.Text = msg;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.Text = ex.Message;
            }
        }

        [WebMethod]
        public static string SaveAuthDetails(ShopifyAuthentication AuthData)
        {
            try
            {
                HttpContext.Current.Session["AuthData"] = AuthData;
                return "0";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod]
        public static ShopifyAuthentication GetAuthData()
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                UserDetail usr = HttpContext.Current.Session["user"] as UserDetail;
                if (usr.AuthDetails != null)
                    return usr.AuthDetails;
            }
            return new ShopifyAuthentication();
        }
    }
}