﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UFS.Web.BusinesEntities;
using UFS.Web.DataOperations;

namespace UFS.Web.UI.Account
{
    public partial class ClearMe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                if (usr!=null && usr.RoleId == "1")
                {
                    new MSSQLUserOperations().ClearOrders();
                    Response.Write("<script type='text/javascript'>window.location.href='../Dashboard/Default.aspx'</script>");
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
}