﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Terms.aspx.cs" Inherits="EasyPostDemo.Terms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="margin-bottom: 0px; color: #0089da">
        <h3>Terms & Conditions</h3>
    </div>
    <div style="width: 100%; text-align: justify">
        <div style="width: 100%; text-align: center">
            <p>
                <h4>Terms & Conditions</h4>
                <br />
                <h3>TERMS OF USE AND PRIVACY POLICY<br />
                    UNITED FREIGHT SAVERS TERMS OF USE</h3>
            </p>
        </div>
        <b>Last Updated: January 26, 2015</b><br />
        <ol>
            <li><b>1. Acceptance of Terms.</b><ol>
                <li>
                    <br />
                    1.1. UnitedFreightSavers.com, or (“UFS”), provides its Service (as defined below) to you, subject to this Terms of Service agreement (“TOS”). By accepting this TOS or by accessing or using the Services or our website located at http://www.unitedfreightsavers.com  (the “Site”), you recognize that you have read, understood, and agree to be bound by this TOS. If you are entering into this TOS on behalf of a company, business or other legal entity, you represent that you have the authority to bind such entity and its affiliates to this TOS, in which case the terms “you” or “your” shall refer to such entity and its affiliates. If you do not have such authority, or if you do not agree with this TOS, you must not accept this TOS and may not use the Service.</li>
                <li>
                    <br />
                    1.2. We reserve the right, at our sole discretion, to change or modify portions of this TOS at any time. If we do this, we will post the changes on this page and will indicate at the top of this page the date these terms were last revised. We will also notify you, either through the Services user interface, in an email notification or through other reasonable means. Any such changes will become effective no earlier than fourteen (14) days after they are posted, except that changes addressing new functions of the Services or changes made for legal reasons will be effective immediately. Your continued use of the Service after the date any such changes become effective constitutes your acceptance of the new TOS.</li>
                <li>
                    <br />
                    1.3. You may be required to register with UFS in order to access and use certain features of the Service. If you choose to register for the Service, you agree to provide and maintain true, accurate, current and complete information about yourself as prompted by the Service’s registration form. Registration data and certain other information about you are governed by our Privacy Policy. If you are under 13 years of age, you are not authorized to use the Service, with or without registering. In addition, if you are under 18 years old, you may use the Service, with or without registering, only with the approval of your parent or guardian.</li>
                <li>
                    <br />
                    1.4. You are responsible for maintaining the confidentiality of your password and account, if any, and are fully responsible for any and all activities that occur under your password or account. You agree to (a) immediately notify UFS of any unauthorized use of your password or account or any other breach of security, and (b) ensure that you exit from your account at the end of each session when accessing the Service. UFS will not be liable for any loss or damage arising from your failure to comply with this Section.</li>
                <li>
                    <br />
                    1.5. In addition, when using certain services, you will be subject to any additional terms applicable to such services that may be posted on the Service from time to time, including, without limitation, the Privacy Policy located at www.unitedfreightsaver.com  All such terms are hereby incorporated by reference into this TOS.</li>
            </ol>
            </li>
            <li>
                <br />
                <b>2. Description of Service.</b> The “Service” includes (a) the Site, (b) UFS’s packaging and shipping services, and (c) all software (including the Software, as defined below), data, reports, text, images, sounds, video, and content made available through any of the foregoing (collectively referred to as the “Content”). Any new features added to or augmenting the Service are also subject to this TOS.
            <br />
            </li>
            <li>
                <br />
                <b>3. General Conditions/ Access and Use of the Service.</b>
                <ol>
                    <li>3.1. Subject to the terms and conditions of this TOS, you may access and use the Service only for lawful purposes. Your use of the Service is also subject to UFS’s Service Guide, available at request for UFS office, which is incorporated by reference into this TOS. All rights, title and interest in and to the Service and its components will remain with and belong exclusively to UFS. You shall not<br />
                        <ul>
                            <li>(a) sublicense, resell, rent, lease, transfer, assign, time share or otherwise make the Service available to any third party, except as set forth in Section 11;
                            </li>
                            <li>(b) use the Service in any unlawful manner (including without limitation in violation of any data, privacy or export control laws) or in any manner that interferes with or disrupts the integrity or performance of the Service or its components or otherwise violates our AUP (as defined below), or 
                            </li>
                            <li>(c) modify, adapt or hack the Service to, or otherwise attempt to gain unauthorized access to the Service or its related systems or networks. You shall comply with any codes of conduct, policies or other notices UFS provides you or publishes in connection with the Service, and you shall promptly notify UFS if you learn of a security breach related to the Service.
                            </li>
                        </ul>
                    </li>
                    <li>
                        <br />
                        3.2. You may not tender for shipment any of the prohibited items listed in UFS’s Service Guide, including, but not limited to, alcoholic beverages, firearms, hazardous materials, tobacco products and any other items that are prohibited by law and such items will not be accepted. Our carriers may inspect your shipment at any time and may permit government authorities to carry out such inspections as they may consider appropriate. Carriers may also photograph items in your shipment for internal use in order to provide the Services. We/Our Carriers reserve the right to reject or suspend the carriage of any prohibited items or one that contains materials that may damage other shipments or that may constitute a risk to our equipment or employees or to those of our service providers.
                    </li>
                    <li>
                        <br />
                        3.3. In addition to (and without limiting the generality of) the other terms and conditions of this TOS, you hereby agree to comply with UFS’s acceptable use policy (“AUP”), as described in this section. You will not use the Service to (or assist another person to):
                   
                        <br />
                        a) (i) poses or creates a privacy or security risk to any person;
                        <br />
                        (ii) is unlawful, harmful, threatening, abusive, harassing, tortious, excessively violent, defamatory, vulgar, obscene, pornographic, libelous, invasive of another’s privacy, hateful racially, ethnically or otherwise objectionable; or 
              <br />
                        (iii) in the sole judgment of UFS, is objectionable or which restricts or inhibits any other person from using or enjoying the Service, or which may expose UFS or its users to any harm or liability of any type;
                   
                        <br />
                        b) interfere with or disrupt the Service or servers or networks connected to the Service, or disobey any requirements, procedures, policies or regulations of networks connected to the Service; or
                        <br />
                        c) Violate any applicable local, state, national or international law, or any regulations having the force of law;<br />
                        d) Impersonate any person or entity, or falsely state or otherwise misrepresent your affiliation with a person or entity; or<br />
                        e) Obtain or attempt to access or otherwise obtain any materials or information through any means not intentionally made available or provided for through the Service.<br />
                    </li>
                    <li>
                        <br />
                        3.4. Any software that may be made available by UFS in connection with the Service, including without limitation the bookmarklets or plug-ins, (“Software”) contains proprietary and confidential information that is protected by applicable intellectual property and other laws. Subject to the terms and conditions of this TOS, UFS hereby grants you a non-transferable, non-sublicensable and non-exclusive right and license to use the object code of any Software solely in connection with the Service, provided that you shall not (and shall not allow any third party to) copy, modify, create a derivative work of, reverse engineer, reverse assemble or otherwise attempt to discover any source code or sell, assign, sublicense or otherwise transfer any right in any Software. You agree not to access the Service by any means other than through the interface that is provided by UFS for use in accessing the Service. Any rights not expressly granted herein are reserved and no license or right to use any trademark of UFS or any third party is granted to you in connection with the Service.
                    </li>
                    <li>
                        <br />
                        3.5. You are solely responsible for all data, information, feedback, suggestions, text, content and other materials that you upload, post, deliver, provide or otherwise transmit or store (hereafter “post(ing)”) in connection with or relating to the Service (“Your Content”).You are responsible for maintaining the confidentiality of your login, password and account and for all activities that occur under your login or account. UFS reserves the right to access your account in order to respond to your requests for technical support. By posting Your Content on or through the Service, you hereby do and shall grant UFS a worldwide, non-exclusive, perpetual, irrevocable, royalty-free, fully paid, sublicensable and transferable license to use, modify, reproduce, distribute, display, publish and perform Your Content in connection with the Service and to improve UFS’s products and services, subject to the terms and conditions of this TOS and our Privacy Policy. UFS has the right, but not the obligation, to monitor the Service, Content, or Your Content and to disclose your Your Content if required to do so by law or in the good faith belief that such action is necessary to (i) comply with a legal obligation, (ii) protect and defend the rights or property of UFS, (iii) act in urgent circumstances to protect the personal safety of users of the Services or the public, or (iv) protect against legal liability. You further agree that UFS may remove or disable any Content at any time for any reason (including, but not limited to, upon receipt of claims or allegations from third parties or authorities relating to such Content), or for no reason at all.
                    </li>
                    <li>
                        <br />
                        3.6. You understand that the operation of the Service, including Your Content, may be unencrypted and involve (a) transmissions over various networks; (b) changes to conform and adapt to technical requirements of connecting networks or devices and (c) transmission to UFS’s third party vendors and hosting partners to provide the necessary hardware, software, networking, storage, and related technology required to operate and maintain the Service. Accordingly, you acknowledge that you bear sole responsibility for adequate security, protection and backup of Your Content. UFS will have no liability to you for any unauthorized access or use of any of Your Content, or any corruption, deletion, destruction or loss of any of Your Content.
                    </li>
                    <li>
                        <br />
                        3.7. You shall be responsible for obtaining and maintaining any equipment and ancillary services needed to connect to, access or otherwise use the Services, including, without limitation, browsers, modems, hardware, servers, software, operating system, networking, web servers and internet service (collectively, “Equipment”). You shall be responsible for ensuring that such Equipment is compatible with the Services (and, to the extent applicable, the Software) and complies with all configurations and specifications set forth in UFS’s published policies then in effect. You shall also be responsible for maintaining the security of the Equipment, your Account, passwords (including but not limited to administrative and user passwords) and files, and for all uses of your Account or the Equipment with or without your knowledge or consent.
                    </li>
                    <li>
                        <br />
                        3.8. The failure of UFS to exercise or enforce any right or provision of this TOS shall not be a waiver of that right. You acknowledge that this TOS is a contract between you and UFS, even though it is electronic and is not physically signed by you and UFS, and it governs your use of the Service and takes the place of any prior agreements between you and UFS.
                    </li>
                    <li>
                        <br />
                        3.9. Subject to the terms hereof, UFS may (but has no obligation to) provide technical support services, through email in accordance with our standard practice.
                    </li>
                </ol>
                <li>
                    <br />
                    <b>4. Payment.</b><ol>
                        <li></li>
                        <li>
                            <br />
                            4.1. To the extent the Service or any portion thereof is made available for any fee; you will be required to provide UFS/Mobilecom information regarding your credit card or other payment instrument. You represent and warrant to UFS that such information is true and that you are authorized to use the payment instrument. You will promptly update your account information with any changes (for example, a change in your billing address or credit card expiration date) that may occur. When you arrange for shipment, all charges for the shipment and any additional fees payable to UFS (“Charges”) will be charged to the credit card or other payment instrument associated with your account. You hereby authorize UFS to bill your payment instrument for Charges in accordance with this TOS. Except as otherwise agreed by the parties, all charges, fees, or surcharges shall be those in effect at the time of shipping, available either via email, on the Site. The applicable Charges will be based upon the characteristics of the shipment actually tendered to us. If you dispute any Charges you must let UFS know within sixty (60) days after the date that UFS bills your payment instrument. We reserve the right to change UFS’s rates. If UFS does change any of its rates, UFS will post the new rates to the Service, effective as of the posting date. Your continued use of the Service after the price change becomes effective constitutes your agreement to pay the changed amount. You shall be responsible for all taxes associated with Services other than U.S. taxes based on UFS’s net income.
                        </li>
                        <li>
                            <br />
                            4.2. Shippers are responsible for providing accurate and complete shipment information to UFS, including service selected, number, weight, and dimensions of shipments. If any aspect of the shipment information is incomplete or incorrect as determined by UFS in its sole discretion, UFS may adjust Charges at any time.
                        </li>
                    </ol>
                </li>
            <li></li>
            <li>
                <br />
                5. <b>Representations and Warranties.</b> You represent and warrant to UFS that (i) you have full power and authority to enter into this TOS; (ii) you own all Your Content or have obtained all permissions, releases, rights or licenses required to engage in your posting and other activities (and allow UFS to perform its obligations) in connection with the Services without obtaining any further releases or consents; (iii) Your Content and other activities in connection with the Service, and UFS’s exercise of all rights and license granted by you herein, do not and will not violate, infringe, or misappropriate any third party’s copyright, trademark, right of privacy or publicity, or other personal or proprietary right, nor does Your Content contain any matter that is defamatory, obscene, unlawful, threatening, abusive, tortious, offensive or harassing; and (iv) you are eighteen (18) years of age or older.
            </li>
            <li>
                <br />
                6. <b>Termination. </b>You have the right to terminate your account at any time by sending a cancellation request to [support@unitedfreightsavers.com]. Subject to earlier termination as provided below, UFS may terminate your Account and this TOS at any time by providing thirty (30) days prior notice to the administrative email address associated with your Account. In addition to any other remedies we may have, UFS may also terminate this TOS upon thirty (30) days’ notice (or ten (10) days in the case of nonpayment), if you breach any of the terms or conditions of this TOS. Also, UFS may terminate this TOS immediately without notice if you violate any provision of the AUP, as determined in UFS’s sole reasonable discretion. UFS reserves the right to modify or discontinue, temporarily or permanently, the Service (or any part thereof). Except as provided above, upon any termination of your account, UFS may store all of Your Content on the Service (if any), or it may be permanently deleted by UFS, in its sole discretion. If UFS terminates your account without cause and you have signed up for a fee-bearing service, UFS will refund the pro-rated, unearned portion of any amount that you have prepaid to UFS for such Service. However, all accrued rights to payment and the terms of Sections 4-12 shall survive termination of this TOS.
            </li>
            <li>
                <br />
                7. <b>DISCLAIMER OF WARRANTIES.</b> The Services may be temporarily unavailable for scheduled maintenance or for unscheduled emergency maintenance, either by UFS or by third-party providers, or because of other causes beyond our reasonable control, but UFS shall use reasonable efforts to provide advance notice on the Site or by email of any scheduled service disruption. HOWEVER, THE SERVICE, INCLUDING THE SITE, SOFTWARE AND CONTENT, AND ANY SERVER AND NETWORK COMPONENTS ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS WITHOUT ANY WARRANTIES OF ANY KIND, AND UFS EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, TITLE, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. YOU ACKNOWLEDGE THAT UFS DOES NOT WARRANT THAT THE SERVICE OR SOFTWARE WILL BE UNINTERRUPTED, TIMELY, SECURE, ERROR-FREE OR VIRUS-FREE, NOR DOES IT MAKE ANY WARRANTY AS TO THE RESULTS THAT MAY BE OBTAINED FROM USE OF THE SERVICES OR SOFTWARE, AND NO INFORMATION, ADVICE OR SERVICES OBTAINED BY YOU FROM UFS OR THROUGH THE SERVICE SHALL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THIS TOS.
            </li>
            <li>
                <br />
                <b>8. LIMITATION OF LIABILITY.</b><ol>
                    <li>8.1. EXCEPT AS PROVIDED IN SECTION 13, UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY (WHETHER IN CONTRACT, TORT, OR OTHERWISE) SHALL UFS BE LIABLE TO YOU OR ANY THIRD PARTY FOR (A) ANY INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, CONSEQUENTIAL OR PUNITIVE DAMAGES, INCLUDING LOST PROFITS, LOST SALES OR BUSINESS, LOST DATA, OR (B) ANY DIRECT DAMAGES, COSTS, LOSSES OR LIABILITIES IN EXCESS OF THE FEES ACTUALLY PAID BY YOU IN THE LAST SIX (6) MONTHS PRECEDING THE EVENT GIVING RISE TO YOUR CLAIM, OR, IF NO FEES APPLY, one hundred ($100) U.S. dollars. THE PROVISIONS OF THIS SECTION ALLOCATE THE RISKS UNDER THIS TOS BETWEEN THE PARTIES, AND THE PARTIES HAVE RELIED ON THESE LIMITATIONS IN DETERMINING WHETHER TO ENTER INTO THIS TOS.
8.2. Some states do not allow the exclusion of implied warranties or limitation of liability for incidental or consequential damages, which means that some of the above limitations may not apply to you. IN THESE STATES, UFS’S LIABILITY WILL BE LIMITED TO THE GREATEST EXTENT PERMITTED BY LAW.
                    </li>
                </ol>
            </li>
            <li>
                <br />
                9. <strong>Indemnification</strong>. You shall defend, indemnify, and hold harmless United Freight Savers from and against any claims, actions or demands, including without limitation reasonable legal and accounting fees, arising or resulting from your breach of this TOS, any of Your Content, or your other access, contribution to, use or misuse of the Service. UFS shall provide notice to you of any such claim, suit or demand. UFS (Mobilecom) reserves the right to assume the exclusive defense and control of any matter which is subject to indemnification under this section. In such case, you agree to cooperate with any reasonable requests assisting UFS’s defense of such matter.
            </li>
            <li>
                <br />
                10. <strong>Reimbursement</strong>. UFS always recommends that you have your own shipping insurance. However, subject to the terms below, UFS will reimburse you for actual loss of or damage to any shipment while in UFS’s Possession (as defined below) up to a maximum reimbursement of $1,000 per shipment or, if less, the maximum amount permitted by applicable law or regulation. However, for clarity, any reimbursement hereunder may not exceed the total value of the original shipment. For purposes hereof, an item shall be deemed to be in “UFS’s Possession” from the point UFS collects the item from you until the item reaches the end destination via one of our mailing partners. Any insurance must be purchased separately from an insurance provider.
If a shipment is lost or damaged while in UFS’s Possession, you may file a claim with UFS for reimbursement. All claims must be initiated within [30 days] of the mailing date by contacting us at [support@unitedfreightsavers.com] where we will provide more details on how to file a claim. The original receipt of the shipping label and an image or photograph of the damaged item may be required when filing a claim. If the recipient accepts the shipment without noting any damage on the delivery record, we will assume the shipment was delivered in good condition. In order for us to consider a claim for damage, the contents, original shipping cartons, and packing must be available to us for inspection. Written documentation (such as a receipt) supporting the amount of a claim will also be required. All supporting documentation must be submitted within 30 days of claim initiation (60 days of mailing date) of the mailing date.
            </li>
            <li>
                <br />
                11. <strong>Arbitration</strong>. At UFS’s or your election, all disputes, claims, or controversies arising out of or relating to the TOS or the Service that are not resolved by mutual agreement may be resolved by binding arbitration to be conducted before JAMS, or its successor. Unless otherwise agreed by the parties, arbitration will be held in Manhattan, New York before a single arbitrator mutually agreed upon by the parties, or if the parties cannot mutually agree, a single arbitrator appointed by JAMS, and will be conducted in accordance with the rules and regulations promulgated by JAMS unless specifically modified in the TOS. The arbitration must commence within forty-five (45) days of the date on which a written demand for arbitration is filed by either party. The arbitrator’s decision and award will be made and delivered within sixty (60) days of the conclusion of the arbitration and within six (6) months of the selection of the arbitrator. The arbitrator will not have the power to award damages in excess of the limitation on actual compensatory, direct damages set forth in the TOS and may not multiply actual damages or award punitive damages or any other damages that are specifically excluded under the TOS, and each party hereby irrevocably waives any claim to such damages. The arbitrator may, in his or her discretion, assess costs and expenses (including the reasonable legal fees and expenses of the prevailing part) against any party to a proceeding. Any party refusing to comply with an order of the arbitrators will be liable for costs and expenses, including attorneys’ fees, incurred by the other party in enforcing the award. Notwithstanding the foregoing, in the case of temporary or preliminary injunctive relief, any party may proceed in court without prior arbitration for the purpose of avoiding immediate and irreparable harm. The provisions of this arbitration section will be enforceable in any court of competent jurisdiction.
Notwithstanding the provisions of the introductory section above, if UFS changes this ‘Arbitration’ section after the date you first accepted these TOS (or accepted any subsequent changes to these TOS ), you may reject any such change by sending us written notice within 30 days of the date such change became effective, as indicated in the “Date of Last Revision” date above or in the date of UFS’s email to you notifying you of such change. By rejecting any change, you are agreeing that you will arbitrate any dispute between you and UFS in accordance with the provisions of this section as of the date you first accepted this TOS (or accepted any subsequent changes to this TOS).
<br />
                <b>U.S. Government Matters. </b>You may not remove or export from the United States or allow the export or re-export of the Services or anything related thereto, or any direct product thereof in violation of any restrictions, laws or regulations of the United States Department of Commerce, the United States Department of Treasury Office of Foreign Assets Control, or any other United States or foreign agency or authority. As defined in FAR section 2.101, the software and documentation installed by UFS on your Equipment (if applicable) are “commercial items” and according to DFAR section 252.227 7014(a)(1) and (5) are deemed to be “commercial computer software” and “commercial computer software documentation.” Consistent with DFAR section 227.7202 and FAR section 12.212, any use modification, reproduction, release, performance, display, or disclosure of such commercial software or commercial software documentation by the U.S. Government will be governed solely by the terms of this TOS and will be prohibited except to the extent expressly permitted by the terms of this TOS.
            </li>
            <li>
                <br />
                12. <strong>Assignment</strong>. You may not assign this TOS without the prior written consent of UFS, in which case, UFS may charge a transfer fee. UFS may assign or transfer this TOS, in whole or in part, without restriction.
            </li>
            <li>
                <br />
                13. <strong>Miscellaneous</strong>. If any provision of this TOS is found to be unenforceable or invalid, that provision will be limited or eliminated to the minimum extent necessary so that this TOS will otherwise remain in full force and effect and enforceable. Both parties agree that this TOS is the complete and exclusive statement of the mutual understanding of the parties and supersedes and cancels all previous written and oral agreements, communications and other understandings relating to the subject matter of this TOS, and that all waivers and modifications must be in a writing signed by both parties, except as otherwise provided herein. No agency, partnership, joint venture, or employment is created as a result of this TOS and you do not have any authority of any kind to bind UFS in any respect whatsoever. In any action or proceeding to enforce rights under this TOS, the prevailing party will be entitled to recover costs and attorneys’ fees. All notices under this TOS will be in writing and will be deemed to have been duly given when received, if personally delivered; when receipt is electronically confirmed, if transmitted by facsimile or email; the day after it is sent, if sent for next day delivery by recognized overnight delivery service; and upon receipt, if sent by certified or registered mail, return receipt requested.
            </li>
            <li>
                <br />
                14. <strong>Governing Law</strong>. This TOS shall be governed by the laws of the State of New York without regard to the principles of conflicts of law. Unless otherwise elected by UFS in a particular instance, you hereby expressly agree to submit to the exclusive personal jurisdiction of the federal and state courts located within New York for the purpose of resolving any dispute relating to your access to or use of the Service.
                <h4>Questions?  Concerns?  Suggestions?</h4>
                <br />
                Please contact us at support@unitedfreightsavers.com to report any violations of these TOS or to pose any questions regarding this TOS or the Service or visit our FAQ at unitedfreightsavers.com/faq.html for more information.
            </li>
        </ol>
    </div>
</asp:Content>
