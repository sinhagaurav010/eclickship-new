﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using UFS.Service.BusinessEntities;

namespace UFS.Service
{
    public class EasyPostHandler
    {
        private List<GetRateResponse> _response;
        public List<GetRateResponse> RateList
        {
            get
            {
                return _response;
            }
        }


        public GetRateResponse RateResponse { get; internal set; }

        public EasyPostHandler()
        {

        }
        public EasyPostHandler(GetRateRequest rq)
        {
            List<Message> msg = new List<Message>();

            try
            {
                string key = "";
                if (ConfigurationManager.AppSettings["easypostkey"] != null)
                {
                    key = ConfigurationManager.AppSettings["easypostkey"].ToString();
                    _response = new List<GetRateResponse>();
                    EasyPost.Client.apiKey = key;
                    EasyPost.Shipment shp = new EasyPost.Shipment();
                    shp.from_address = new EasyPost.Address()
                    {
                        zip = rq.FromZipCode
                    };
                    shp.to_address = new EasyPost.Address()
                    {
                        zip = rq.ToZipCode
                    };
                    shp.parcel = new EasyPost.Parcel();
                    shp.parcel.weight = rq.Weight*16; // converting pound to ounce
                    if (rq.Dimension.IsValidDimention)
                    {
                        shp.parcel.height = rq.Dimension.Height;
                        shp.parcel.width = rq.Dimension.Width;
                        shp.parcel.length = rq.Dimension.Length;
                    }
                    shp.Create();
                    //RateResponse.Rate = shp.LowestRate();
                    if (shp.rates != null && shp.rates.Count > 0)
                    {
                        _response = new List<GetRateResponse>();
                        foreach (var item in shp.rates)
                        {
                            _response.Add(new GetRateResponse()
                            {
                                Rate = item

                            });
                        }
                    }
                    msg.Add(new Message()
                    {
                        Code = 0,
                        Description = "Success"
                    });
                }
                else
                {
                    msg.Add(new Message()
                    {
                        Code = -2,
                        Description = "Key not found in the configuration file"
                    });
                }
            }
            catch (Exception ex)
            {
                msg.Add(new Message()
                {
                    Code = -1,
                    Description = ex.Message
                });
                _response.Add(new GetRateResponse()
                {
                    Messages = msg
                });
            }
        }
    }
}
