﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using UFS.Service.BusinessEntities;

namespace UFS.Service
{
    [ServiceContract(Namespace = "")]
    public interface IUFSService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml)]
        List<GetRateResponse> GetAllRates(GetRateRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml)]
        GetRateResponse GetLowestRate(GetRateRequest request);
    }

}
