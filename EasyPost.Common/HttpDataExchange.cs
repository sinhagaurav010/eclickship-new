﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace UFS.Common
{
    public static class HttpDataExchange
    {
        public static T GetWebServiceResult<T>(string requestURL, object request)
        {
            string xmlRequest = GetXMlRequest(request);
            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(requestURL);
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(xmlRequest.ToString());
            httpWReq.ContentType = "text/xml; encoding='utf-8'";
            httpWReq.ContentLength = bytes.Length;
            httpWReq.Method = "POST";
            Stream requestStream = httpWReq.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse httpWResp = (HttpWebResponse)httpWReq.GetResponse();
            T response = default(T);
            try
            {
                //Test the connection
                if (httpWResp.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = httpWResp.GetResponseStream();
                    string responseString = null;
                    //Set jsonString using a stream reader
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        responseString = reader.ReadToEnd();
                        responseString = "<?xml version=\"1.0\" encoding=\"UTF - 8\"?>" + responseString;
                        var strReader = new StringReader(responseString);
                        XmlSerializer xmlsr = new XmlSerializer(typeof(T));
                        response = ConvertObject<T>(xmlsr.Deserialize(strReader));
                        reader.Close();
                    }

                }
                else
                {
                    FailComponent(httpWResp.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                FailComponent(e.ToString());
            }
            return response;
        }

        private static string GetXMlRequest(object request)
        {
            StringBuilder sb = new StringBuilder();
            //var stringwriter = new System.IO.StringWriter();
            //var serializer = new XmlSerializer(request.GetType());
            using (XmlWriter writer = XmlWriter.Create(sb, new XmlWriterSettings() { OmitXmlDeclaration = true }))
            {
                new XmlSerializer(request.GetType()).Serialize(writer, request);
            }
            //serializer.Serialize(stringwriter, request);

            return sb.ToString();
        }

        private static void FailComponent(string toString)
        {
            //bool fail = false;
            //IDTSComponentMetaData100 compMetadata = this.GetWebServiceResult();
            //compMetadata.FireError(1, "Error Getting Data From Webservice!", errorMsg, "", 0, out fail);
        }

        private static T ConvertObject<T>(object obj)
        {
            if (obj is T)
            {
                return (T)obj;
            }
            else {
                try
                {
                    return (T)Convert.ChangeType(obj, typeof(T));
                }
                catch (InvalidCastException)
                {
                    return default(T);
                }
            }
        }
    }


    public enum DataFormat
    {
        JSON,
        XML
    }
}
