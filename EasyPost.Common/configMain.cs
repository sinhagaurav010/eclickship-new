﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UFS.Common
{
    public partial class configEShipper
    {
        eShipperConfigMain _mainConfig;
        public eShipperConfigMain MainConfig
        {
            get
            {
                _mainConfig = new eShipperConfigMain();
                _mainConfig.URL = this.IsInTesting ? this.URL.Test : this.URL.Production;
                _mainConfig.UserName = this.IsInTesting ? this.Credentials.Test.UserName : this.Credentials.Production.UserName;
                _mainConfig.Password = this.IsInTesting ? this.Credentials.Test.Password : this.Credentials.Production.Password;
                return _mainConfig;
            }
        }
    }
    public partial class configUPS
    {
        MainURL _urls;
        public MainURL MainUrls
        {

            get
            {
                _urls = new MainURL();
                _urls.PickupServiceURL = this.IsInTesting ? this.TestingURL.PickupServiceURL : this.ProdURL.PickupServiceURL;
                _urls.RateServiceURL = this.IsInTesting ? this.TestingURL.RateServiceURL : this.ProdURL.RateServiceURL;
                _urls.RecoveryServiceURL = this.IsInTesting ? this.TestingURL.RecoveryServiceURL : this.ProdURL.RecoveryServiceURL;
                _urls.ShipServiceURL = this.IsInTesting ? this.TestingURL.ShipServiceURL : this.ProdURL.ShipServiceURL;
                _urls.VoidServiceURL = this.IsInTesting ? this.TestingURL.VoidServiceURL : this.ProdURL.VoidServiceURL;
                _urls.XAVServiceURL = this.IsInTesting ? this.TestingURL.XAVServiceURL : this.ProdURL.XAVServiceURL;
                return _urls;
            }
        }
        private MainUPSKeys _keys;
        public MainUPSKeys UPSKeys
        {
            get
            {
                _keys = new MainUPSKeys();
                _keys.AccessLicenseNumber = this.IsInTesting ? this.TestKeys.AccessLicenseNumber : this.ProdKeys.AccessLicenseNumber;
                _keys.ShipperNumber = this.IsInTesting ? this.TestKeys.ShipperNumber : this.ProdKeys.ShipperNumber;
                _keys.ShipperAccountNumber = this.IsInTesting ? this.TestKeys.ShipperAccountNumber : this.ProdKeys.ShipperAccountNumber;
                _keys.UserId = this.IsInTesting ? this.TestKeys.UserId : this.ProdKeys.UserId;
                _keys.Password = this.IsInTesting ? this.TestKeys.Password : this.ProdKeys.Password;
                return _keys;
            }
        }
    }

    public class MainURL
    {
        private string shipServiceURLField;

        private string recoveryServiceURLField;

        private string voidServiceURLField;

        private string pickupServiceURLField;

        private string rateServiceURLField;

        private string xAVServiceURLField;

        /// <remarks/>
        public string ShipServiceURL
        {
            get
            {
                return this.shipServiceURLField;
            }
            set
            {
                this.shipServiceURLField = value;
            }
        }

        /// <remarks/>
        public string RecoveryServiceURL
        {
            get
            {
                return this.recoveryServiceURLField;
            }
            set
            {
                this.recoveryServiceURLField = value;
            }
        }

        /// <remarks/>
        public string VoidServiceURL
        {
            get
            {
                return this.voidServiceURLField;
            }
            set
            {
                this.voidServiceURLField = value;
            }
        }

        /// <remarks/>
        public string PickupServiceURL
        {
            get
            {
                return this.pickupServiceURLField;
            }
            set
            {
                this.pickupServiceURLField = value;
            }
        }

        /// <remarks/>
        public string RateServiceURL
        {
            get
            {
                return this.rateServiceURLField;
            }
            set
            {
                this.rateServiceURLField = value;
            }
        }

        /// <remarks/>
        public string XAVServiceURL
        {
            get
            {
                return this.xAVServiceURLField;
            }
            set
            {
                this.xAVServiceURLField = value;
            }
        }
    }

    public class MainUPSKeys
    {
        private string accessLicenseNumberField;

        private string shipperNumberField;

        private string shipperAccountNumberField;

        private string userIdField;

        private string passwordField;


        public string AccessLicenseNumber
        {
            get
            {
                return this.accessLicenseNumberField;
            }
            set
            {
                this.accessLicenseNumberField = value;
            }
        }

        public string ShipperNumber
        {
            get
            {
                return this.shipperNumberField;
            }
            set
            {
                this.shipperNumberField = value;
            }
        }

        public string ShipperAccountNumber
        {
            get
            {
                return this.shipperAccountNumberField;
            }
            set
            {
                this.shipperAccountNumberField = value;
            }
        }

        public string UserId
        {
            get
            {
                return this.userIdField;
            }
            set
            {
                this.userIdField = value;
            }
        }

        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }
    }

    public class eShipperConfigMain
    {
        public string URL { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
