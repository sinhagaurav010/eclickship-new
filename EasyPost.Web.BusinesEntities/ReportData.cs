﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UFS.Web.BusinesEntities
{
    public class ReportData
    {
        public string Carrier { get; set; }
        public string Service { get; set; }
        public string FromAddress { get; set; }
        public string ToAddress { get; set; }
        public string EmailAddress { get; set; }
        public string LabelField { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Price { get; set; }
        public string ChargedPrice { get; set; }

        public string ShipmentId { get; set; }
        public string CreateDate { get; set; }
        public string OrderId { get; set; }

        public string BatchNumber { get; set; }

        public string TrackingUrl { get; set; }
    }
}
