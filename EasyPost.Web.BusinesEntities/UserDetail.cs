﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UFS.Web.BusinesEntities;

namespace UFS.Web.BusinesEntities
{
    [Serializable]
    public class UserDetail
    {
        public string EmailAddress { get; set; }
        public string EmailAddress1 { get; set; }
        public string EmailAddress2 { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Company { get; set; }

        public string Password { get; set; }
        public UserAddress Address { get; set; }
        public string RoleId { get; set; }
        public string Message { get; set; }
        public bool IsUpdate { get; set; }

        public int IsApproved  { get; set; }
        public int UserId { get; set; }

        public string FedExDisc { get; set; }
        public string USPSDisc { get; set; }
        public string UPSDisc { get; set; }

        public string AuthCode { get; set; }

        private string easyportkey = "";
        public string EasyPostFedExKey { get { return ""; } set { easyportkey = value; } }

        public string ShopifyApiKey { get; set; }

        public string ShopifyApiPassword { get; set; }

        public string ShpifyStoreUrl { get; set; }
        public ShopifyAuthentication AuthDetails { get; set; }
    }

    public class CarierDiscount
    {
        public int Id { get; set; }
        public string CareerName { get; set; }
        public string ServiceName { get; set; }
        public string Discount { get; set; }

    }

}
