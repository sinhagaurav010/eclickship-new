﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UFS.Web.BusinesEntities
{
    [Serializable]
    public class Address : UserAddress
    {
        public string Name { get; set; }
        public string Company { get; set; }

        string _fullNme = "";
        public string FullName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Name))
                    _fullNme = Name;

                if (!string.IsNullOrWhiteSpace(_fullNme) && !string.IsNullOrWhiteSpace(Company))
                    _fullNme = _fullNme + ", " + Company;

                if (string.IsNullOrWhiteSpace(_fullNme))
                    _fullNme = Company;

                return _fullNme;
            }
        }
        public string PartialName
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Name) ? Name : Company;
            }
        }
        public string FullAddress
        {
            get
            {
                return Street1 + " " + Street2 + ", " + City + ", " + State + ", " + Country + "-" + Zip + ", " + Phone;
            }
        }
    }
}
