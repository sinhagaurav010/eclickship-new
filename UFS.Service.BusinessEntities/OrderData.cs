﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UFS.Service.BusinessEntities
{
    public class MainOrder
    {
        public CustomerOrder MyOrder { get; set; }
    }
    public class CustomerOrder
    {
        public List<Order> orders { get; set; }
    }
    public class Order
    {
        public long id { get; set; }
        public string email { get; set; }
        public object closed_at { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public int? number { get; set; }
        public string note { get; set; }
        public string token { get; set; }
        public string gateway { get; set; }
        public bool? test { get; set; }
        public string total_price { get; set; }
        public string subtotal_price { get; set; }
        public int? total_weight { get; set; }
        public string total_tax { get; set; }
        public bool? taxes_included { get; set; }
        public string currency { get; set; }
        public string financial_status { get; set; }
        public bool? confirmed { get; set; }
        public string total_discounts { get; set; }
        public string total_line_items_price { get; set; }
        public string cart_token { get; set; }
        public bool? buyer_accepts_marketing { get; set; }
        public string name { get; set; }
        public string referring_site { get; set; }
        public string landing_site { get; set; }
        public object cancelled_at { get; set; }
        public object cancel_reason { get; set; }
        public string total_price_usd { get; set; }
        public string checkout_token { get; set; }
        public object reference { get; set; }
        public object user_id { get; set; }
        public object location_id { get; set; }
        public object source_identifier { get; set; }
        public object source_url { get; set; }
        public DateTime? processed_at { get; set; }
        public object device_id { get; set; }
        public object browser_ip { get; set; }
        public object landing_site_ref { get; set; }
        public int? order_number { get; set; }
        public object[] discount_codes { get; set; }
        public object[] note_attributes { get; set; }
        public string[] payment_gateway_names { get; set; }
        public string processing_method { get; set; }
        public long? checkout_id { get; set; }
        public string source_name { get; set; }
        public object fulfillment_status { get; set; }
        public object[] tax_lines { get; set; }
        public string tags { get; set; }
        public string contact_email { get; set; }
        public string order_status_url { get; set; }
        public Line_Items[] line_items { get; set; }
        public Shipping_Lines[] shipping_lines { get; set; }
        public Billing_Address billing_address { get; set; }
        public Shipping_Address shipping_address { get; set; }
        public object[] fulfillments { get; set; }
        public Client_Details client_details { get; set; }
        public object[] refunds { get; set; }
        public Customer customer { get; set; }
    }

    public class Billing_Address
    {
        public string first_name { get; set; }
        public string address1 { get; set; }
        public object phone { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string province { get; set; }
        public string country { get; set; }
        public string last_name { get; set; }
        public string address2 { get; set; }
        public object company { get; set; }
        public float? latitude { get; set; }
        public float? longitude { get; set; }
        public string name { get; set; }
        public string country_code { get; set; }
        public string province_code { get; set; }
    }

    public class Shipping_Address
    {
        public string first_name { get; set; }
        public string address1 { get; set; }
        public object phone { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string province { get; set; }
        public string country { get; set; }
        public string last_name { get; set; }
        public string address2 { get; set; }
        public object company { get; set; }
        public float? latitude { get; set; }
        public float? longitude { get; set; }
        public string name { get; set; }
        public string country_code { get; set; }
        public string province_code { get; set; }
    }

    public class Client_Details
    {
        public string browser_ip { get; set; }
        public string accept_language { get; set; }
        public string user_agent { get; set; }
        public string session_hash { get; set; }
        public int? browser_width { get; set; }
        public int? browser_height { get; set; }
    }

    public class Customer
    {
        public long? id { get; set; }
        public string email { get; set; }
        public bool? accepts_marketing { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public int? orders_count { get; set; }
        public string state { get; set; }
        public string total_spent { get; set; }
        public long? last_order_id { get; set; }
        public object note { get; set; }
        public bool? verified_email { get; set; }
        public object multipass_identifier { get; set; }
        public bool? tax_exempt { get; set; }
        public string tags { get; set; }
        public string last_order_name { get; set; }
        public Default_Address default_address { get; set; }
    }

    public class Default_Address
    {
        public long? id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public object company { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
        public object phone { get; set; }
        public string name { get; set; }
        public string province_code { get; set; }
        public string country_code { get; set; }
        public string country_name { get; set; }
        public bool? _default { get; set; }
    }

    public class Line_Items
    {
        public long? id { get; set; }
        public long? variant_id { get; set; }
        public string title { get; set; }
        public int? quantity { get; set; }
        public string price { get; set; }
        public int? grams { get; set; }
        public string sku { get; set; }
        public string variant_title { get; set; }
        public string vendor { get; set; }
        public string fulfillment_service { get; set; }
        public long? product_id { get; set; }
        public bool? requires_shipping { get; set; }
        public bool? taxable { get; set; }
        public bool? gift_card { get; set; }
        public string name { get; set; }
        public object variant_inventory_management { get; set; }
        public object[] properties { get; set; }
        public bool? product_exists { get; set; }
        public int? fulfillable_quantity { get; set; }
        public string total_discount { get; set; }
        public object fulfillment_status { get; set; }
        public Tax_Lines[] tax_lines { get; set; }
        public Origin_Location origin_location { get; set; }
        public Destination_Location destination_location { get; set; }
    }

    public class Origin_Location
    {
        public long? id { get; set; }
        public string country_code { get; set; }
        public string province_code { get; set; }
        public string name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
    }

    public class Destination_Location
    {
        public long? id { get; set; }
        public string country_code { get; set; }
        public string province_code { get; set; }
        public string name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
    }

    public class Tax_Lines
    {
        public string title { get; set; }
        public string price { get; set; }
        public float? rate { get; set; }
    }

    public class Shipping_Lines
    {
        public long? id { get; set; }
        public string title { get; set; }
        public string price { get; set; }
        public string code { get; set; }
        public string source { get; set; }
        public object phone { get; set; }
        public string carrier_identifier { get; set; }
        public Tax_Lines1[] tax_lines { get; set; }
    }

    public class Tax_Lines1
    {
        public string title { get; set; }
        public string price { get; set; }
        public float? rate { get; set; }
    }


    public class DisplayData
    {
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }

        public string RecepientName { get; set; }

        public string RecepientAddress { get; set; }

        public string TrackingUrl { get; set; }
    }
}