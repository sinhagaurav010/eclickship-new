﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace UFS.Service.BusinessEntities
{
    [DataContract]
    public class GetRateRequest
    {
        [DataMember]
        public string FromZipCode { get; set; }
        [DataMember]
        public string ToZipCode { get; set; }
        [DataMember]
        public string Carrier { get; set; }
        [DataMember]
        public double Weight { get; set; }
        [DataMember]
        public Dimension Dimension { get; set; }
    }
    [DataContract]
    public class Dimension
    {
        [DataMember]
        public double Height { get; set; }
        [DataMember]
        public double Width { get; set; }
        [DataMember]
        public double Length { get; set; }
        public bool IsValidDimention { get { return Height > 0 && Width > 0 && Length > 0; } }
    }
}
