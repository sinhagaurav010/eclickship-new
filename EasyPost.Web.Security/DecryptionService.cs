﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace UFS.Web.Security
{
   public static class DecryptionService
    {
        /// <summary>
        /// Decrypts a given string.
        /// </summary>
        /// <param name="cipher">A base64 encoded string that was created
        /// through the <see cref="Encrypt(string)"/> or
        /// <see cref="Encrypt(SecureString)"/> extension methods.</param>
        /// <returns>The decrypted string.</returns>
        /// <remarks>Keep in mind that the decrypted string remains in memory
        /// and makes your application vulnerable per se. If runtime protection
        /// is essential, <see cref="SecureString"/> should be used.</remarks>
        /// <exception cref="ArgumentNullException">If <paramref name="cipher"/>
        /// is a null reference.</exception>
        public static string DecryptByBase64String(string cipher)
        {
            if (cipher == null) throw new ArgumentNullException("cipher");

            //parse base64 string
            byte[] data = Convert.FromBase64String(cipher);

            //decrypt data
            return Encoding.Unicode.GetString(data);
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="encryptedText"></param>
       /// <returns></returns>
       public static string DecryptUsingRC2(string encryptedText)
       {
           string c_key = "aneeta786";
           string c_iv = "aneeta7860"; 
           RC2CryptoServiceProvider rc2CSP = new RC2CryptoServiceProvider();
        //   c_key = rc2CSP.Key;
        //   rc2CSP = 
       //    ICryptoTransform decryptor = rc2CSP.CreateDecryptor(Convert.FromBase64String(c_key), Convert.FromBase64String(c_iv));
           ICryptoTransform decryptor = rc2CSP.CreateDecryptor(rc2CSP.Key,rc2CSP.IV);
           using (MemoryStream msDecrypt = new MemoryStream(Convert.FromBase64String(encryptedText)))
           {
               using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
               {
                   List<Byte> bytes = new List<byte>();
                   int b;
                   do
                   {
                       b = csDecrypt.ReadByte();
                       if (b != -1)
                       {
                           bytes.Add(Convert.ToByte(b));
                       }

                   }
                   while (b != -1);

                   return Encoding.Unicode.GetString(bytes.ToArray());
               }
           }
       }
    }
}
