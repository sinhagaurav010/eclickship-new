﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EasyPost.Web.UI.Startup))]
namespace EasyPost.Web.UI
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
