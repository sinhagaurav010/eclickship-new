﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace UFS.eShipper.Service.Request
{
    public class eShipperQuoteRequest
    {
        public EShipper Data { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Version { get; set; }
    }

    [XmlRoot(ElementName = "From", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class From
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "company")]
        public string Company { get; set; }
        [XmlAttribute(AttributeName = "address1")]
        public string Address1 { get; set; }

        [XmlAttribute(AttributeName = "address2")]
        public string Address2 { get; set; }

        [XmlAttribute(AttributeName = "city")]
        public string City { get; set; }
        [XmlAttribute(AttributeName = "state")]
        public string State { get; set; }
        [XmlAttribute(AttributeName = "zip")]
        public string Zip { get; set; }
        [XmlAttribute(AttributeName = "country")]
        public string Country { get; set; }
        [XmlAttribute(AttributeName = "phone")]
        public string Phone { get; set; }
        [XmlAttribute(AttributeName = "attention")]
        public string Attention { get; set; }
        [XmlAttribute(AttributeName = "email")]
        public string Email { get; set; }
        [XmlAttribute(AttributeName = "residential")]
        public string Residential { get; set; }
    }

    [XmlRoot(ElementName = "To", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class To
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "company")]
        public string Company { get; set; }
        [XmlAttribute(AttributeName = "address1")]
        public string Address1 { get; set; }

        [XmlAttribute(AttributeName = "address2")]
        public string Address2 { get; set; }
        [XmlAttribute(AttributeName = "city")]
        public string City { get; set; }
        [XmlAttribute(AttributeName = "state")]
        public string State { get; set; }
        [XmlAttribute(AttributeName = "zip")]
        public string Zip { get; set; }
        [XmlAttribute(AttributeName = "country")]
        public string Country { get; set; }
        [XmlAttribute(AttributeName = "phone")]
        public string Phone { get; set; }
        [XmlAttribute(AttributeName = "attention")]
        public string Attention { get; set; }
        [XmlAttribute(AttributeName = "email")]
        public string Email { get; set; }
        [XmlAttribute(AttributeName = "residential")]
        public string Residential { get; set; }
    }

    [XmlRoot(ElementName = "CODReturnAddress", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class CODReturnAddress
    {
        [XmlAttribute(AttributeName = "codCompany")]
        public string CodCompany { get; set; }
        [XmlAttribute(AttributeName = "codName")]
        public string CodName { get; set; }
        [XmlAttribute(AttributeName = "codAddress1")]
        public string CodAddress1 { get; set; }
        [XmlAttribute(AttributeName = "codCity")]
        public string CodCity { get; set; }
        [XmlAttribute(AttributeName = "codStateCode")]
        public string CodStateCode { get; set; }
        [XmlAttribute(AttributeName = "codZip")]
        public string CodZip { get; set; }
        [XmlAttribute(AttributeName = "codCountry")]
        public string CodCountry { get; set; }
    }

    [XmlRoot(ElementName = "COD", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class COD
    {
        [XmlElement(ElementName = "CODReturnAddress", Namespace = "http://www.eshipper.net/XMLSchema")]
        public CODReturnAddress CODReturnAddress { get; set; }
        [XmlAttribute(AttributeName = "paymentType")]
        public string PaymentType { get; set; }
    }

    [XmlRoot(ElementName = "Package", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class Package
    {
        [XmlAttribute(AttributeName = "length")]
        public string Length { get; set; }
        [XmlAttribute(AttributeName = "width")]
        public string Width { get; set; }
        [XmlAttribute(AttributeName = "height")]
        public string Height { get; set; }
        [XmlAttribute(AttributeName = "weight")]
        public string Weight { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "freightClass")]
        public string FreightClass { get; set; }
        [XmlAttribute(AttributeName = "nmfcCode")]
        public string NmfcCode { get; set; }
        [XmlAttribute(AttributeName = "insuranceAmount")]
        public string InsuranceAmount { get; set; }
        [XmlAttribute(AttributeName = "codAmount")]
        public string CodAmount { get; set; }
        [XmlAttribute(AttributeName = "description")]
        public string Description { get; set; }
    }

    [XmlRoot(ElementName = "Packages", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class Packages
    {
        [XmlElement(ElementName = "Package", Namespace = "http://www.eshipper.net/XMLSchema")]
        public List<Package> Package { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "Pickup", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class Pickup
    {
        [XmlAttribute(AttributeName = "contactName")]
        public string ContactName { get; set; }
        [XmlAttribute(AttributeName = "phoneNumber")]
        public string PhoneNumber { get; set; }
        [XmlAttribute(AttributeName = "pickupDate")]
        public string PickupDate { get; set; }
        [XmlAttribute(AttributeName = "pickupTime")]
        public string PickupTime { get; set; }
        [XmlAttribute(AttributeName = "closingTime")]
        public string ClosingTime { get; set; }
        [XmlAttribute(AttributeName = "location")]
        public string Location { get; set; }
    }


    [XmlRoot(ElementName = "QuoteRequest", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class QuoteRequest
    {
        [XmlElement(ElementName = "From", Namespace = "http://www.eshipper.net/XMLSchema")]
        public From From { get; set; }
        [XmlElement(ElementName = "To", Namespace = "http://www.eshipper.net/XMLSchema")]
        public To To { get; set; }
        [XmlElement(ElementName = "COD", Namespace = "http://www.eshipper.net/XMLSchema")]
        public COD COD { get; set; }
        [XmlElement(ElementName = "Packages", Namespace = "http://www.eshipper.net/XMLSchema")]
        public Packages Packages { get; set; }
        [XmlElement(ElementName = "Pickup", Namespace = "http://www.eshipper.net/XMLSchema")]
        public Pickup Pickup { get; set; }
        [XmlAttribute(AttributeName = "serviceId")]
        public string ServiceId { get; set; }
        [XmlAttribute(AttributeName = "stackable")]
        public string Stackable { get; set; }
    }

    [XmlRoot(ElementName = "EShipper", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class EShipper
    {
        [XmlElement(ElementName = "QuoteRequest", Namespace = "http://www.eshipper.net/XMLSchema")]
        public QuoteRequest QuoteRequest { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        [XmlAttribute(AttributeName = "username")]
        public string Username { get; set; }
        [XmlAttribute(AttributeName = "password")]
        public string Password { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
    }

}
