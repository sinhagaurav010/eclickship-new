﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UFS.eShipper.Service.Response
{
    public class eShipperQuoteResponse
    {
        public EShipper ResponseData { get; set; }
    }

    [XmlRoot(ElementName = "Surcharge", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class Surcharge
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "amount")]
        public string Amount { get; set; }
    }

    [XmlRoot(ElementName = "Quote", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class Quote
    {
        [XmlElement(ElementName = "Surcharge", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public List<Surcharge> Surcharge { get; set; }
        [XmlAttribute(AttributeName = "carrierId")]
        public string CarrierId { get; set; }
        [XmlAttribute(AttributeName = "carrierName")]
        public string CarrierName { get; set; }
        [XmlAttribute(AttributeName = "serviceId")]
        public string ServiceId { get; set; }
        [XmlAttribute(AttributeName = "serviceName")]
        public string ServiceName { get; set; }
        [XmlAttribute(AttributeName = "modeTransport")]
        public string ModeTransport { get; set; }
        [XmlAttribute(AttributeName = "transitDays")]
        public string TransitDays { get; set; }
        [XmlAttribute(AttributeName = "currency")]
        public string Currency { get; set; }
        [XmlAttribute(AttributeName = "baseCharge")]
        public string BaseCharge { get; set; }
        [XmlAttribute(AttributeName = "totalTariff")]
        public string TotalTariff { get; set; }
        [XmlAttribute(AttributeName = "baseChargeTariff")]
        public string BaseChargeTariff { get; set; }
        [XmlAttribute(AttributeName = "fuelSurchargeTariff")]
        public string FuelSurchargeTariff { get; set; }
        [XmlAttribute(AttributeName = "fuelSurcharge")]
        public string FuelSurcharge { get; set; }
        [XmlAttribute(AttributeName = "totalCharge")]
        public string TotalCharge { get; set; }
        [XmlAttribute(AttributeName = "deliveryDate")]
        public string DeliveryDate { get; set; }
    }

    [XmlRoot(ElementName = "QuoteReply", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class QuoteReply
    {
        [XmlElement(ElementName = "Quote", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public List<Quote> Quote { get; set; }
    }

    [XmlRoot(ElementName = "EShipper", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class EShipper
    {
        [XmlElement(ElementName = "QuoteReply", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public QuoteReply QuoteReply { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
        [XmlElement(ElementName = "ErrorReply", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public ErrorReply ErrorReply { get; set; }
    }

    [XmlRoot(ElementName = "Error", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class Error
    {
        [XmlAttribute(AttributeName = "Message")]
        public string Message { get; set; }
    }

    [XmlRoot(ElementName = "ErrorReply", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class ErrorReply
    {
        [XmlElement(ElementName = "Error", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public Error Error { get; set; }
    }


}
