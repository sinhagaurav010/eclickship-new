﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UFS.UPSWrapper;
using UFS.Common;

namespace UFS.UnitTest
{
    [TestClass]
    public class XMLLoadTest
    {
        [TestMethod]
        public void TestXMLLoad()
        {

            config conf = ConfigReader.Instance.LoadData();
          Assert.AreEqual(conf.UPS.UPSKeys.Password, "Reiki@ups");
        }
    }
}
