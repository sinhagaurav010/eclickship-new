﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace UFS.Web.Common
{
    public static class StaticValues
    {
        public static string GetServiceId(string title)
        {
            return title.ToUpper().Contains("ECONOMY") ? (title.ToUpper().Contains("2") ? "29" : "3700")
                    : (title.ToUpper().Contains("EXPEDITED") ? "28" : "");
        }

    }
}
