﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UFS.Web.BusinesEntities;
using System.Data;
using MySql.Data.MySqlClient;
using UFS.DatabaseEntity;
namespace UFS.Web.DataOperations
{
    public class MySQLUserOperations
    {
        private MySqlDataService dao = null;
        public MySQLUserOperations()
        {

        }
        public bool AddUser(UserDetail usr,ref string msg)
        {
            try
            {
                int userid = 0;
                if (!this.IsUserExists(usr.EmailAddress, ref userid))
                {
                    List<MySqlParameter> lstParam = new List<MySqlParameter>();
                    dao = new MySqlDataService();
                    string sql = "INSERT INTO `userdetails` (`EmailId`, `FirstName`, `LastName`, `Password`, `RoleId`, `Company`) VALUES " +
                                  "('" + usr.EmailAddress + "', '" + usr.FirstName + "', '" + usr.LastName + "', '" + usr.Password + "', '" + usr.RoleId + "', '" + usr.Company + "');";
                    dao.ExecuteQuery(sql);
                    this.IsUserExists(usr.EmailAddress, ref userid);
                    if (userid > 0)
                    {
                        AddUserAddress(userid, usr.Address);
                    }

                }
                else
                {
                    msg = "Email address already registered";
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private void AddUserAddress(int userid, UserAddress address)
        {
            dao = new DatabaseEntity.MySqlDataService();
            string sqlQuery = "INSERT INTO `useraddress` (`UserId`, `Street1`, `Street2`, `City`, `State`, `Zip`, `Country`, `Phone`, `IsResidential`) VALUES" +
                "('" + userid.ToString() + "', '" + address.Street1 + "', '" + address.Street2 + "', '" + address.City + "', '" + address.State + "', '" + address.Zip + "', '" + address.Country + "', '" + address.Phone + "','" + address.IsResidential + "');";
            dao.ExecuteQuery(sqlQuery);

        }
        private bool IsValidaUser(int userId, string password)
        {
            dao = new DatabaseEntity.MySqlDataService();
            string query = "SELECT COUNT(*) FROM userdetails WHERE UserId=" + userId.ToString() + " and Password='" + password + "'";
            object returnVal = dao.ExecuteScalar(query);
            if (returnVal != null)
                return Int32.Parse(returnVal.ToString()) > 0;
            else
                return false;
        }

        public UserDetail ValidateLogin(string email, string password)
        {
            int userId = 0;
            UserDetail usr = null;
            if (IsUserExists(email, ref userId))
            {
                if (IsValidaUser(userId, password))
                {
                    usr = GetUsrDetail(userId);
                }
            }
            return usr;
        }


        private bool IsUserExists(string emailId, ref int userId)
        {
            dao = new DatabaseEntity.MySqlDataService();
            string query = "SELECT COUNT(*) AS Count,UserId FROM userdetails WHERE EmailId='" + emailId + "'";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && Utility.GetInt(ds.Tables[0].Rows[0]["Count"]) > 0)
            {
                userId = Utility.GetInt(ds.Tables[0].Rows[0]["UserId"]);
                return true;
            }
            else
                return false;
        }
        public UserAddress GetUsrAddress(int userId)
        {
            UserAddress addr = new UserAddress();
            dao = new DatabaseEntity.MySqlDataService();
            string query = "SELECT *  FROM useraddress WHERE UserId='" + userId.ToString() + "'";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                addr.Street1 = Utility.GetString(ds.Tables[0].Rows[0]["Street1"]);
                addr.Street2 = Utility.GetString(ds.Tables[0].Rows[0]["Street2"]);
                addr.City = Utility.GetString(ds.Tables[0].Rows[0]["City"]);
                addr.State = Utility.GetString(ds.Tables[0].Rows[0]["State"]);
                addr.Zip = Utility.GetString(ds.Tables[0].Rows[0]["Zip"]);
                addr.Country = Utility.GetString(ds.Tables[0].Rows[0]["Country"]);
                addr.Phone = Utility.GetString(ds.Tables[0].Rows[0]["Phone"]);
                addr.IsResidential = Utility.GetBoolean(ds.Tables[0].Rows[0]["IsResidential"]);
            }
            return addr;
        }
        public UserDetail GetUsrDetail(int userId)
        {
            UserDetail usr = new UserDetail();
            dao = new DatabaseEntity.MySqlDataService();
            string query = "SELECT *  FROM userdetails WHERE UserId='" + userId.ToString() + "'";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                usr.EmailAddress = Utility.GetString(ds.Tables[0].Rows[0]["EmailId"]);
                usr.FirstName = Utility.GetString(ds.Tables[0].Rows[0]["FirstName"]);
                usr.LastName = Utility.GetString(ds.Tables[0].Rows[0]["LastName"]);
                usr.RoleId = Utility.GetString(ds.Tables[0].Rows[0]["RoleId"]);
                usr.Company = Utility.GetString(ds.Tables[0].Rows[0]["Company"]);
                usr.Address = GetUsrAddress(userId);
            }
            return usr;
        }
        public UserDetail GetUserByEmail(string email)
        {
            int userid = 0;
            if (this.IsUserExists(email, ref userid))
            {
                return GetUsrDetail(userid);
            }
            else
            {
                return null;
            }
        }
    }
}
