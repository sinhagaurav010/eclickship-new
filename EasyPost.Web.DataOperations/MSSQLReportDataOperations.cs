﻿using UFS.DatabaseEntity;
using UFS.Web.BusinesEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Configuration;

namespace UFS.Web.DataOperations
{
    public class MSSQLReportDataOperations
    {
        private MSSQLDataService dao = null;
        public MSSQLReportDataOperations()
        {

        }
        public List<ReportData> GetReportData(string carrier, string fromDate, string toDate, bool isAdmin, string email)
        {
            string carrierQuery = !string.IsNullOrWhiteSpace(carrier) ? "Carrier = '" + carrier + "' " : "";

            string fromDt = !string.IsNullOrWhiteSpace(fromDate) ? "Convert(varchar(8),CreateDate,112) >= Convert(varchar(8),CAST('" + fromDate + "' AS DATETIME),112) " : "";
            string toDt = !string.IsNullOrWhiteSpace(toDate) ? "Convert(varchar(8),CreateDate,112) <= Convert(varchar(8),CAST('" + toDate + "' AS DATETIME),112) " : "";
            dao = new MSSQLDataService();
            List<ReportData> lstData = new List<ReportData>();

            string query = "SELECT Carrier,ISNULL(ServiceDescription,'') as Service,EmailId,FromAddress,ToAddress,Label,FullName,Price,ChargedPrice,CreateDate,ShipmentId FROM PackageHistory LEFT JOIN ServiceDetails ON LTRIM(RTRIM(Service))= LTRIM(RTRIM(ServiceCode)) " + (!isAdmin ? " WHERE EmailId='" + email + "'" : "") + " order by CreateDate DESC";
            //  if(!string.IsNullOrWhiteSpace(carrier) && !string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
            // {
            if (!string.IsNullOrWhiteSpace(carrier) || !string.IsNullOrWhiteSpace(fromDate) || !string.IsNullOrWhiteSpace(toDate))
            {
                query = "SELECT Carrier,ISNULL(ServiceDescription,'')  as Service,EmailId,FromAddress,ToAddress,Label,FullName,Price,ChargedPrice,CreateDate,ShipmentId FROM PackageHistory LEFT JOIN ServiceDetails ON LTRIM(RTRIM(Service))= LTRIM(RTRIM(ServiceCode)) ";//WHERE Carrier='" + carrier + "' AND CreateDate >" + fromDate + " AND CreateDate < " + toDate; ;
                if (!string.IsNullOrWhiteSpace(carrierQuery))
                {
                    query = query + " WHERE " + carrierQuery;
                }
                if (!string.IsNullOrWhiteSpace(carrierQuery) && !string.IsNullOrWhiteSpace(fromDate))
                {
                    query = query + " AND " + fromDt;
                }
                if (!string.IsNullOrWhiteSpace(carrierQuery) && !string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDt))
                {
                    query = query + " AND " + toDt;
                }
                if (string.IsNullOrWhiteSpace(carrierQuery) && !string.IsNullOrWhiteSpace(fromDate))
                {
                    query = query + " WHERE " + fromDt;
                }
                if (string.IsNullOrWhiteSpace(carrierQuery) && string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDt))
                {
                    query = query + " WHERE " + toDt;
                }
                string adminRule = !isAdmin ? " EmailId='" + email + "'" : "";
                if (query.Contains("WHERE"))
                {
                    if (!string.IsNullOrWhiteSpace(adminRule))
                    {
                        query += " AND " + adminRule;
                    }
                }
                else
                {
                    query += " WHERE " + adminRule;
                }

                query += " order by CreateDate DESC";
            }

            //  }
           /// File.WriteAllText(@"C:\Meeku\Projects\Sanganan\Projects\EasyPost\dk.txt", query);
            DataSet ds = dao.ExecuteDataSet(query);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    lstData.Add(
                        new ReportData()
                        {
                            Carrier = Utility.GetString(row["Carrier"]),
                            Service = Utility.GetString(row["Service"]).Replace('_', ' '),
                            EmailAddress = Utility.GetString(row["EmailId"]),
                            FromAddress = Utility.GetString(row["FromAddress"]),
                            ToAddress = Utility.GetString(row["ToAddress"]),
                            LabelField = Utility.GetString(row["Label"]),
                            FullName = Utility.GetString(row["FullName"]),
                            Price = Utility.GetString(row["Price"]),
                            ChargedPrice = Utility.GetString(row["ChargedPrice"]),
                            CreateDate = Utility.GetString(row["CreateDate"]),
                            ShipmentId = Utility.GetString(row["ShipmentId"]).Replace("shp_", "").ToUpper()
                        }
                        );
                }
                return lstData;
            }
            else
                return lstData;
        }

        public List<ReportData> GetReportData(int userId,bool isAdmin)
        {
            dao = new MSSQLDataService();
            List<ReportData> lstData = new List<ReportData>();
            string query = "SELECT DISTINCT * FROM [dbo].[ShopifyOrders] WHERE BatchStatus = 1 " + ( !isAdmin ? " AND UserId=" + userId.ToString() : "");
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    lstData.Add(
                        new ReportData()
                        {
                            BatchNumber = Utility.GetString(row["BatchNumber"]),
                            Carrier = Utility.GetString(row["ShipVia"]),
                            Service = Utility.GetString(row["ShippingService"]),
                            FromAddress = Utility.GetString(row["ShippingFName"]) +" "+ Utility.GetString(row["ShippingLName"]) + " " + Utility.GetString(row["ShippingAddress1"]) + " " + Utility.GetString(row["ShippingAddress2"]) + " " + Utility.GetString(row["ShippingCity"]) + " " + Utility.GetString(row["ShippingPin"]),
                            ToAddress = Utility.GetString(row["FromCustomerFName"]) + " " + Utility.GetString(row["FromCustomerLName"]) + " " + Utility.GetString(row["FromCustomerAddress1"]) + " " + Utility.GetString(row["FromCustomerAddress2"]) + " " + Utility.GetString(row["FromCustomerCity"]) + " " + Utility.GetString(row["FromCustomerPin"]),
                            LabelField = Utility.GetString(row["TrackingUrl"]),
                            Price = Utility.GetString(row["PriceCharged"]),
                            ChargedPrice = CalculateChargedPrice(Utility.GetString(row["PriceCharged"]), Utility.GetString(row["ShipVia"]), Utility.GetString(row["ShippingService"]),userId.ToString()),                            
                            CreateDate = Utility.GetDateAsString(row["LastUpdated"]),
                            OrderId = Utility.GetString(row["OrderNumber"]),
                            ShipmentId = Utility.GetString(row["TrackingNumber"]),

                        }
                        );
                }
                //return lstData;
            }
            return lstData;
        }

        public string GetBatchOrders(string batchnum)
        {
            try
            {
                dao = new MSSQLDataService();
                string res = dao.ExecuteString("SELECT OrderNumber FROM [ShopifyOrders] WHERE BatchNumber='" + batchnum + "'");
                return res;
            }
            catch (Exception)
            {
                return "0";
            }
        }
        public string GetBatchOrderIds(string batchnum)
        {
            try
            {
                dao = new MSSQLDataService();
                string res = dao.ExecuteString("SELECT DISTINCT OrderId FROM [ShopifyOrders] WHERE BatchNumber='" + batchnum + "'");
                return res;
            }
            catch (Exception)
            {
                return "0";
            }
        }

        public List<UFS.Service.BusinessEntities.OrderDisplay> GetBatchData(string batch,int userId)
        {
            List<UFS.Service.BusinessEntities.OrderDisplay> lstOrders = new List<Service.BusinessEntities.OrderDisplay>();
            dao = new MSSQLDataService();
            UFS.Service.BusinessEntities.OrderDisplay ordr;
            string query = @"SELECT OrderNumber,BatchNumber,BatchStatus, FirstName+' '+ LastName As 'ProcessedBy',Updated,LastUpdated
                            FROM ShopifyOrders 
                            LEFT JOIN userdetails ON ShopifyOrders.UserId  = userdetails.UserId
                            WHERE ShopifyOrders.UserId='" + userId.ToString() + "' AND ISNULL(BatchNumber,'')='"+batch+"'";

            // string query = "SELECT BatchNumber,BatchStatus,  FROM ShopifyOrders WHERE UserId='" + userId.ToString() + "' AND ISNULL(BatchNumber,'')<>'' GROUP BY BatchNumber,BatchStatus";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ordr = new Service.BusinessEntities.OrderDisplay();
                    ordr.BatchNumber = Utility.GetString(ds.Tables[0].Rows[i]["BatchNumber"]);
                    ordr.BatchStatus = Utility.GetString(ds.Tables[0].Rows[i]["BatchStatus"]);
                    ordr.UserName = Utility.GetString(ds.Tables[0].Rows[i]["ProcessedBy"]);
                    ordr.ProcessedDate = Utility.GetDateAsString(ds.Tables[0].Rows[i]["Updated"]);
                    ordr.OrderNumber = Utility.GetString(ds.Tables[0].Rows[i]["OrderNumber"]);
                    lstOrders.Add(ordr);
                }

            }
            return lstOrders;
        }

        public bool AddReportData(ReportData rptData, string userId)
        {
            string sql = "";
            try
            {
                string calculatedPrice = CalculateChargedPrice(rptData.Price, rptData.Carrier, rptData.Service, userId);
                dao = new MSSQLDataService();
                sql = "INSERT INTO PackageHistory (UserName, Carrier, [Service], EmailId, FromAddress, ToAddress,Label,FullName,CreateDate,Price,ChargedPrice,ShipmentId,OrderId) VALUES " +
                              "('" + rptData.UserName + "', '" + rptData.Carrier + "', '" + rptData.Service + "', '" + rptData.EmailAddress + "', '" + rptData.FromAddress + "', '" + rptData.ToAddress + "','" + rptData.LabelField + "','" + rptData.FullName + "','" + DateTime.Now.ToString() + "','" + rptData.Price + "','" + calculatedPrice + "','" + rptData.ShipmentId + "','"+rptData.OrderId+"');";
                dao.ExecuteQuery(sql);
                return true;
            }
            catch (Exception ex)
            {
                //throw new Exception(sql);
                return false;
            }


        }

        public string CalculateChargedPrice(string price, string carrier, string parcel, string userId)
        {
            string calPrice = "";
            try
            {
                //  decimal defPercDisc = 15.00M;
                decimal defPercDisc = 0M;
               
                dao = new MSSQLDataService();
                object res = dao.ExecuteScalar("SELECT ISNULL(PercDisc," + defPercDisc.ToString() + ") FROM PackageServiceDiscount WHERE LTRIM(RTRIM(CarrierName))='" + carrier.Trim() + "' AND LTRIM(RTRIM(ServiceName))='" +parcel.Trim()+"' AND UserName='" + userId + "'");
                decimal percDis = 0;
                if (res != null && decimal.TryParse(res.ToString(), out percDis))
                {
                    if (percDis >0)
                        calPrice = GetCalculatedPrice(price, percDis);
                    else
                        calPrice = "";

                }
               
            }
            catch (Exception ex)
            {
                calPrice = "";
            }
            return calPrice;
        }

        private static string GetCalculatedPrice(string price, decimal percDis)
        {
            string calPrice = "";
            decimal orgPrice = 0;
            decimal cPrice = 0;
            if (decimal.TryParse(price, out orgPrice))
            {
                cPrice = orgPrice + ((orgPrice * percDis) / 100);
                calPrice = cPrice.ToString("0.00");
            }
            else
            {
                calPrice = "0.00";
            }
            return calPrice;
        }

        public void UpdateBatch(string id, int status, string batch, string trackingCode, string labelUrl,string amount,string carrier)
        {
            try
            {
                //string carrier = GetCarrier();
                dao = new MSSQLDataService();
                string sql = "UPDATE ShopifyOrders SET BatchNumber='"+batch+ "',BatchStatus="+status.ToString()+ ",LastUpdated=GETDATE(),Updated= GETDATE(),PriceCharged='"+amount+"',ShipVia='"+carrier+"' WHERE OrderId = " + id;
                sql = sql + "  ";
                if (!string.IsNullOrWhiteSpace(trackingCode) && !string.IsNullOrWhiteSpace(labelUrl))
                {
                    sql = sql + " UPDATE ShopifyOrders SET TrackingUrl = '" + labelUrl+ "', TrackingNumber = '" + trackingCode+ "' WHERE OrderId = "+id;
                }
                dao.ExecuteQuery(sql);
            }
            catch (Exception ex)
            {

            }
        }

        public string GetLabels(string shipmentId)
        {
            dao = new MSSQLDataService();
            object res = dao.ExecuteScalar("SELECT Label FROM PackageHistory WHERE ShipmentId='" + shipmentId + "'");
            return Utility.GetString(res);  
        }

        public string GetSummaryOrderIds(string batchnum)
        {
            try
            {
                dao = new MSSQLDataService();
                string res = dao.ExecuteString("SELECT DISTINCT OrderId FROM [ShopifyOrders] WHERE SummaryNumber='" + batchnum + "'");
                return res;
            }
            catch (Exception)
            {
                return "0";
            }
        }

        public string GetShipmentIds(string shipmentId)
        {
            dao = new MSSQLDataService();
            object res = dao.ExecuteScalar("SELECT ShipmentId FROM PackageHistory WHERE OrderId='" + shipmentId + "'");
            return Utility.GetString(res);
        }
    }
}
