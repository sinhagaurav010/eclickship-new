﻿using UFS.DatabaseEntity;
using UFS.Web.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace UFS.Web.DataOperations
{
    public class MySQLCommonDataOperations
    {
        private MySqlDataService dao = null;
        public List<KeyValuePairClass> GetCarriers()
        {
            List<KeyValuePairClass> lstCarriers = new List<KeyValuePairClass>();
            dao = new DatabaseEntity.MySqlDataService();
            string query = "SELECT *  FROM carriers ";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    lstCarriers.Add(
                    new KeyValuePairClass()
                    {
                        Key = Utility.GetString(item["Value"]),
                        Value = Utility.GetString(item["Description"])
                    }
                    );
                }


            }
            return lstCarriers;
        }
        public List<KeyValuePairClass> GetParcels(string carrier)
        {
            List<KeyValuePairClass> lstCarriersParcel = new List<KeyValuePairClass>();
            lstCarriersParcel.Add(new KeyValuePairClass()
            {
                Value = "Custom",
                Key = "Custom"
            });
            dao = new DatabaseEntity.MySqlDataService();
            string query = "SELECT *  FROM parcel where Carrier = '" + carrier + "'";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    lstCarriersParcel.Add(
                        new KeyValuePairClass()
                        {
                            Key = Utility.GetString(item["Parcel"]),
                            Value = Utility.GetString(item["Parcel"])
                        }
                        );
                }

            }
            return lstCarriersParcel;
        }

        public bool ApproveUser(string userId)
        {
            try
            {
                string query = "UPDATE userdetails SET IsApproved=1 WHERE UserID=" + userId;
                dao = new MySqlDataService();
                int res = dao.ExecuteQuery(query);
                return res > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateDiscountRates(string userId, string carriername, string discount)
        {
            try
            {
                string query = "UPDATE userdetails SET IsApproved=1 WHERE UserId=" + userId;
                dao = new MySqlDataService();
                int res = dao.ExecuteQuery(query);
                return res > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsDiscountExists(string userId, string carrierName)
        {
            try
            {
                string query = "SELECT COUNT(1) FROM  PackageServiceDiscount  WHERE CarrierName='"+carrierName+"' AND UserName='"+userId+"'";
                dao = new MySqlDataService();
                object res1 = dao.ExecuteScalar(query);
                int res = 0;
                return int.TryParse(res1.ToString(), out res) && res > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
