﻿using UFS.DatabaseEntity;
using UFS.Messaging.EmailService;
using UFS.Web.BusinesEntities;
using UFS.Web.Common;
using UFS.Web.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using UFS.Service.BusinessEntities;

namespace UFS.Web.DataOperations
{
    public class MSSQLUserOperations
    {
        private MSSQLDataService dao = null;
        public MSSQLUserOperations()
        {

        }
        public void DeleteDiscounts(int id)
        {
            dao = new MSSQLDataService();
            string query = "DELETE FROM PackageServiceDiscount WHERE Id=" + id.ToString();
            dao.ExecuteQuery(query);

        }

        public void ClearOrders()
        {
            try
            {
                dao = new MSSQLDataService();
                dao.ExecuteQuery("DELETE FROM OrderLineItems");
                dao = new MSSQLDataService();
                dao.ExecuteQuery(" DELETE FROM ShopifyOrders");
            }
            catch (Exception ex)
            {
            }
        }

        public List<UFS.Service.BusinessEntities.OrderDisplay> GetSummaryOrders(int userId, string summaryId)
        {
            List<UFS.Service.BusinessEntities.OrderDisplay> lstOrders = new List<Service.BusinessEntities.OrderDisplay>();
            dao = new MSSQLDataService();
            UFS.Service.BusinessEntities.OrderDisplay ordr;
            string query = "SELECT *  FROM ShopifyOrders WHERE UserId='" + userId.ToString() + "' AND ISNULL(BatchNumber,'')='' AND ISNULL(SummaryNumber,'')='" + summaryId + "'";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ordr = new Service.BusinessEntities.OrderDisplay();
                    ordr.CustomerName = Utility.GetString(ds.Tables[0].Rows[i]["FromCustomerFName"]) + " " + Utility.GetString(ds.Tables[0].Rows[i]["FromCustomerLName"]);
                    ordr.CustomerAddress = Utility.GetString(ds.Tables[0].Rows[i]["FromCustomerAddress1"]) + " " + Utility.GetString(ds.Tables[0].Rows[i]["FromCustomerAddress2"]) + " " + Utility.GetString(ds.Tables[0].Rows[i]["FromCustomerCity"]) + "-" + Utility.GetString(ds.Tables[0].Rows[i]["FromCustomerPin"]);
                    ordr.OrderId = Utility.GetLong(ds.Tables[0].Rows[i]["OrderId"]);
                    ordr.RecieverName = Utility.GetString(ds.Tables[0].Rows[i]["ShippingFName"]) + " " + Utility.GetString(ds.Tables[0].Rows[i]["ShippingLName"]);
                    ordr.RecieverAddress = Utility.GetString(ds.Tables[0].Rows[i]["ShippingAddress1"]) + " " + Utility.GetString(ds.Tables[0].Rows[i]["ShippingAddress2"]) + " " + Utility.GetString(ds.Tables[0].Rows[i]["ShippingCity"]) + "-" + Utility.GetString(ds.Tables[0].Rows[i]["ShippingPin"]);
                    ordr.TrackingUrl = Utility.GetString(ds.Tables[0].Rows[i]["TrackingUrl"]);
                    ordr.OrderNumber = Utility.GetString(ds.Tables[0].Rows[i]["OrderNumber"]);
                    ordr.OrderStatus = Utility.GetString(ds.Tables[0].Rows[i]["OrderStatus"]);
                    ordr.ShippingService = Utility.GetString(ds.Tables[0].Rows[i]["ShippingService"]);
                    ordr.OrderDate = Utility.GetDateAsString(ds.Tables[0].Rows[i]["OrderDate"]);
                    lstOrders.Add(ordr);
                }

            }
            return lstOrders;
        }


        public object GetDiscounts(string carriername)
        {
            List<string> data = new List<string>();
            dao = new MSSQLDataService();
            string query = "SELECT *  FROM CarrierServices WHERE CarrierName='" + carriername + "'";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    data.Add(ds.Tables[0].Rows[i]["ServiceName"].ToString());
                }
            }

            return data;
        }
        public void AddDiscounts(int userId, string carriername, string discount, string service, bool isUpdate = false, int id = 0)
        {
            dao = new MSSQLDataService();
            string sqlQuery = "";
            if (!isUpdate && id == 0)
            {
                sqlQuery = "INSERT INTO PackageServiceDiscount ([CarrierName], [ServiceName], [PercDisc], [UserName]) VALUES" +
                   "('" + carriername + "', '" + service + "', " + discount + ", '" + userId.ToString() + "');";
            }
            if (isUpdate && id > 0)
            {
                sqlQuery = "UPDATE PackageServiceDiscount SET PercDisc=" + discount + " WHERE id =" + id.ToString();
            }
            dao.ExecuteQuery(sqlQuery);
        }

        public object GetOrderIdByNum(string orderids, int userId)
        {
            try
            {
                string query = " SELECT OrderId FROM ShopifyOrders WHERE OrderNumber = '" + orderids + "' AND UserId = " + userId.ToString();
                object res = new MSSQLDataService().ExecuteScalar(query);
                return int.Parse(res.ToString());
            }
            catch (Exception x)
            {

                throw;
            }
        }

        public bool AddUser(UserDetail usr, ref string msg)
        {
            try
            {
                int userid = 0;
                bool isUserExists = this.IsUserExists(usr.EmailAddress, ref userid);
                if (!isUserExists || (isUserExists && usr.IsUpdate))
                {
                    dao = new MSSQLDataService();
                    string dyPassword = EncryptionService.EncryptByBase64String(usr.Password);
                    string sql = "INSERT INTO userdetails (EmailId, FirstName, LastName, Password, RoleId, Company,EmailAddress1,EmailAddress2,AuthKey,ShopifyApiKey,ShopifyApiPassword,ShopifyStoreUrl) VALUES " +
                                  "('" + usr.EmailAddress + "', '" + usr.FirstName + "', '" + usr.LastName + "', '" + dyPassword + "', '" + usr.RoleId + "', '" + usr.Company + "','" + usr.EmailAddress1 + "','" + usr.EmailAddress2 + "','" + usr.AuthCode + "','" + usr.ShopifyApiKey + "','" + usr.ShopifyApiPassword + "','" + usr.ShpifyStoreUrl + "');";
                    if (usr.IsUpdate)
                    {
                        sql = "UPDATE userdetails SET FirstName='" + usr.FirstName + "', LastName='" + usr.LastName + "', Company='" + usr.Company + "', EmailAddress1='" + usr.EmailAddress1 + "', EmailAddress2='" + usr.EmailAddress2 + "'";
                        if (!string.IsNullOrWhiteSpace(usr.AuthCode) && !string.IsNullOrWhiteSpace(usr.ShopifyApiKey) && !string.IsNullOrWhiteSpace(usr.ShopifyApiPassword) && !string.IsNullOrWhiteSpace(usr.ShpifyStoreUrl))
                        {
                            sql = sql + " , AuthKey='" + usr.AuthCode + "', ShopifyApiKey='" + usr.ShopifyApiKey + "', ShopifyApiPassword='" + usr.ShopifyApiPassword + "', ShopifyStoreUrl='" + usr.ShpifyStoreUrl + "'";
                        }

                        sql = sql + " WHERE EmailId='" + usr.EmailAddress + "'";
                    }
                    else
                    {

                    }
                    dao.ExecuteQuery(sql);
                    this.IsUserExists(usr.EmailAddress, ref userid);
                    if (userid > 0)
                    {
                        AddUserAddress(userid, usr.Address, usr.IsUpdate);
                        AddAuthenticationDetail(userid, usr.AuthDetails);
                    }
                    usr.UserId = userid;
                    try
                    {
                        if (!usr.IsUpdate)
                        {
                            SendEmailToUser(usr);
                            SendEmailToAdmin(usr);
                        }
                    }
                    catch (Exception) { }
                }
                else
                {
                    msg = "Email address already registered";
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                return false;
            }
        }

        public string GetOrderIdsOfSummary(string summaryNumber)
        {
            return new DatabaseEntity.MSSQLDataService().ExecuteString("SELECT OrderId from ShopifyOrders WHERE SummaryNumber = '" + summaryNumber + "'");
        }

        public List<UFS.Service.BusinessEntities.OrderDisplay> GetSummaryOrders(int userId)
        {
            try
            {
                List<UFS.Service.BusinessEntities.OrderDisplay> lstOrders = new List<Service.BusinessEntities.OrderDisplay>();
                dao = new MSSQLDataService();
                UFS.Service.BusinessEntities.OrderDisplay ordr;
                string query = @"SELECT SummaryNumber,BatchStatus,SummaryDate, FirstName+' '+ LastName As 'ProcessedBy',Updated,LastUpdated
                            FROM ShopifyOrders 
                            LEFT JOIN userdetails ON ShopifyOrders.UserId  = userdetails.UserId
                            WHERE ShopifyOrders.UserId='" + userId.ToString() + "' AND ISNULL(SummaryNumber,'')<>''";

                // string query = "SELECT BatchNumber,BatchStatus,  FROM ShopifyOrders WHERE UserId='" + userId.ToString() + "' AND ISNULL(BatchNumber,'')<>'' GROUP BY BatchNumber,BatchStatus";
                DataSet ds = dao.ExecuteDataSet(query);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ordr = new Service.BusinessEntities.OrderDisplay();
                        ordr.BatchNumber = Utility.GetString(ds.Tables[0].Rows[i]["SummaryNumber"]);
                        ordr.BatchStatus = "";
                        ordr.OrderNumber = GetOrdersOfSummary(Utility.GetString(ds.Tables[0].Rows[i]["SummaryNumber"])).TrimEnd(new char[] { ',' });
                        ordr.UserName = Utility.GetString(ds.Tables[0].Rows[i]["ProcessedBy"]);
                        ordr.ProcessedDate = Utility.GetDateAsString(ds.Tables[0].Rows[i]["SummaryDate"]);
                        lstOrders.Add(ordr);
                    }

                }
                return lstOrders;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string GetOrdersOfSummary(string summaryNumber)
        {
            return new DatabaseEntity.MSSQLDataService().ExecuteString("SELECT OrderNumber from ShopifyOrders WHERE SummaryNumber = '" + summaryNumber + "'");
        }
        public List<CarierDiscount> GetDiscounts(int userId, string career)
        {
            List<CarierDiscount> discounts = new List<CarierDiscount>();
            try
            {
                dao = new MSSQLDataService();
                string query = "SELECT *  FROM PackageServiceDiscount WHERE CarrierName='" + career + "' AND UserName='" + userId.ToString() + "'";
                DataSet ds = dao.ExecuteDataSet(query);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        discounts.Add(new CarierDiscount()
                        {
                            CareerName = career,
                            Id = Utility.GetInt(ds.Tables[0].Rows[i]["Id"]),
                            Discount = Utility.GetString(ds.Tables[0].Rows[i]["PercDisc"]),
                            ServiceName = Utility.GetString(ds.Tables[0].Rows[i]["ServiceName"])
                        });

                    }
                }
            }
            catch (Exception ex)
            {
            }
            return discounts;
        }

        public string UpdateDiscountRates(string userId, string carriername, string discount, string service)
        {
            try
            {
                string query = "";
                dao = new MSSQLDataService();
                if (IsDiscountExists(userId, carriername, service))
                {
                    query = "DELETE FROM PackageServiceDiscount WHERE UserName='" + userId + "' AND CarrierName='" + carriername + "' AND ServiceName='" + service + "'";
                    dao.ExecuteQuery(query);
                }
                query = "INSERT INTO PackageServiceDiscount(CarrierName,PercDisc,UserName,ServiceName) VALUES('" + carriername + "','" + discount + "','" + userId + "','" + service + "')";
                int res = dao.ExecuteQuery(query);
                return res > 0 ? "true" : "false";
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool IsDiscountExists(string userId, string carrierName, string service)
        {
            try
            {
                string query = "SELECT COUNT(1) FROM  PackageServiceDiscount  WHERE CarrierName='" + carrierName + "' AND UserName='" + userId + "' AND ServiceName='" + service + "'";
                dao = new MSSQLDataService();
                object res1 = dao.ExecuteScalar(query);
                int res = 0;
                return int.TryParse(res1.ToString(), out res) && res > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public void AddShopifyOrderDetail(UFS.Web.ShopifyOrder.MainOrder ordr, int userId)
        {
            try
            {
                if (ordr != null && ordr.Order != null && ordr.Order.line_items != null && ordr.Order.line_items.Count > 0)
                {
                    string query = "";
                    foreach (var item in ordr.Order.line_items)
                    {
                        double weight = item.grams * 0.00220462;
                        query = "IF NOT EXISTS(SELECT * FROM OrderLineItems WHERE OrderNumber='" + ordr.Order.order_number.ToString() + "' AND SKU='" + item.sku + "') BEGIN INSERT INTO[dbo].[OrderLineItems]([OrderNumber],[SKU],[Title],[Quantity],[Price],[Weight],[UserId]) " +
                                " VALUES('" + ordr.Order.order_number.ToString() +
                                        "','" + item.sku +
                                        "','" + item.title.Replace("'", "''") +
                                        "'," + item.quantity.ToString() +
                                        "," + item.price +
                                        "," + weight.ToString("") +
                                        "," + userId.ToString() + ")  END";
                        dao = new MSSQLDataService();
                        try
                        {
                            dao.ExecuteQuery(query);
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public string UpdateShopifyOrderDetail(List<SummaryData> data, string orderId, string service, int userId)
        {
            try
            {
                if (data != null && data.Count > 0 && !string.IsNullOrWhiteSpace(orderId) && !string.IsNullOrWhiteSpace(service) && userId > 0)
                {
                    dao = new MSSQLDataService();
                    string query = "";
                    foreach (var item in data)
                    {
                        query = "UPDATE OrderLineItems SET Weight=" + Convert.ToDecimal(item.Weight).ToString() + ",Quantity=" + item.Quantity + " WHERE OrderNumber='" + orderId + "' AND SKU='" + item.SKU + "' AND UserId =  " + userId.ToString();

                        try
                        {
                            dao.ExecuteQuery(query);
                        }
                        catch (Exception e)
                        {
                            return "-1";
                        }
                    }
                    query = "UPDATE ShopifyOrders SET ShippingService='" + service + "' WHERE OrderNumber = '" + orderId + "' AND UserId = " + userId.ToString();
                    dao.ExecuteQuery(query);
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }
        public UFS.Web.ShopifyOrder.MainOrder GetShopifyOrderDetail(string orderId, int userId)
        {
            UFS.Web.ShopifyOrder.MainOrder order = new ShopifyOrder.MainOrder();
            dao = new MSSQLDataService();
            string query = @"SELECT *
                            FROM OrderLineItems 
                            WHERE OrderNumber='" + orderId + "'";

            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                order.Order = new ShopifyOrder.Order();
                order.Order.line_items = new List<ShopifyOrder.LineItem>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    order.Order.line_items.Add(new ShopifyOrder.LineItem()
                    {
                        sku = Utility.GetString(ds.Tables[0].Rows[i]["SKU"]),
                        title = Utility.GetString(ds.Tables[0].Rows[i]["Title"]),
                        grams = Utility.GetDoubletoInt(ds.Tables[0].Rows[i]["Weight"]),
                        grams1 = Utility.GetString(ds.Tables[0].Rows[i]["Weight"]),
                        quantity = Utility.GetInt(ds.Tables[0].Rows[i]["Quantity"]),
                        price = Utility.GetString(ds.Tables[0].Rows[i]["Price"])
                    });
                }

            }
            return order;
        }

        public string GetOrderLabel(string orderId, int userId)
        {
            try
            {
                dao = new MSSQLDataService();
                string query = "SELECT TrackingUrl FROM ShopifyOrders WHERE UserId=" + userId.ToString() + " AND (replace(OrderNumber,'#','')='" + orderId.Replace("#", "").Replace("_", "") + "' OR (BatchNumber = '" + orderId + "' AND BatchStatus='1'))";
                object returnVal = dao.ExecuteString(query);
                if (returnVal != null)
                {
                    return returnVal.ToString();
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public List<UFS.Service.BusinessEntities.OrderDisplay> GetBatchOrders(int userId)
        {
            List<UFS.Service.BusinessEntities.OrderDisplay> lstOrders = new List<Service.BusinessEntities.OrderDisplay>();
            dao = new MSSQLDataService();
            UFS.Service.BusinessEntities.OrderDisplay ordr;
            string query = @"SELECT BatchNumber,BatchStatus, FirstName+' '+ LastName As 'ProcessedBy',Updated,LastUpdated
                            FROM ShopifyOrders 
                            LEFT JOIN userdetails ON ShopifyOrders.UserId  = userdetails.UserId
                            WHERE ShopifyOrders.UserId='" + userId.ToString() + "' AND ISNULL(BatchNumber,'')<>''";

            // string query = "SELECT BatchNumber,BatchStatus,  FROM ShopifyOrders WHERE UserId='" + userId.ToString() + "' AND ISNULL(BatchNumber,'')<>'' GROUP BY BatchNumber,BatchStatus";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ordr = new Service.BusinessEntities.OrderDisplay();
                    ordr.BatchNumber = Utility.GetString(ds.Tables[0].Rows[i]["BatchNumber"]);
                    ordr.BatchStatus = Utility.GetString(ds.Tables[0].Rows[i]["BatchStatus"]);
                    ordr.UserName = Utility.GetString(ds.Tables[0].Rows[i]["ProcessedBy"]);
                    ordr.ProcessedDate = Utility.GetDateTimeString(ds.Tables[0].Rows[i]["Updated"]);
                    lstOrders.Add(ordr);
                }

            }
            return lstOrders;
        }

        public void AddShopifyOrders(UFS.Service.BusinessEntities.CustomerOrder ordr, int userId)
        {
            try
            {
                dao = new MSSQLDataService();
                int i = 0;
                string query = "";
                foreach (var item in ordr.orders)
                {
                    string carrier = GetCarrierName(item.shipping_lines);
                    if (item.customer != null)
                    {
                        query = "  if(NOT EXISTS (SELECT * FROM ShopifyOrders WHERE OrderId = " + item.order_number + " AND UserId = " + userId.ToString() + " )) BEGIN " +
                                    " INSERT INTO[dbo].[ShopifyOrders]                     " +
                                    "               ([OrderId]                                " +
                                    "               ,[FromCustomerFName]                      " +
                                    "               ,[FromCustomerLName]                      " +
                                    "               ,[FromCustomerAddress1]                   " +
                                    "               ,[FromCustomerAddress2]                   " +
                                    "               ,[FromCustomerCity]                       " +
                                    "               ,[FromCustomerPin]                        " +
                                    "               ,[ShippingFName]                          " +
                                    "               ,[ShippingLName]                          " +
                                    "               ,[ShippingAddress1]                       " +
                                    "               ,[ShippingAddress2]                       " +
                                    "               ,[ShippingCity]                           " +
                                    "               ,[ShippingPin]                            " +
                                    "               ,[TrackingUrl]                             " +
                                    "               ,[UserId]                          " +
                                    "               ,[OrderDate]                          " +
                                    "               ,[OrderStatus]                          " +
                                    "               ,[ShippingService]                           " +
                                    "               ,[TrackingNumber]                          " +
                                       "            ,[OrderNumber]                          " +
                                    "               ,[ShipVia])                           " +
                                    "         VALUES                                          " +
                                    "               (" + item.id + "                         " +
                                    "               ,'" + (item.billing_address.first_name != null ? item.billing_address.first_name.Replace("'", "''") : "") + "'        " +
                                    "               ,'" + (item.billing_address.last_name != null ? item.billing_address.last_name.Replace("'", "''") : "") + "'     " +
                                    "               ,'" + (item.billing_address.address1 != null ? item.billing_address.address1.Replace("'", "''") : "") + "' " +
                                    "               ,'" + (item.billing_address.address2 != null ? item.billing_address.address2.Replace("'", "''") : "") + "' " +
                                    "               ,'" + (item.billing_address.city != null ? item.billing_address.city.Replace("'", "''") : "") + "' " +
                                    "               ,'" + (item.billing_address.zip != null ? item.billing_address.zip : "") + "' " +
                                    "               ,'" + (item.shipping_address.first_name != null ? item.shipping_address.first_name.Replace("'", "''") : "") + "' " +
                                    "               ,'" + (item.shipping_address.last_name != null ? item.shipping_address.last_name.Replace("'", "''") : "") + "' " +
                                    "               ,'" + (item.shipping_address != null ? item.shipping_address.address1.Replace("'", "''") : "") + "' " +
                                    "               ,'" + (item.shipping_address.address2 != null ? item.shipping_address.address2.Replace("'", "''") : "") + "' " +
                                    "               ,'" + (item.shipping_address.city != null ? item.shipping_address.city.Replace("'", "''") : "") + "' " +
                                    "               ,'" + (item.shipping_address.zip != null ? item.shipping_address.zip : "") + "' " +
                                    "               ,' ' " +
                                    "               ,'" + userId.ToString() + "' " +
                                    "               ,'" + item.created_at.ToString() + "' " +
                                    "               ,'" + (item.fulfillment_status != null ? item.fulfillment_status : "") + "' " +
                                    "               ,'" + (item.shipping_lines != null && item.shipping_lines.Length > 0 ? item.shipping_lines[0].title : "Not Available") + "' " +
                                    "               ,' ' " +
                                      "               ,'" + (item.name != null ? item.name.Replace("'", "''") : "") + "' " +
                                    "               ,'" + carrier.Replace("'", "''") + "')  END ";
                        if (!string.IsNullOrWhiteSpace(query))
                        {                           
                            dao.ExecuteQuery(query);
                        }
                    }
                }


            }
            catch (Exception ex)
            {

            }
        }

        private string GetCarrierName(Shipping_Lines[] shipping_lines)
        {
            string res = "Federal Express";
            try
            {
                if (shipping_lines != null && shipping_lines.Length > 0)
                {
                    switch (shipping_lines[0].title)
                    {
                        case "Expedited (One Day)":
                        case "Economy (2 Day)":
                        case "Economy":
                            res = "Federal Express";
                            break;
                        default:
                            break;
                    }
                }

            }
            catch (Exception)
            {
            }
            return res;
        }

        public string GetService(long id, int userId)
        {
            try
            {
                dao = new MSSQLDataService();
                string query = "SELECT ShippingService FROM ShopifyOrders WHERE UserId=" + userId.ToString() + " AND OrderId='" + id.ToString() + "'";
                object returnVal = dao.ExecuteScalar(query);
                if (returnVal != null)
                {
                    return returnVal.ToString();
                }
                else
                    return string.Empty;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public string GetOrderId(string item, int userId)
        {
            try
            {
                dao = new MSSQLDataService();
                string query = "SELECT OrderId FROM ShopifyOrders WHERE UserId=" + userId.ToString() + " AND OrderNumber='" + item + "'";
                object returnVal = dao.ExecuteScalar(query);
                if (returnVal != null)
                {
                    return returnVal.ToString();
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<UFS.Service.BusinessEntities.OrderDisplay> GetOrders(int userId)
        {
            List<UFS.Service.BusinessEntities.OrderDisplay> lstOrders = new List<Service.BusinessEntities.OrderDisplay>();
            dao = new MSSQLDataService();
            UFS.Service.BusinessEntities.OrderDisplay ordr;
            string query = "SELECT *  FROM ShopifyOrders WHERE UserId='" + userId.ToString() + "' AND ISNULL(BatchNumber,'')='' AND ISNULL(SummaryNumber,'')=''";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ordr = new Service.BusinessEntities.OrderDisplay();
                    ordr.CustomerName = Utility.GetString(ds.Tables[0].Rows[i]["FromCustomerFName"]) + " " + Utility.GetString(ds.Tables[0].Rows[i]["FromCustomerLName"]);
                    ordr.CustomerAddress = Utility.GetString(ds.Tables[0].Rows[i]["FromCustomerAddress1"]) + " " + Utility.GetString(ds.Tables[0].Rows[i]["FromCustomerAddress2"]) + " " + Utility.GetString(ds.Tables[0].Rows[i]["FromCustomerCity"]) + "-" + Utility.GetString(ds.Tables[0].Rows[i]["FromCustomerPin"]);
                    ordr.OrderId = Utility.GetLong(ds.Tables[0].Rows[i]["OrderId"]);
                    ordr.RecieverName = Utility.GetString(ds.Tables[0].Rows[i]["ShippingFName"]) + " " + Utility.GetString(ds.Tables[0].Rows[i]["ShippingLName"]);
                    ordr.RecieverAddress = Utility.GetString(ds.Tables[0].Rows[i]["ShippingAddress1"]) + " " + Utility.GetString(ds.Tables[0].Rows[i]["ShippingAddress2"]) + " " + Utility.GetString(ds.Tables[0].Rows[i]["ShippingCity"]) + "-" + Utility.GetString(ds.Tables[0].Rows[i]["ShippingPin"]);
                    ordr.TrackingUrl = Utility.GetString(ds.Tables[0].Rows[i]["TrackingUrl"]);
                    ordr.OrderNumber = Utility.GetString(ds.Tables[0].Rows[i]["OrderNumber"]);
                    ordr.OrderStatus = Utility.GetString(ds.Tables[0].Rows[i]["OrderStatus"]);
                    ordr.ShippingService = Utility.GetString(ds.Tables[0].Rows[i]["ShippingService"]);
                    ordr.OrderDate = Utility.GetDateAsString(ds.Tables[0].Rows[i]["OrderDate"]);
                    ordr.SuccessCount = GetItemsCount(Utility.GetString(ds.Tables[0].Rows[i]["OrderNumber"]));
                    lstOrders.Add(ordr);
                }

            }
            return lstOrders;
        }

        private int GetItemsCount(string orderNumber)
        {
            return Utility.GetInt(new MSSQLDataService().ExecuteScalar("select SUM(CASE WHEN SKU LIKE '%DOZ%' THEN Quantity*12 ELSE Quantity END) FROM OrderLineItems WHERE OrderNumber = " + orderNumber));
        }

        private void AddAuthenticationDetail(int userid, ShopifyAuthentication authDetails)
        {
            try
            {
                if (userid > 0 && authDetails != null
                    && (((!string.IsNullOrWhiteSpace(authDetails.APIKey)
                    && !string.IsNullOrWhiteSpace(authDetails.APIPassword))
                    || !string.IsNullOrWhiteSpace(authDetails.AuthKey))
                    && !string.IsNullOrWhiteSpace(authDetails.StoreURL)))
                {
                    dao = new MSSQLDataService();
                    string sqlQuery = "UPDATE userdetails SET AuthKey = '" + authDetails.AuthKey + "', ShopifyApiKey = '" + authDetails.APIKey + "', ShopifyApiPassword = '" +
                        authDetails.APIPassword + "', ShopifyStoreUrl = '" + authDetails.StoreURL + "' WHERE UserId = " + userid.ToString();
                    dao.ExecuteQuery(sqlQuery);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void AddUserAddress(int userid, UserAddress address, bool isUpdate)
        {
            dao = new MSSQLDataService();
            string sqlQuery = "INSERT INTO useraddress (UserId, Street1, Street2, City, State, Zip, Country, Phone) VALUES" +
                "('" + userid.ToString() + "', '" + address.Street1 + "', '" + address.Street2 + "', '" + address.City + "', '" + address.State + "', '" + address.Zip + "', '" + address.Country + "', '" + address.Phone + "');";
            if (isUpdate)
            {
                sqlQuery = "UPDATE useraddress SET Street1='" + address.Street1 + "',Street2='" + address.Street2 + "',City='" + address.City + "',State='" + address.State + "',Zip='" + address.Zip + "',Country='" + address.Country + "',Phone='" + address.Phone + "' WHERE UserId=" + userid.ToString();
            }
            dao.ExecuteQuery(sqlQuery);

        }
        private bool IsValidaUser(int userId, string password)
        {
            dao = new MSSQLDataService();
            string query = "SELECT Password FROM userdetails WHERE UserId=" + userId.ToString();
            object returnVal = dao.ExecuteScalar(query);
            if (returnVal != null)
            {
                return password.Trim().Equals(DecryptionService.DecryptByBase64String(returnVal.ToString()));
            }
            else
                return false;
        }

        private bool IsUserApproved(int userId)
        {
            int res = 0;
            dao = new MSSQLDataService();
            string query = "SELECT COUNT(1) FROM userdetails WHERE UserId=" + userId.ToString() + " AND ISNULL(IsApproved,0)=1";
            object returnVal = dao.ExecuteScalar(query);
            if (returnVal != null)
            {
                return int.TryParse(returnVal.ToString(), out res) && res > 0;
            }
            else
                return false;
        }

        public UserDetail ValidateLogin(string email, string password)
        {
            int userId = 0;
            UserDetail usr = null;
            if (IsUserExists(email, ref userId))
            {
                if (IsUserApproved(userId) && IsValidaUser(userId, password))
                {
                    usr = GetUsrDetail(userId);
                }
            }
            return usr;
        }

        public void SendEmailToUser(UserDetail usr)
        {
            try
            {
                EmailService email = new EmailService(usr.EmailAddress, false);
                email.MailBody = "<p>Hello " + usr.FirstName + " " + usr.LastName + " !</p> <br/> You have successfully created your account on eCilckShip. After admin approves your account you will be able to login to your account.<br/><b>Regards,<br/>eClickShip";
                email.Subject = "Registration on eClickShip";
                string resp = email.SendMail();

            }
            catch (Exception)
            {

            }
        }

        private void SendApprovalEmailToUser(string userId, int approval)
        {
            try
            {
                UserDetail usr = GetUsrDetail(Convert.ToInt32(userId));
                EmailService email = new EmailService(usr.EmailAddress, false);
                if (approval == 1)
                {
                    email.MailBody = "<p>Hello " + usr.FirstName + " " + usr.LastName + " !</p> <br/> Your account on  eClickShip has been approved.You can now access the webpage with your username and password.<br/><b>Regards,<br/>eClickShip";
                    email.Subject = "Account Approval on eClickShip";
                }
                else
                {
                    email.MailBody = "<p>Hello " + usr.FirstName + " " + usr.LastName + " !</p> <br/> Your account on  eClickShip has been Disabled.You can not  access the application. Please contact us.<br/><b>Regards,<br/>eClickShip";
                    email.Subject = "Account Disabled on eClickShip";
                }
                string resp = email.SendMail();

            }
            catch (Exception)
            {

            }
        }
        public void SendEmailToAdmin(UserDetail usr)
        {
            try
            {
                string toMailAddress = WebConfigurationManager.AppSettings["CCEmailAddress"].ToString();

                EmailService email = new EmailService("", toMailAddress);
                email.MailBody = "<p>Hello Administrator <br/> A new user with below details has registered on eClickShip.<br/>" +
                    "Name : <b>" + usr.FirstName + " " + usr.LastName + "</b><br/>" +
                    "Company : <b>" + usr.Company + "</b><br/>" +
                    "Email Address : <b>" + usr.EmailAddress + "</b><br/>" +
                    "Login to your Administrator account and approve this user. You can approve this user by logging into below url</p> <br/>" +
                    "<b>http://www.eclickship.com/shopifylogin/Account/ManageUsers?UserId=" + usr.UserId + "</b>";
                email.Subject = "New User registered on eClickShip";
                string resp = email.SendMail();

            }
            catch (Exception)
            {

            }
        }

        private bool IsUserExists(string emailId, ref int userId)
        {
            dao = new MSSQLDataService();
            string query = "SELECT COUNT(*) AS Count,UserId FROM userdetails WHERE EmailId='" + emailId + "' group by UserId";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && Utility.GetInt(ds.Tables[0].Rows[0]["Count"]) > 0)
            {
                userId = Utility.GetInt(ds.Tables[0].Rows[0]["UserId"]);
                return true;
            }
            else
                return false;
        }
        public UserAddress GetUsrAddress(int userId)
        {
            UserAddress addr = new UserAddress();
            dao = new MSSQLDataService();
            string query = "SELECT *  FROM useraddress WHERE UserId='" + userId.ToString() + "'";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                addr.Street1 = Utility.GetString(ds.Tables[0].Rows[0]["Street1"]);
                addr.Street2 = Utility.GetString(ds.Tables[0].Rows[0]["Street2"]);
                addr.City = Utility.GetString(ds.Tables[0].Rows[0]["City"]);
                addr.State = Utility.GetString(ds.Tables[0].Rows[0]["State"]);
                addr.Zip = Utility.GetString(ds.Tables[0].Rows[0]["Zip"]);
                addr.Country = Utility.GetString(ds.Tables[0].Rows[0]["Country"]);
                addr.Phone = Utility.GetString(ds.Tables[0].Rows[0]["Phone"]);
                addr.IsResidential = Utility.GetBoolean(ds.Tables[0].Rows[0]["IsResidential"]);
            }
            return addr;
        }
        public UserDetail GetUsrDetail(int userId)
        {
            UserDetail usr = new UserDetail();
            dao = new MSSQLDataService();
            string query = "SELECT *  FROM userdetails WHERE UserId='" + userId.ToString() + "'";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                usr.UserId = Utility.GetInt(ds.Tables[0].Rows[0]["UserId"]);
                usr.EmailAddress = Utility.GetString(ds.Tables[0].Rows[0]["EmailId"]);
                usr.FirstName = Utility.GetString(ds.Tables[0].Rows[0]["FirstName"]);
                usr.LastName = Utility.GetString(ds.Tables[0].Rows[0]["LastName"]);
                usr.RoleId = Utility.GetString(ds.Tables[0].Rows[0]["RoleId"]);
                usr.Company = Utility.GetString(ds.Tables[0].Rows[0]["Company"]);
                usr.IsApproved = Utility.GetInt(ds.Tables[0].Rows[0]["IsApproved"]);
                usr.EmailAddress1 = Utility.GetString(ds.Tables[0].Rows[0]["EmailAddress1"]);
                usr.EmailAddress2 = Utility.GetString(ds.Tables[0].Rows[0]["EmailAddress2"]);
                usr.AuthCode = Utility.GetString(ds.Tables[0].Rows[0]["AuthKey"]);
                usr.ShopifyApiKey = Utility.GetString(ds.Tables[0].Rows[0]["ShopifyApiKey"]);
                usr.ShopifyApiPassword = Utility.GetString(ds.Tables[0].Rows[0]["ShopifyApiPassword"]);
                usr.ShpifyStoreUrl = Utility.GetString(ds.Tables[0].Rows[0]["ShopifyStoreUrl"]);
                usr.Address = GetUsrAddress(userId);
                usr.AuthDetails = new ShopifyAuthentication()
                {
                    AuthKey = Utility.GetString(ds.Tables[0].Rows[0]["AuthKey"]),
                    APIKey = Utility.GetString(ds.Tables[0].Rows[0]["ShopifyApiKey"]),
                    APIPassword = Utility.GetString(ds.Tables[0].Rows[0]["ShopifyApiPassword"]),
                    StoreURL = Utility.GetString(ds.Tables[0].Rows[0]["ShopifyStoreUrl"])
                };
            }
            return usr;
        }

        private AuthorizationDetails GetAuthDetails(int userId)
        {
            try
            {
                AuthorizationDetails detail = new AuthorizationDetails();
                dao = new MSSQLDataService();
                string query = "SELECT *  FROM AuthenticationDetails WHERE UserId=" + userId.ToString() + "";
                DataSet ds = dao.ExecuteDataSet(query);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    detail.FedExKey = Utility.GetString(ds.Tables[0].Rows[0]["FedExAuthKey"]);
                    detail.USPSKey = Utility.GetString(ds.Tables[0].Rows[0]["USPSAuthKey"]);
                    detail.UPSKey = new AuthorizationDetails.UPSKeys()
                    {
                        AccessLicenseNumber = Utility.GetString(ds.Tables[0].Rows[0]["UPSAccessLicenseNumber"]),
                        ShipperNumber = Utility.GetString(ds.Tables[0].Rows[0]["UPSShipperNumber"]),
                        ShipperAccountNumber = Utility.GetString(ds.Tables[0].Rows[0]["UPSShipperAccountNumber"]),
                        UserName = Utility.GetString(ds.Tables[0].Rows[0]["UPSUserName"]),
                        Password = Utility.GetString(ds.Tables[0].Rows[0]["UPSPassword"])
                    };
                }
                return detail;
            }
            catch (Exception ex)
            {
                return new AuthorizationDetails();
            }
        }

        public UserDetail GetUserByEmail(string email)
        {
            int userid = 0;
            if (this.IsUserExists(email, ref userid))
            {
                return GetUsrDetail(userid);
            }
            else
            {
                return null;
            }
        }

        public bool AddToAddress(int userId, Address address)
        {
            dao = new MSSQLDataService();
            string sqlQuery = "INSERT INTO usertoaddress (UserId, Street1, Street2, City, State, Zip, Country, Phone,Company,Name) VALUES" +
                "('" + userId.ToString() + "', '" + address.Street1 + "', '" + address.Street2 + "', '" + address.City + "', '" + address.State + "', '" + address.Zip + "', '" + address.Country + "', '" + address.Phone + "','" + address.Company + "','" + address.Name + "');";
            dao.ExecuteQuery(sqlQuery);
            return true;
        }
        public List<KeyValuePairClass> GetUserToAddresses(int userId)
        {
            List<KeyValuePairClass> addresses = new List<KeyValuePairClass>();
            dao = new MSSQLDataService();
            addresses.Add(new KeyValuePairClass() { Key = "0", Value = "-- Select Address --" });
            string query = "SELECT *  FROM usertoaddress WHERE UserId='" + userId.ToString() + "'";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    addresses.Add(new KeyValuePairClass()
                    {
                        Key = Utility.GetString(item["AddressId"]),
                        Value = (!string.IsNullOrWhiteSpace(Utility.GetString(item["Name"])) ? Utility.GetString(item["Name"]) : "") + " " + (!string.IsNullOrWhiteSpace(Utility.GetString(item["Company"])) ? ", " + Utility.GetString(item["Company"]) : "") +
                                ", " + Utility.GetString(item["Street1"]) + " " + Utility.GetString(item["City"]) + ", " + Utility.GetString(item["State"])
                        /*  Street1 = Utility.GetString(item["Street1"]),
                          Street2 = Utility.GetString(item["Street2"]),
                          City = Utility.GetString(item["City"]),
                          State = Utility.GetString(item["State"]),
                          Zip = Utility.GetString(item["Zip"]),
                          Country = Utility.GetString(item["Country"]),
                          Phone = Utility.GetString(item["Phone"]),
                          IsResidential = Utility.GetBoolean(item["IsResidential"]),
                          Name = Utility.GetString(item["Name"]),
                          Company = Utility.GetString(item["Company"])*/
                    });
                }

            }
            return addresses;
        }

        public Address GetUserToAddress(int addressId)
        {
            Address add = null;
            dao = new MSSQLDataService();
            string query = "SELECT *  FROM usertoaddress WHERE AddressId=" + addressId.ToString();
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataRow item = ds.Tables[0].Rows[0];
                add = new Address()
                {
                    AddressId = Utility.GetInt(item["AddressId"]),
                    Street1 = Utility.GetString(item["Street1"]),
                    Street2 = Utility.GetString(item["Street2"]),
                    City = Utility.GetString(item["City"]),
                    State = Utility.GetString(item["State"]),
                    Zip = Utility.GetString(item["Zip"]),
                    Country = Utility.GetString(item["Country"]),
                    Phone = Utility.GetString(item["Phone"]),
                    // IsResidential = Utility.GetBoolean(item["IsResidential"]),
                    Name = Utility.GetString(item["Name"]),
                    Company = Utility.GetString(item["Company"])
                };
            }
            return add;
        }

        public List<Address> GetUserAddress(string searchQuery, int userId)
        {
            List<Address> lstAddress = new List<Address>();
            dao = new MSSQLDataService();
            string query = "SELECT *  FROM usertoaddress WHERE UserId=" + userId.ToString() + " AND (Name LIKE '" + searchQuery + "%' OR Company LIKE '" + searchQuery + "%' OR City Like '" + searchQuery + "%' OR Street1 LIKE '" + searchQuery + "%' OR Street2 LIKE '" + searchQuery + "%')";
            // File.WriteAllText(@"C:\Meeku\Projects\Sanganan\Projects\EasyPost\d2k.txt", query);
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                //  DataRow item = ds.Tables[0].Rows[0];
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    lstAddress.Add(new Address()
                    {
                        Street1 = Utility.GetString(item["Street1"]),
                        Street2 = Utility.GetString(item["Street2"]),
                        City = Utility.GetString(item["City"]),
                        State = Utility.GetString(item["State"]),
                        Zip = Utility.GetString(item["Zip"]),
                        Country = Utility.GetString(item["Country"]),
                        Phone = Utility.GetString(item["Phone"]),
                        // IsResidential = Utility.GetBoolean(item["IsResidential"]),
                        Name = Utility.GetString(item["Name"]),
                        Company = Utility.GetString(item["Company"])
                    });
                }
            }
            return lstAddress;
        }


        public string SendResetPasswordLink(string userEmail)
        {
            string msg = "";
            try
            {
                string baseUrl = System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/";
                int userId = 0;

                if (this.IsUserExists(userEmail, ref userId))
                {
                    string url = baseUrl + "Account/ResetMe?iam=" + PreparePasswordReset(userEmail, userId) + "&uare=" + EncryptionService.EncryptByBase64String(userId.ToString());
                    string message = "<center><img src='http://www.eclickship.com/images/logo-name.png' alt='logo'></center><hr/><div style='font-family:ariel;font-size:25;text-align:justify'>Dear User, <br/> You have recently requested for the password reset of you account having email address <b>" + userEmail + "</b> on eClickShip. Click the below link" +
                        " to reset your password.<p><h1><a href='" + url + "' target='_blank'>Click Here To Reset</a></p>" +
                        "<br/><br/><hr/>If you haven't requested this, then please inform us at support@eclickship.com immediately<hr/><br/<br/> Regards,<br/> Admin,<br/> eClickShip.</div>";
                    string subject = "Password Reset on ECLICKSHIP";
                    var emailObject = new EmailService("", userEmail);
                    emailObject.MailBody = message;
                    emailObject.Subject = subject;
                    emailObject.SendMail();
                    msg = "We have send an email to your email address " + userEmail + ". Please follow the instruction in the email to reset your password.If you have already requested for the same then that link will not work.";
                }
                else
                {
                    msg = "Error : Email provided does not exists in our database.<br/> Please try with valid email address";
                }

            }
            catch (Exception)
            {
                throw;
            }
            return msg;
        }

        private string PreparePasswordReset(string userEmail, int userId)
        {
            string rand = UFS.Web.Common.CommonUtilities.GetRandomString(35);
            dao = new MSSQLDataService();
            string sql2 = "DELETE FROM UserPasswordReset WHERE UserId=" + userId.ToString();
            dao.ExecuteQuery(sql2);
            //  File.WriteAllText(@"C:\Meeku\Projects\Sanganan\Projects\EasyPost\d1k.txt", sql2);
            string sql = "INSERT INTO UserPasswordReset(UserId,RandData) VALUES " +
                          "(" + userId + ",'" + rand + "');";
            dao.ExecuteQuery(sql);
            // File.WriteAllText(@"C:\Meeku\Projects\Sanganan\Projects\EasyPost\d2k.txt", sql);
            return rand;
        }
        public bool IsResetCodeExists(int userId, string randCode)
        {
            string sql = "SELECT COUNT(*) from UserPasswordReset where UserId = " + userId.ToString() + " AND RandData='" + randCode + "'";
            dao = new MSSQLDataService();

            object res = dao.ExecuteScalar(sql);
            return res != null && Convert.ToInt32(res) > 0;
        }
        public bool UpdatePassword(string password, int userId)
        {
            try
            {
                string dyPassword = EncryptionService.EncryptByBase64String(password);
                dao = new MSSQLDataService();
                string sql2 = "UPDATE userdetails SET Password='" + dyPassword + "' WHERE UserId =" + userId.ToString();
                dao.ExecuteQuery(sql2);
                string sql = "DELETE FROM UserPasswordReset WHERE UserId=" + userId.ToString();
                dao.ExecuteQuery(sql);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public List<UserDetail> GetUsers()
        {
            List<UserDetail> usr = new List<UserDetail>();
            dao = new MSSQLDataService();
            string query = "SELECT *  FROM userdetails ORDER BY IsApproved ASC";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    usr.Add(new UserDetail()
                    {
                        UserId = Utility.GetInt(ds.Tables[0].Rows[i]["UserId"]),
                        EmailAddress = Utility.GetString(ds.Tables[0].Rows[i]["EmailId"]),
                        FirstName = Utility.GetString(ds.Tables[0].Rows[i]["FirstName"]),
                        LastName = Utility.GetString(ds.Tables[0].Rows[i]["LastName"]),
                        Company = Utility.GetString(ds.Tables[0].Rows[i]["Company"]),
                        EmailAddress1 = Utility.GetString(ds.Tables[0].Rows[0]["EmailAddress1"]),
                        EmailAddress2 = Utility.GetString(ds.Tables[0].Rows[0]["EmailAddress2"]),
                        IsApproved = Utility.GetInt(ds.Tables[0].Rows[i]["IsApproved"]),
                        FedExDisc = GetDiscount(Utility.GetString(ds.Tables[0].Rows[i]["UserId"]), "FedEx"),
                        USPSDisc = GetDiscount(Utility.GetString(ds.Tables[0].Rows[i]["UserId"]), "USPS"),
                        UPSDisc = GetDiscount(Utility.GetString(ds.Tables[0].Rows[i]["UserId"]), "UPS")
                    }
                );
                }
            }

            return usr;
        }

        private string GetDiscount(string userId, string carrier)
        {
            dao = new MSSQLDataService();
            string query = "SELECT PercDisc  FROM PackageServiceDiscount WHERE UserName='" + userId + "' AND CarrierName='" + carrier + "'";
            object ds = dao.ExecuteScalar(query);
            if (ds != null)
            {
                return ds.ToString();
            }
            return "";
        }



        public bool ApproveUser(string userId, int isApprove)
        {
            try
            {
                string query = "UPDATE userdetails SET IsApproved=" + isApprove + " WHERE UserID=" + userId;
                dao = new MSSQLDataService();
                int res = dao.ExecuteQuery(query);

                SendApprovalEmailToUser(userId, isApprove);

                return res > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string UpdateDiscountRates(string userId, string carriername, string discount)
        {
            try
            {
                string query = "";
                dao = new MSSQLDataService();
                if (IsDiscountExists(userId, carriername))
                {
                    query = "DELETE FROM PackageServiceDiscount WHERE UserName='" + userId + "' AND CarrierName='" + carriername + "'";
                    dao.ExecuteQuery(query);
                }
                query = "INSERT INTO PackageServiceDiscount(CarrierName,PercDisc,UserName,ServiceName) VALUES('" + carriername + "','" + discount + "','" + userId + "','" + carriername + "')";
                int res = dao.ExecuteQuery(query);
                return res > 0 ? "true" : "false";
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsDiscountExists(string userId, string carrierName)
        {
            try
            {
                string query = "SELECT COUNT(1) FROM  PackageServiceDiscount  WHERE CarrierName='" + carrierName + "' AND UserName='" + userId + "'";
                dao = new MSSQLDataService();
                object res1 = dao.ExecuteScalar(query);
                int res = 0;
                return int.TryParse(res1.ToString(), out res) && res > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GetEShipperCarrierName(string serviceId)
        {
            try
            {
                string query = "";
                dao = new MSSQLDataService();
                query = "select ShipperName from eShipperCarriers where ShipperId=" + serviceId;
                object res = dao.ExecuteScalar(query);
                return res != null ? res.ToString() : "";
            }
            catch (Exception)
            {
                return "";
            }
        }


    }
}
