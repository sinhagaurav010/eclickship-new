﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using UFS.Common;
namespace UFS.DatabaseEntity
{
    public class MSSQLDataService
    {
        private SqlConnection _con = new SqlConnection(ConfigReader.Instance.LoadData().Common.ConnectionString.ConnectionString);// ConfigurationManager.ConnectionStrings["MSSqlCon"].ToString());
        public SqlConnection Connection
        {

            get
            {
                return _con;
            }
        }
        public MSSQLDataService()
        {

        }
        public int ExecuteQuery(string query)
        {
            int result = 0;
            SqlCommand cmd = PrepareCommand(query, CommandType.Text);
            Connection.Open();
            result = cmd.ExecuteNonQuery();
            Connection.Close();
            return result;
        }
        public int ExecuteQuery(string procName, List<SqlParameter> sqlParams)
        {
            int result = 0;
            SqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
            AddParameters(cmd, sqlParams);
            Connection.Open();
            result = cmd.ExecuteNonQuery();
            Connection.Close();
            return result;
        }
        public int ExecuteQuery(string procName, List<SqlParameter> sqlParams, ref object outPutValue)
        {
            int result = 0;
            SqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
            AddParameters(cmd, sqlParams);
            Connection.Open();
            result = cmd.ExecuteNonQuery();
            try
            {
                outPutValue = cmd.Parameters[sqlParams.Count - 1].Value;
            }
            catch { outPutValue = null; }
            Connection.Close();
            return result;
        }

        public int ExecuteQuery(string procName, List<SqlParameter> sqlParams, ref object outPutValue1, ref object outputValue2)
        {
            int result = 0;
            SqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
            AddParameters(cmd, sqlParams);
            Connection.Open();
            result = cmd.ExecuteNonQuery();
            try
            {
                outPutValue1 = cmd.Parameters[sqlParams.Count - 2].Value;
                outputValue2 = cmd.Parameters[sqlParams.Count - 1].Value;
            }
            catch { outPutValue1 = null; outputValue2 = null; }
            Connection.Close();
            return result;
        }

        public Object ExecuteScalar(string query)
        {
            SqlCommand cmd = PrepareCommand(query, CommandType.Text);
            Connection.Open();
            Object obj = cmd.ExecuteScalar();
            Connection.Close();
            return obj;

        }
        public Object ExecuteScalar(string procName, List<SqlParameter> sqlParams)
        {
            SqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
            AddParameters(cmd, sqlParams);
            Connection.Open();
            object obj = cmd.ExecuteScalar();
            Connection.Close();
            return obj;
        }
        public DataSet ExecuteDataSet(string query)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = null;
            SqlCommand cmd = PrepareCommand(query, CommandType.Text);
            da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public DataSet ExecuteDataSet(string procName, List<SqlParameter> sqlParams)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = null;
            SqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
            AddParameters(cmd, sqlParams);
            da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public DataSet ExecuteDataSet(string procName, List<SqlParameter> sqlParams, ref object outPutValue)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = null;
            SqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
            AddParameters(cmd, sqlParams);
            da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            try
            {
                outPutValue = cmd.Parameters[sqlParams.Count - 1].Value;
            }
            catch { outPutValue = null; }
            return ds;
        }

        public string ExecuteString(string query)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = null;
            SqlCommand cmd = PrepareCommand(query, CommandType.Text);
            da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return GetStringFromDataSet(ds);
        }
        public string ExecuteString(string procName, List<SqlParameter> sqlParams)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = null;
            SqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
            AddParameters(cmd, sqlParams);
            da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return GetStringFromDataSet(ds);
        }
        private static string GetStringFromDataSet(DataSet ds)
        {
            StringBuilder sb = new StringBuilder();
            if (Utility.IsValidDataSet(ds))
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                    {
                        sb.Append(ds.Tables[0].Rows[i][j].ToString());
                        sb.Append(", ");
                    }
                }
                if (sb.Length > 0)
                    sb.Remove(sb.Length - 1, 1);
            }
            return sb.ToString();
        }
        public SqlParameter AddParameter(string paramName, object paramValue)
        {
            return new SqlParameter(paramName, paramValue);
        }
        public SqlParameter AddParameter(string paramName, object paramValue, SqlDbType dbType, int size)
        {
            SqlParameter p = new SqlParameter(paramName, dbType, size);
            p.Value = paramValue;
            p.Direction = ParameterDirection.Input;
            return p;
        }
        public SqlParameter AddParameter(string paramName, object paramValue, SqlDbType dbType, int size, ParameterDirection dir)
        {
            SqlParameter p = new SqlParameter(paramName, dbType, size);
            p.Value = paramValue;
            p.Direction = dir;
            return p;
        }

        private void AddParameters(SqlCommand cmd, List<SqlParameter> sqlParams)
        {
            foreach (SqlParameter param in sqlParams)
            {
                cmd.Parameters.Add(param);
            }
        }

        private SqlCommand PrepareCommand(string query, CommandType cmdType)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 0;
            cmd.CommandText = query;
            cmd.CommandType = cmdType;
            cmd.Connection = Connection;
            cmd.CommandTimeout = 0;
            return cmd;
        }

    }

    public class MySqlDataService
    {
        private long _lastId;
        public long  LastInsertedId { get { return _lastId; } }
        #region Common
        private MySqlConnection _con = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlCon"].ToString());
        //  new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ToString());
        public MySqlConnection Connection
        {

            get
            {
               // _con.ConnectionTimeout = 0;
                return _con;
            }
        }
        public MySqlDataService()
        {

        }
        #endregion

        #region DB Methods
         public int ExecuteQuery(string query)
        {
            Connection.Open();
            int result = 0;
            MySqlCommand cmd = PrepareCommand(query, CommandType.Text);
           
            result = cmd.ExecuteNonQuery();
            Connection.Close();
            return result;
        }

         public int ExecuteQuery(string query,ref string IdentityColValue)
         {
             Connection.Open();
             int result = 0;
             MySqlCommand cmd = PrepareCommand(query, CommandType.Text);
             result = cmd.ExecuteNonQuery();
             IdentityColValue = cmd.LastInsertedId.ToString();
             Connection.Close();
             return result;
         }
         public int ExecuteQuery(string procName, List<MySqlParameter> sqlParams)
         {
             int result = 0;
             Connection.Open();
             MySqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
             AddParameters(cmd, sqlParams);             
             result = cmd.ExecuteNonQuery();
             _lastId = cmd.LastInsertedId;
             Connection.Close();
             return result;
         }

         public int ExecuteQuery(string procName, List<MySqlParameter> sqlParams, ref object outPutValue)
         {
             int result = 0;
             Connection.Open();
             MySqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
             AddParameters(cmd, sqlParams);
             result = cmd.ExecuteNonQuery();
             try
             {
                 outPutValue = cmd.Parameters[sqlParams.Count - 1].Value;
             }
             catch { outPutValue = null; }
             Connection.Close();
             return result;
         }

         public int ExecuteQuery(string procName, List<MySqlParameter> sqlParams, ref object outPutValue1, ref object outputValue2)
         {
             Connection.Open();
             int result = 0;
             MySqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
             AddParameters(cmd, sqlParams);
            
             result = cmd.ExecuteNonQuery();
             try
             {
                 outPutValue1 = cmd.Parameters[sqlParams.Count - 2].Value;
                 outputValue2 = cmd.Parameters[sqlParams.Count - 1].Value;
             }
             catch { outPutValue1 = null; outputValue2 = null; }
             Connection.Close();
             return result;
         }

         public Object ExecuteScalar(string query)
         {
             Connection.Open();
             MySqlCommand cmd = PrepareCommand(query, CommandType.Text);
           
             Object obj = cmd.ExecuteScalar();
             Connection.Close();
             return obj;

         }
         public Object ExecuteScalar(string procName, List<MySqlParameter> sqlParams)
         {
             Connection.Open();
             MySqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
             AddParameters(cmd, sqlParams);            
             object obj = cmd.ExecuteScalar();
             Connection.Close();
             return obj;
         }
         public DataSet ExecuteDataSet(string query)
         {
             Connection.Open();
             DataSet ds = new DataSet();
             MySqlDataAdapter da = null;
             MySqlCommand cmd = PrepareCommand(query, CommandType.Text);
             da = new MySqlDataAdapter(cmd);
             da.Fill(ds);
             Connection.Close();
             return ds;
         }
         public DataSet ExecuteDataSet(string procName, List<MySqlParameter> sqlParams)
         {
             Connection.Open();
             DataSet ds = new DataSet();
             MySqlDataAdapter da = null;
             MySqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
             AddParameters(cmd, sqlParams);
             da = new MySqlDataAdapter(cmd);
             da.Fill(ds);
             Connection.Close();
             return ds;
         }
         public DataSet ExecuteDataSet(string procName, List<MySqlParameter> sqlParams, ref object outPutValue)
         {
             Connection.Open();
             DataSet ds = new DataSet();
             MySqlDataAdapter da = null;
             MySqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
             AddParameters(cmd, sqlParams);
             da = new MySqlDataAdapter(cmd);
             da.Fill(ds);
             try
             {
                 outPutValue = cmd.Parameters[sqlParams.Count - 1].Value;
             }
             catch { outPutValue = null; }
             finally
             {
                 Connection.Close();
             }
             return ds;
         }

         public string ExecuteString(string query)
         {
             Connection.Open();
             DataSet ds = new DataSet();
             MySqlDataAdapter da = null;
             MySqlCommand cmd = PrepareCommand(query, CommandType.Text);
             da = new MySqlDataAdapter(cmd);
             da.Fill(ds);
             Connection.Close();
             return GetStringFromDataSet(ds);
         }
         public string ExecuteString(string procName, List<MySqlParameter> sqlParams)
         {
             Connection.Open();
             DataSet ds = new DataSet();
             MySqlDataAdapter da = null;
             MySqlCommand cmd = PrepareCommand(procName, CommandType.StoredProcedure);
             AddParameters(cmd, sqlParams);
             da = new MySqlDataAdapter(cmd);
             da.Fill(ds);
             Connection.Close();
             return GetStringFromDataSet(ds);
         }
         private static string GetStringFromDataSet(DataSet ds)
         {
             StringBuilder sb = new StringBuilder();
             if (Utility.IsValidDataSet(ds))
             {
                 for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                 {
                     for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                     {
                         sb.Append(ds.Tables[0].Rows[i][j].ToString());
                         sb.Append(",");
                     }
                 }
                 if (sb.Length > 0)
                     sb.Remove(sb.Length - 1, 1);
             }
             return sb.ToString();
         }
        #endregion

        #region Private Methods - Common
        private MySqlCommand PrepareCommand(string query, CommandType cmdType)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = query;
            cmd.CommandType = cmdType;
            cmd.Connection = Connection;
            cmd.Prepare();
            cmd.CommandTimeout = 0;
            return cmd;
        }
        public MySqlParameter AddParameter(string paramName, object paramValue)
        {
            return new MySqlParameter(paramName, paramValue);
        }
        public MySqlParameter AddParameter(string paramName, object paramValue, MySqlDbType dbType, int size)
        {
            MySqlParameter p = new MySqlParameter(paramName, dbType, size);
            p.Value = paramValue;
            p.Direction = ParameterDirection.Input;
            return p;
        }
        public MySqlParameter AddParameter(string paramName, object paramValue, MySqlDbType dbType, int size, ParameterDirection dir)
        {
            MySqlParameter p = new MySqlParameter(paramName, dbType, size);
            p.Value = paramValue;
            p.Direction = dir;
            return p;
        }
        private void AddParameters(MySqlCommand cmd, List<MySqlParameter> sqlParams)
        {
            foreach (MySqlParameter param in sqlParams)
            {
                cmd.Parameters.Add(param);
            }
        }
        #endregion
    }
}
