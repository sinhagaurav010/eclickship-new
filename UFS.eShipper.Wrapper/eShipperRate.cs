﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UFS.eShipper.Wrapper
{
    public class eShipperRate
    {
            public string ServiceId { get; set; }
            public string ServiceDesc { get; set; }
            public string Amount { get; set; }
            public string Currency { get; set; }
            public string RateId { get; set; }
            public string Message { get; set; }
            public string DeliveryDate { get; set; }
    }
}
